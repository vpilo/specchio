set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm)

set(triple arm-linux-gnueabihf)

set(CMAKE_C_COMPILER clang-7)
set(CMAKE_C_COMPILER_TARGET ${triple})
set(CMAKE_CXX_COMPILER clang++-7)
set(CMAKE_CXX_COMPILER_TARGET ${triple})

set(CMAKE_CXX_FLAGS "-mthumb")
add_compile_options(-fcxx-exceptions)
add_link_options(-latomic)

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

# Where to find packages and cmake scripts
set(ENV{PKG_CONFIG_PATH} /usr/lib/arm-linux-gnueabihf/pkgconfig)
set(CMAKE_LIBRARY_PATH /usr/lib/arm-linux-gnueabihf;/usr/lib/x86_64-linux-gnu)
include_directories(BEFORE SYSTEM /usr/lib/llvm-7.0/lib/clang/7.0.0/include)

# Qt include directories
set(QT_INCLUDE_DIR           /usr/include/arm-linux-gnu)
set(QT_QT_INCLUDE_DIR        ${QT_INCLUDE_DIR}/qt5)
set(QT_QTCORE_INCLUDE_DIR    ${QT_QT_INCLUDE_DIR}/QtCore)
set(QT_QTXML_INCLUDE_DIR     ${QT_QT_INCLUDE_DIR}/QtXml)
set(QT_QTGUI_INCLUDE_DIR     ${QT_QT_INCLUDE_DIR}/QtGui)
set(QT_QTNETWORK_INCLUDE_DIR ${QT_QT_INCLUDE_DIR}/QtNetwork)
set(QT_QTUITOOLS_INCLUDE_DIR ${QT_QT_INCLUDE_DIR}/QtUiTools)
set(QT_QTSCRIPT_INCLUDE_DIR  ${QT_QT_INCLUDE_DIR}/QtScript)
set(QT_QTWEBKIT_INCLUDE_DIR  ${QT_QT_INCLUDE_DIR}/QtWebkit)
set(QT_INCLUDES ${QT_INCLUDE_DIR} ${QT_QT_INCLUDE_DIR} ${QT_QTCORE_INCLUDE_DIR} ${QT_QTXML_INCLUDE_DIR} ${QT_GUI_INCLUDE_DIR} ${QT_QTNETWORK_INCLUDE_DIR} ${QT_QTWEBKIT_INCLUDE_DIR} )

# Qt libraries
set(QT_LIBRARY_DIR           ${CMAKE_LIBRARY_PATH})
set(QT_CROSS_LIBRARIES QtSvg5 QtGui5 QtCore5 QtXml5 QtNetwork5 QtWebKit5 QtXmlPatterns5 QtDeclarative5)

# Qt binaries
set(QT_BINARY_DIR          /usr/lib/x86_64-linux-gnu/qt5/bin)
set(QT_MOC_EXECUTABLE      ${QT_BINARY_DIR}/moc)
set(QT_UIC_EXECUTABLE      ${QT_BINARY_DIR}/uic)
set(QT_QMAKE_EXECUTABLE    ${QT_BINARY_DIR}/qmake)
set(QT_RCC_EXECUTABLE      ${QT_BINARY_DIR}/rcc)
set(QT_LRELEASE_EXECUTABLE ${QT_BINARY_DIR}/lrelease)

# Without these, Qt looks for the x86_64 tools, instead of the arm ones
if(NOT TARGET Qt5::qmake)
add_executable(Qt5::moc IMPORTED)
set_target_properties(Qt5::moc PROPERTIES IMPORTED_LOCATION ${QT_MOC_EXECUTABLE})
add_executable(Qt5::rcc IMPORTED)
set_target_properties(Qt5::rcc PROPERTIES IMPORTED_LOCATION ${QT_RCC_EXECUTABLE})
add_executable(Qt5::qmake IMPORTED)
set_target_properties(Qt5::qmake PROPERTIES IMPORTED_LOCATION ${QT_QMAKE_EXECUTABLE})
endif()
