set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR x86_64)

set(triple x86_64-linux-gnu)

set(CMAKE_C_COMPILER clang-7)
set(CMAKE_C_COMPILER_TARGET ${triple})
set(CMAKE_CXX_COMPILER clang++-7)
set(CMAKE_CXX_COMPILER_TARGET ${triple})

add_compile_options(-fcxx-exceptions)

include_directories(BEFORE SYSTEM /usr/lib/llvm-7.0/lib/clang/7.0.0/include)
