# Plugins development

Any experience with web development is good, since plugins are mostly written in QML - which is Javascript at heart.
A very helpful [Qt documentation page](https://doc.qt.io/qt-5/qmlapplications.html) will guide through the first steps;
another very simple way to begin is to copy one of the existing plugins, hack on it and rework it to understand how it
ticks.

For a more advanced example, the [ExampleCppQmlClock](../plugins/Specchio.ExampleCppQmlClock/) plugin implements a C++
and QML plugin, which creates its own QML user interface from the C++ code instead of just providing a QML file.

To develop plugins, the Specchio framework provides a few useful interfaces.

## Units

This QML always-accessible class contains various sizes, durations and units you can use to space and resize your UI
elements appropriately and using the same style as the rest of the plugins, for visual consistency.

### Icon dimensions

Use `Units.iconSize.<size>` to uniformly size icons.
The available dimensions are `small`, `smallMedium`, `medium`, `large`, and `huge`.

### Text size

Use `Units.fontSize.<size>` to appropriately size text.
The available dimensions are `tiny`, `small`, `smallMedium`, `medium`, `large`, and `huge`.

### Empty space

Use `Units.spacing.<size>` to set the distance between your UI elements.
The available dimensions are `tiny`, `small`, `medium`, and `large`.

### Duration

To simplify the definition of timers and animations, there are some useful time durations:

 * `Units.duration.lengthy`: Half a second, 500 milliseconds.
 * `Units.duration.medium`: A quarter of a second, 250 milliseconds.
 * `Units.duration.brief`: 150 milliseconds.

For timers, something longer, these durations, all expressed in milliseconds:

 * `Units.duration.second`: One second: 1000 milliseconds.
 * `Units.duration.minute`: One minute.
 * `Units.duration.hour`: One hour.

You can also check out the source of the Units file [here](../resources/qml/Specchio/Units.qml).

## Plugin settings

Every plugin has its own settings. If the user has not created a config file for the plugin placed in a specific region,
Specchio will copy the default configuration. When a user config file is present, the default settings file is ignored.

When making a plugin, always provide a `default.toml` file, even if it's empty.

## QML interfaces

### Plugin configuration

The plugin's configuration is always accessible via the `settings` object. It is a JSON object, containing all the
settings defined by your plugin and potentially overridden by the user. Remember that the settings contained are those
specific to the plugin *and* the region it's located in.

If your plugin has a configuration file like this:

```toml
[group]
config1=true
[group.subgroup]
config2=false
```

Then in your plugin you can access it like this:

```qml
var enabled = settings.group.config1 || settings.group.subgroup.config2
```

### Global configuration

To access global configuration (e.g. whether debug mode is on) and to connect to system events, there is the `Specchio`
object.

Always add `import Specchio 1.0` at the beginning of your file.

First a few useful properties:

 * `Specchio.connected`: Boolean. Whether there is an available Internet connection.
 * `Specchio.debugMode`: Boolean. Whether the debug mode is enabled or not, e.g. whether to display extra debug info.

Other properties are `font`, `backgroundColor`, `rotation`, and `settings` (containing the global Specchio options),
but they're not very useful.

### Receiving global events

Here's how to connect to global events:

```qml
import Specchio 1.0

[...]

    Connections {
        target: Specchio
        onStarting: {
            // Signal received when Specchio is ready to show on screen
        }
        onStopping: {
            // Signal received when Specchio is about to turn off
        }
        onConnectedChanged: {
            if (isOnline) {
                // Internet is available
            } else {
                // Internet is unavailable
            }
        }
        onRefreshView: {
            // Signal received every half minute: can be used to update labels which should change over time,
            // for example "one minute ago"
        }
    }
```

### Inter-plugin communication

Another important concept is that of *broadcasts*. A broadcast is a message that a plugin can send to all others, at any
time it deems useful. A broadcast has a name, and an optional JSON object where to store metadata about the event.

```qml
Plugin.sendBroadcast(name, metadata)
```

For example:

```qml
// Simple case
Plugin.sendBroadcast('I_WANT_CAKE')
// Complex broadcast
Plugin.sendBroadcast('I_WANT_CAKE', { type: 'sachertorte', amount: 200, ingredients: { alcohol: true, chocolate: 99999 } })
```

Broadcasts are received using the Plugin object's signal `broadcast`. It has as arguments a string argument `name`, and
a JSON object `extras`:

```qml
import Specchio 1.0

[...]

    Connections {
        target: Plugin
        onBroadcast: {
            // Received when a plugin has an event to share with the others.
            if (name == 'I_WANT_CAKE') {
                happy = true
                if ('type' in extras && extras.type == 'sachertorte') {
                    extra_happy = true
                }
            }
        }
    }
```

### Smart timers

Smart timers are used to:

* Only trigger timed events when internet connectivity is available.
* Reduce the system activity, as they all use a single OS timer to avoid waking up the system too often.
* Avoid having to deal manually with onStarting/onStopping signals.

Network smart timers better respond to intermittently lacking connectivity. Timed events which rely on
connectivity will not be fired until network is available. So if a timer was supposed to trigger, but the
network was then not reachable, it will trigger (only once) as soon as the network is again reachable.

Smart timers begin already in the 'started' state. If the `triggeredImmediately` argument is `true`, a trigger is
scheduled asynchronously, to give time to establish signal connections.

To get a normal smart timer, give an id, an interval in seconds, and whether the timer should trigger as soon as
possible after creation:

```qml
let timer = Plugin.timer(id, intervalInSeconds, triggeredImmediately)
```

Giving the same ID twice, or an invalid interval, returns `undefined`.

For example:

```qml
let timer = Plugin.timer("hello", 10, false)
timer.triggered.connect(showHello)
...
function showHello() { label.text = "hello!" }
```

A lambda can also be used:
```qml
timer.triggered.connect(function () { label.text = "hello!" })
```

To get a network-bound smart timer:

```qml
let timer = Plugin.networkTimer(id, intervalInSeconds, triggeredImmediately)
```

To retrieve an existing timer by its ID, use `Plugin.getTimer(id)`. It will return `undefined` if the ID was not found.

Removing a timer is possible by using `Plugin.removeTimer(id)`.

### Others

There are a few other useful environment-related pieces of data that can be obtained by plugins:

* `bool Plugin.positionedAtLeft` - Contains `true` if the plugin is on the left side of the screen
* `bool Plugin.positionedAtRight` - Contains `true` if the plugin is on the right side of the screen
* `bool Plugin.positionedAtTop` - Contains `true` if the plugin is on the top half of the screen
* `bool Plugin.positionedAtBottom` - Contains `true` if the plugin is on the bottom half of the screen
* `bool Plugin.positionedAtCenter` - Contains `true` if the plugin is on one of the center regions or a full line
* `string Plugin.regionId` - The enum with the actual region ID the plugin is currently loaded in
* `string Plugin.pluginPath` - The path on disk where the plugin was loaded from (useful to load extra resources)
