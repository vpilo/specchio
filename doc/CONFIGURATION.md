# Configuration

When Specchio is run the first time, it will generate a default configuration directory in your home: `~/.specchio`; if
you use the default `pi` user, it will be `/home/pi/.specchio/`.

This directory is where all configuration and extension takes place. All config files have extension `.toml`, because
they are written in the easily human-readable and changeable [TOML](https://github.com/toml-lang/toml) language.

The main config file is `specchio.toml`. It contains a few interesting options; the main ones are:

 * `debug`: which you should set to `true` before re-launching Specchio. You will be able to see how the screen is
    partitioned and how the partitions (also called "regions") are named: in each of those areas you can have a plugin
    to add some functionality to your mirror's screen.
 * `rotation`: This rotates the visual output. The unit is degrees. You can set it to 90° increments between `-270` and
    `270`. The default is `0`.
 * `regions`: this group contains the names of the regions the screen is divided in. Put a plugin name in any of the
    entries there to load it there. When Specchio is launched next, it will create a default configuration file, if you
    haven't made one yourself. See the next paragraph for more information.
 * `backgroundColor`: The background color is changeable. The format is `#RRGGBB`, as in HTML.
 * `customFont`: The font for all text in the application. Just put your font in the `~/.specchio` directory and specify
    its file name here.

# Regions configuration

Multiple copies of the same plugin can coexist. Every one will have a different configuration file in the configuration
directory, named after the plugin and the location; for example, `Specchio.Compliments.MiddleCenter.toml`.

To install a new plugin, put it in the `plugins` sub-directory and add it to one of the regions in
`~/.specchio/specchio.toml`.

Each plugin has its own configuration, or even none. In that case, the configuration file will be empty.

# Plugins

Specchio currently comes with the following built-in plugins:

 * [Calendar](../plugins/Specchio.Calendar/README.md), to show your events
 * [Compliments](../plugins/Specchio.Compliments/README.md), to make you feel special
 * [DigitalClock](../plugins/Specchio.DigitalClock/README.md), to show how late are you
 * [GPIO](../plugins/Specchio.GPIO/README.md), to connect your mirror to the physical world
 * [News](../plugins/Specchio.News/README.md), to see what's up in the world
 * [PiDisplay](../plugins/Specchio.PiDisplay/README.md), to save power when you're not around
 * [Weather](../plugins/Specchio.Weather/README.md), to check if you need to carry the umbrella

Upon first run, all loaded plugins will generate a configuration file in your `~/.specchio` directory with their default
settings.
