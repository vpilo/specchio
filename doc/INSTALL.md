# Installation

Specchio runs on x86 and ARM, and in theory on anything that runs Qt 5.11.
For Raspberry Pi, this means you have to run Raspbian Buster.

1. On Raspbian, first some extra dependencies' packages need to be installed:

    ```bash
    sudo apt update && apt install -y --no-install-recommends --no-install-suggests \
    libgl1-mesa-dri \
    libqt5quick5 \
    libqt5svg5 \
    libqt5multimedia5-plugins \
    qml-module-qtwayland-compositor \
    qml-module-qtquick2 \
    qml-module-qtquick-window2 \
    qml-module-qtquick-controls2 \
    qml-module-qtquick-templates2 \
    qml-module-qtquick-xmllistmodel \
    libical3 \
    libatlas3-base \
    pigpiod
    ```

    They amount to around 450MB of packages.

1. Also ensure you are running the 'real' Raspberry Pi openGL drivers:

    ```bash
    sudo raspi-config
    ```

    From there, select `7 Advanced Options` then `A7 GL Driver`, and choose either `G2 GL (Fake KMS)` or `G3 GL (Full KMS)`.
    Note that with option G3 the Raspberry Pi doesn't support screen rotation at all. You can instead use the `rotation` option
    of Specchio to adapt it to the actual screen orientation.

1. Download a build from GitLab: check which builds with name `armv6hf-debug` are marked as `passed` on [this page](https://gitlab.com/vpilo/specchio/-/jobs?scope=finished),
    then click the Download icon on the right.

1. Copy the file on your Pi (or use `wget`) and extract the files: `sudo unzip -d / specchio-*.zip` - the zip already
   contains the full path `/opt/specchio`.
   You should end up with a file called `/opt/specchio/bin/specchio`.

1. Then, to launch the application:

    ```bash
    /opt/specchio/bin/specchio
    ```

1. To set it up to start automatically at boot, you can use the systemd [service file](../resources/specchio.service).
   Copy it in `~/.systemd/user/` and run `systemctl --user enable specchio`.

1. To have the Pi login automatically and show Specchio, you should then edit `/etc/systemd/system/getty@tty1.service`
   and add the `--autologin <username>` option.

1. Now you can proceed to [the Configuration page](./CONFIGURATION.md).
