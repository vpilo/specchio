/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#pragma once

#include <functional>

#include <QtCore/QMap>
#include <QtCore/QObject>


class Types
{
    Q_GADGET

public:
    enum class Region {
          TopBar
        , TopLeft
        , TopCenter
        , TopRight
        , UpperThird
        , MiddleCenter
        , LowerThird
        , BottomLeft
        , BottomCenter
        , BottomRight
        , BottomBar
    };
    enum RegionMeta {
        FirstRegion = static_cast<int>(Region::TopBar),
        LastRegion = static_cast<int>(Region::BottomBar),
        NumRegions = static_cast<int>(LastRegion) + 1,
    };
    Q_ENUM(Region);

    using RegionOperation = std::function<void(Region)>;

public:
    static void forEachRegion(const RegionOperation& op);
    static const QString regionName(const Region region);

private:
    explicit Types() {};
};
