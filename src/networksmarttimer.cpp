/*
 * Copyright 2020 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#include "networksmarttimer.h"

#include <QtCore/QDateTime>
#include <QtCore/QLoggingCategory>

Q_LOGGING_CATEGORY(spNST, "NetworkSmartTimer", QtInfoMsg);

NetworkSmartTimer::NetworkSmartTimer(QObject* parent, const QString& id, quint32 intervalSeconds, bool triggerOnStart)
: SmartTimer(parent, id, intervalSeconds, triggerOnStart)
{
}

void NetworkSmartTimer::trigger()
{
    if (mNetworkPresent) {
        qCDebug(spNST) << "Network OK: triggering" << id();
        SmartTimer::trigger();
    } else {
        qCDebug(spNST) << "Network absent: queueing" << id();
        mShouldTriggerWhenConnected = true;
        mLastUpdateTimeSeconds = QDateTime::currentSecsSinceEpoch();
    }
}

void NetworkSmartTimer::onConnectedChanged(bool isOnline)
{
    mNetworkPresent = isOnline;

    if (isOnline && mShouldTriggerWhenConnected) {
        qCDebug(spNST) << "Network OK: triggering from queue" << id();
        mShouldTriggerWhenConnected = false;
        SmartTimer::trigger();
    }
}
