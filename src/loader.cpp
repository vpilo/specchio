/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#include "loader.h"
#include "paths.h"
#include "plugin.h"
#include "region.h"
#include "registry.h"
#include "settings.h"
#include "smarttimer.h"
#include "specchio.h"
#include "types.h"

#include <QtCore/QCoreApplication>
#include <QtCore/QDir>
#include <QtCore/QDirIterator>
#include <QtCore/QLoggingCategory>
#include <QtCore/QSharedPointer>
#include <QtGui/QFontDatabase>
#include <QtGui/QGuiApplication>
#include <QtQuick/QQuickItem>
#include <QtQuick/QQuickWindow>
#include <QtQml/QQmlApplicationEngine>
#include <QtQml/QQmlContext>


namespace {
    const QString s_pluginSettingsFileName { QStringLiteral("%1.%2.toml") };
    const QString s_pluginLibraryFileName { QStringLiteral("^(lib)?%1.*\\.so") };

    const QString s_defaultFont { QStringLiteral("NotoSans-Regular.ttf") };
    const QString s_emojiFont { QStringLiteral("TwitterColorEmoji-SVGinOT.ttf") };
} // namespace

Q_LOGGING_CATEGORY(spLoad, "Loader", QtInfoMsg);


Loader::Loader(QObject *parent, Registry *registry, Specchio *interface)
: QObject(parent)
, mRegistry(registry)
, mEngine(new QQmlApplicationEngine(this))
{
    Q_ASSERT(registry != nullptr);
    Q_ASSERT(interface != nullptr);

    qRegisterMetaType<Types::Region>("RegionId");
    qmlRegisterUncreatableType<Types>("Specchio.Internal", 1, 0, "RegionId", "Cannot create enum types");
    qmlRegisterUncreatableType<SmartTimer>("Specchio", 1, 0, "SmartTimer", "Cannot create SmartTimer type objects");
    qmlRegisterType<Region>("Specchio.Internal", 1, 0, "RegionImpl");

    mEngine->rootContext()->setContextProperty(QStringLiteral("Specchio"), interface);

    connect(qApp, &QCoreApplication::aboutToQuit, this, &Loader::onQuit);
}

bool Loader::initialize(const QString &mainQmlFile)
{
    if(mRegistry->settings().isNull()) {
        return false;
    }
    if(mRegistry->interface() == nullptr) {
        return false;
    }

    mEngine->addImportPath(QStringLiteral(SPECCHIO_RESOURCE_DIR));
    mEngine->load(QUrl(mainQmlFile));

    QList<QObject*> rootObjects = mEngine->rootObjects();
    if (rootObjects.size() != 1) {
        qCCritical(spLoad) << "Unable to load main layout," << rootObjects.size() << "found";
        return false;
    }

    auto mainWindow = rootObjects.first()->findChild<QQuickWindow*>();
    if (mainWindow == nullptr) {
        qCCritical(spLoad) << "Scene does not contain a window:" << rootObjects.first();
        return false;
    }

    bool success = true;
    Registry::RegionMap regions;
    Types::forEachRegion([&success, &regions, mainWindow](Types::Region id) {
        const QString &regionName { Types::regionName(id) };
        auto region = mainWindow->findChild<Region*>(regionName);
        if (region == nullptr) {
            success = false;
            return;
        }
        regions[id] = region;
    });
    if (!success) {
        qCCritical(spLoad) << "Unable to find all regions in the main layout";
        return false;
    }
    Q_ASSERT(regions.count() == Types::RegionMeta::NumRegions);
    mRegistry->setRegions(regions);

    mainWindow->setIcon(QIcon(QStringLiteral(":/icon.png")));

    return initializeFonts();
}

bool Loader::initializeFonts()
{
    QString customFont { mRegistry->settings()->value(QStringLiteral("customFont"), QString()).toString() };
    if (customFont.isEmpty() || !loadFont(customFont, true)) {
        qCDebug(spLoad) << "Using default font";

        if (!loadFont(s_defaultFont, true)) {
            qCCritical(spLoad) << "Unable to load default font!";
            return false;
        }
    } else {
        qCDebug(spLoad) << "Using custom font:" << customFont;
    }
    if (!loadFont(s_emojiFont, false)) {
        qCWarning(spLoad) << "Unable to load emoji font!";
    }
    return true;
}

bool Loader::loadFont(const QString &fontFile, bool loadApplicationFont)
{
    const QString file { Paths::resource(fontFile) };

    int id = QFontDatabase::addApplicationFont(file);
    if (id == -1) {
        qCWarning(spLoad) << "Unable to load font:" << file;
        return false;
    }
    const QStringList families { QFontDatabase::applicationFontFamilies(id) };
    if (families.isEmpty()) {
        qCWarning(spLoad) << "Invalid font:" << file;
        return false;
    }
    if (loadApplicationFont) {
        const QFont font { families.constFirst() };
        QGuiApplication::setFont(font);
        mRegistry->interface()->setFont(font);
    }
    return true;
}

void Loader::scanPlugins()
{
    qCDebug(spLoad) << "Scan directories:";
    for(const auto &entry: Paths::getPluginsPaths()) {
        qCDebug(spLoad) << "->" << entry;
    }

    qCDebug(spLoad) << "Scanning for plugins...";
    QStringList foundPluginNames;

    // Need to find the 'qmldir' file for every plugin and examine its contents
    const QString &pluginInfoFile { QStringLiteral("qmldir") };
    Q_FOREACH(QString path, Paths::getPluginsPaths()) {
        QDirIterator entries { path, QDir::Files, QDirIterator::Subdirectories | QDirIterator::FollowSymlinks };
        while (entries.hasNext()) {
            entries.next();
            const QString &fileName { entries.fileName() };
            if (fileName != pluginInfoFile) {
                continue;
            }

            const QFileInfo fileInfo { entries.fileInfo() };

            Plugin::Ptr newPlugin = parsePlugin(fileInfo.absoluteFilePath(), fileInfo.absolutePath());
            if (newPlugin.isNull()) {
                continue;
            }

            const QString &module { newPlugin->module() };

            if (!newPlugin->isValid()) {
                qCWarning(spLoad) << "Skipping invalid plugin" << module;
                continue;
            }

            // Modules can contain dots; they get converted to subdirectories (like java packages) so we need to take them off.
            int dots = module.count('.') + 1;
            QDir pathWithoutPrefix { fileInfo.absoluteDir() };
            while (dots-- > 0) {
                pathWithoutPrefix.cdUp();
            }
            const QString &absolutePath { pathWithoutPrefix.absolutePath() };
            if (!mEngine->importPathList().contains(absolutePath)) {
                mEngine->addImportPath(absolutePath);
            }
            if (!newPlugin->library().isEmpty() && !mEngine->pluginPathList().contains(absolutePath)) {
                mEngine->addPluginPath(absolutePath);
            }

            foundPluginNames.append(module);
            mRegistry->addPlugin(module, newPlugin);
        }
    }

    qCDebug(spLoad) << "Found plugins:";
    for(const auto &entry: qAsConst(foundPluginNames)) {
        qCDebug(spLoad) << "->" << entry;
    }
    qCDebug(spLoad) << "Import paths:";
    Q_FOREACH(const QString &entry, mEngine->importPathList()) {
        qCDebug(spLoad) << "->" << entry;
    }
    qCDebug(spLoad) << "Plugin paths:";
    Q_FOREACH(const QString &entry, mEngine->pluginPathList()) {
        qCDebug(spLoad) << "->" << entry;
    }
}

Plugin::Ptr Loader::parsePlugin(const QString &qmldirPath, const QString &pluginPath)
{
    QFile file(qmldirPath);
    if (!file.open(QIODevice::Text | QIODevice::ReadOnly)) {
        qCWarning(spLoad) << "Cannot examine plugin info file:" << qmldirPath;
        return {};
    }

    QString module;
    QString library;
    QString mainObject;
    QString version;
    while (!file.atEnd()) {
        const QString &line { file.readLine().simplified() };
        if (line.isEmpty() || line.startsWith(QStringLiteral("#"))) {
            continue;
        }
        QStringList parts { line.split(QStringLiteral(" ")) };

        // Line example: "module com.specchio.plugins.example1"
        if (line.startsWith(QStringLiteral("module"))) {
            if (parts.size() != 2) {
                continue;
            }
            module = parts.at(1);
            continue;
        }

        // Line example: "plugin example1"
        if (line.startsWith(QStringLiteral("plugin"))) {
            if (parts.size() != 2) {
                continue;
            }
            // Find the library of this plugin on the filesystem
            const QString &tempLibName { parts.at(1) };
            QDirIterator pluginFiles(pluginPath, QDir::Files, QDirIterator::FollowSymlinks);
            while (pluginFiles.hasNext()) {
                pluginFiles.next();
                QRegExp libraryNameRegex { s_pluginLibraryFileName.arg(tempLibName) };
                if (pluginFiles.fileName().contains(libraryNameRegex)) {
                    library = pluginFiles.filePath();
                    break;
                }
            }
            continue;
        }

        // Line example: "ExampleQmlObject 1.0 example1.qml"
        if (parts.size() == 4 /* singleton object type */ && parts.at(0) == QStringLiteral("singleton")) {
            parts.removeAt(0);
        }
        if (parts.size() != 3) {
            continue;
        }
        if (mainObject.isEmpty()) {
            mainObject = parts.at(0);
            version = parts.at(1);
        }
    }

    return QSharedPointer<Plugin>::create(module, pluginPath, mainObject, version, library);
}

bool Loader::loadSavedPlugins()
{
    const QVariantMap regionSettings { mRegistry->settings()->value(QStringLiteral("regions")).toMap() };
    if (regionSettings.isEmpty()) {
        qCWarning(spLoad) << "Unable to load region settings!";
        return false;
    }

    Types::forEachRegion([regionSettings, this](Types::Region region) {
        const QString &regionName { Types::regionName(region) };
        const QVariant value { regionSettings.value(regionName) };
        if (value.isValid()) {
            const QString plugin { value.toString() };
            if (!plugin.isEmpty() && !loadPlugin(region, plugin)) {
                qCWarning(spLoad) << "Region" << regionName << ": cannot load plugin" << plugin;
            }
        } else {
            qCDebug(spLoad) << "Invalid config for region" << regionName;
        }
    });

    mEngine->trimComponentCache();

    mRegistry->prepare();

    Specchio *interface = mRegistry->interface();
    interface->start();
    return true;
}

void Loader::onQuit()
{
    qCInfo(spLoad) << "Quitting";
    mRegistry->interface()->stop();
}

bool Loader::loadPlugin(const Types::Region regionId, const QString& name)
{
    Region *region = mRegistry->region(regionId);
    Q_ASSERT(region != nullptr);
    Plugin::Ptr plugin = mRegistry->plugin(name);
    if (plugin.isNull()) {
        qCWarning(spLoad) << "Unregistered plugin" << name << "specified for region" << region->name();
        return false;
    }

    // A plugin has its own settings, loaded differently according to the region it's loaded in
    auto pluginSettings = Settings::Ptr::create(plugin->path() + '/' + Paths::getDefaultSettingsFileName(),
                                                Paths::getSettingsPath() + s_pluginSettingsFileName.arg(name, Types::regionName(regionId)));

    if (region->populate(plugin, pluginSettings)) {
        return region->activate();
    }
    return false;
}

void Loader::translate()
{
    const QString language { mRegistry->interface()->language().toLower() };
    const QString translationFileName { QStringLiteral(SPECCHIO_TRANSLATIONS_DIR) + '/' + QStringLiteral("specchio.%1.qm").arg(language) };

    if (!QFile::exists(translationFileName)) {
        qCWarning(spLoad) << "Translation for selected language" << language << "cannot be found!";
        qCInfo(spLoad) << "The file should be:" << translationFileName;
    } else if (!translator.load(translationFileName)) {
        qCWarning(spLoad) << "Translation loading failed for language" << language;
    } else if (!QCoreApplication::installTranslator(&translator)) {
        qCWarning(spLoad) << "Translation setting failed for language" << language;
    } else {
        return;
    }

    // If no valid language was set, unset the value to use the default.
    mRegistry->settings()->remove(QStringLiteral("language"));
}
