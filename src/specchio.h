/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#pragma once

#include "networkwatcher.h"
#include "settings.h"
#include "types.h"

#include <QtGui/QColor>
#include <QtGui/QFont>
#include <QtCore/QMap>
#include <QtCore/QObject>
#include <QtCore/QTimer>


class Specchio : public QObject
{
    Q_OBJECT

public:
    Q_PROPERTY(bool debugMode READ debugMode CONSTANT)
    Q_PROPERTY(QColor backgroundColor READ backgroundColor CONSTANT)
    Q_PROPERTY(QFont font READ font CONSTANT)
    Q_PROPERTY(int rotation READ rotation CONSTANT)
    Q_PROPERTY(QVariantMap settings READ settings CONSTANT)
    Q_PROPERTY(bool connected READ isConnected NOTIFY connectedChanged)
    Q_PROPERTY(QString language READ language CONSTANT)

    Q_INVOKABLE QString timeDistance(qint64 timestamp, bool isFullDay = false) const;

public:
    Specchio(QObject *parent, Settings::Ptr settings);

    void setFont(const QFont &font) { mFont = font; }

    void start();
    void stop();

public:
    bool debugMode() const { Q_ASSERT(!mSettings.isNull()); return mSettings->value(QStringLiteral("debug"), false).toBool(); }
    QColor backgroundColor() const { Q_ASSERT(!mSettings.isNull()); return mSettings->value(QStringLiteral("backgroundColor"), QStringLiteral("#000")).toString(); }
    int rotation() const { Q_ASSERT(!mSettings.isNull()); return mSettings->value(QStringLiteral("rotation"), 0).toInt(); }
    const QFont &font() const { return mFont; }
    QString language() const { Q_ASSERT(!mSettings.isNull()); return mSettings->value(QStringLiteral("language"), QString("en_us")).toString(); }
    QVariantMap settings() const { Q_ASSERT(!mSettings.isNull()); return *mSettings; }
    inline bool isConnected() const { return mNetworkWatcher.isConnected(); }

private Q_SLOTS:
    void sendUpdates();

private:
    QFont mFont;
    Settings::Ptr mSettings;
    QString mLanguage;

    bool mStarted = false;
    QTimer mUpdateTimer;
    qint64 mLastViewRefresh = 0;

    NetworkWatcher mNetworkWatcher;

Q_SIGNALS:
    // Public signals
    void starting();
    void stopping();
    void refreshView();

    // Internal signals
    void update(qint64 now);
    void connectedChanged(bool isOnline);

#ifdef SPECCHIO_TEST
    friend class InterfaceTests;
#endif
};
