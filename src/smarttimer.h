/*
 * Copyright 2020 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#pragma once

#include <QtCore/QObject>
#include <QtCore/QString>


class SmartTimer : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool disabled READ signalsBlocked CONSTANT)
    Q_PROPERTY(bool running READ isRunning WRITE setRunning)
    Q_PROPERTY(QString id READ id CONSTANT)

public:
    SmartTimer(QObject *parent, const QString &id, quint32 intervalSeconds, bool triggerOnStart);
    ~SmartTimer() = default;

    const QString id() const { return objectName(); }
    quint32 intervalSeconds() const { return mIntervalSeconds; }
    quint64 lastUpdateSeconds() const { return mLastUpdateTimeSeconds; }

    inline Q_INVOKABLE void start() { mRunning = true; }
    inline Q_INVOKABLE void stop() { mRunning = false; }
    bool isRunning() const { return mRunning; }
    void setRunning(bool running) { mRunning = running; }

    Q_INVOKABLE quint32 setInterval(quint32 newInterval);
    Q_INVOKABLE void remove();

public slots:
    Q_INVOKABLE virtual void trigger();
    virtual void update(const qint64 now);

signals:
    void triggered(const QString& id);

protected:
    qint64 mLastUpdateTimeSeconds = 0;

private:
    quint32 mIntervalSeconds;

    bool mRunning = true;
};
