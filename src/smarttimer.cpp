/*
 * Copyright 2020 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#include "smarttimer.h"

#include <QtCore/QDateTime>


SmartTimer::SmartTimer(QObject* parent, const QString& id, quint32 intervalSeconds, bool triggerOnStart)
: QObject{parent}
, mLastUpdateTimeSeconds{QDateTime::currentSecsSinceEpoch()}
, mIntervalSeconds{intervalSeconds}
{
    Q_ASSERT(intervalSeconds > 0);

    setObjectName(id);

    if (triggerOnStart) {
        QMetaObject::invokeMethod(this, &SmartTimer::trigger, Qt::QueuedConnection);
    }
}

quint32 SmartTimer::setInterval(quint32 newInterval)
{
    quint32 oldIntervalSeconds = mIntervalSeconds;
    mIntervalSeconds = newInterval;
    return oldIntervalSeconds;
}

void SmartTimer::trigger()
{
    mLastUpdateTimeSeconds = QDateTime::currentSecsSinceEpoch();
    emit triggered(objectName());
}

void SmartTimer::update(const qint64 now)
{
    if (!mRunning) {
        return;
    }

    if ((now - mLastUpdateTimeSeconds) < mIntervalSeconds) {
        return;
    }

    trigger();
}

void SmartTimer::remove()
{
    delete this;
}
