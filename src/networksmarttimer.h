/*
 * Copyright 2020 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#pragma once

#include "smarttimer.h"

class NetworkSmartTimer : public SmartTimer
{
    Q_OBJECT

public:
    NetworkSmartTimer(QObject *parent, const QString &id, quint32 intervalSeconds, bool triggerOnStart);
    ~NetworkSmartTimer() = default;

public slots:
    Q_INVOKABLE void trigger() override;

    void onConnectedChanged(bool isOnline);

private:
    bool mNetworkPresent = false;
    bool mShouldTriggerWhenConnected = false;
};
