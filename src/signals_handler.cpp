/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#include "signals_handler.h"

#include <QtCore/QLoggingCategory>
#include <QtGui/QGuiApplication>

#include <cstdlib>


namespace
{
    bool s_mustExit = false;
} // namespace

Q_LOGGING_CATEGORY(spSig, "UnixSignals", QtInfoMsg);

SignalsHandler::SignalsHandler()
: QObject(qApp)
, mSigInt()
, mSigTerm()
{
    mSigInt.sa_handler = static_cast<SignalHandlerPtr>(&SignalsHandler::handler);
    sigemptyset(&mSigInt.sa_mask);
    mSigInt.sa_flags = SA_RESTART;
    sigaction(SIGINT, &mSigInt, nullptr);

    mSigTerm.sa_handler = static_cast<SignalHandlerPtr>(&SignalsHandler::handler);
    sigemptyset(&mSigTerm.sa_mask);
    mSigTerm.sa_flags = SA_RESTART;
    sigaction(SIGTERM, &mSigTerm, nullptr);

    mSigTerm.sa_handler = static_cast<SignalHandlerPtr>(&SignalsHandler::handler);
    sigemptyset(&mSigTerm.sa_mask);
    mSigTerm.sa_flags = SA_RESTART;
    sigaction(SIGTERM, &mSigTerm, nullptr);
}

SignalsHandler::~SignalsHandler()
{
    struct sigaction defaultAction;
    defaultAction.sa_handler = SIG_DFL;
    sigemptyset(&defaultAction.sa_mask);

    sigaction(SIGINT, &defaultAction, nullptr);
    sigaction(SIGTERM, &defaultAction, nullptr);
}

void SignalsHandler::handler(int signal)
{
    if (s_mustExit) {
        qCCritical(spSig) << "\rExiting immediately on signal" << signal;
        exit(1);
    }

    qCCritical(spSig) << "\rQuitting on signal" << signal;
    qApp->quit();
    s_mustExit = true;
}
