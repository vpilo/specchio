/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#include "logging.h"

#include <iostream>
#include <QtCore/QDateTime>
#include <QtCore/QThreadStorage>
#include <QtQml/QQmlEngine>

qint64 Logging::sRuntime = -1;
QHash<QByteArray,QByteArray> Logging::sRegionPluginNames;

namespace {
    const char * const s_internalCategory = "Internal";
    const char * const s_pluginsCategory = "Plugins runtime";
    const int s_logMessagesBufferSize = 160;
    QThreadStorage<QByteArray> s_logMessagesBuffer;
} // namespace

void Logging::initialize()
{
    if (sRuntime != -1) {
        return;
    }

#ifdef SPECCHIO_DEBUG
    QLoggingCategory::setFilterRules(
        R"(
        *.debug=true
        qt.*=false
        Loader.debug=false
        *SmartTimer.debug=false
        Specchio.WuW.Listener.debug=false
        Specchio.Calendar.Parser.debug=false
        )");
#else // SPECCHIO_DEBUG
    QLoggingCategory::setFilterRules(
        R"(
        *.debug=false
        )");
#endif // SPECCHIO_DEBUG

    qInstallMessageHandler(Logging::messageHandler);
    sRuntime = QDateTime::currentMSecsSinceEpoch();

    messageHandler(QtMsgType::QtInfoMsg, {nullptr, -1, nullptr, s_internalCategory},
                   QStringLiteral("Specchio starting on %1").arg(QDateTime::currentDateTime().toString()));
}

void Logging::setupQmlEngine(QQmlEngine *engine)
{
    Q_ASSERT(engine != nullptr);
    engine->setOutputWarningsToStandardError(false);
    QObject::connect(engine, &QQmlEngine::warnings, [](const QList<QQmlError> &warnings) {
        printQmlErrors(QString{}, warnings);
    });
}

void Logging::setRegionName(Types::Region region, const QString& name)
{
    sRegionPluginNames.insert(Types::regionName(region).toUtf8(), name.toUtf8());
}

void Logging::printQmlErrors(const QString &plugin, const QList<QQmlError> &errors)
{
    Q_ASSERT(!errors.isEmpty());
    for (const auto &error : errors) {
        if (error.url().isEmpty() && error.description().endsWith(QStringLiteral("unavailable"))) {
            continue;
        }
        const QByteArray fileNameBytes { error.url().toString().toUtf8() };
        const QByteArray pluginBytes { plugin.toUtf8() };

        QMessageLogContext ctx {
            fileNameBytes.constData(),
            error.line(),
            nullptr,
            plugin.isEmpty() ? s_pluginsCategory : pluginBytes.constData() };

        messageHandler(QtMsgType::QtWarningMsg, ctx, QStringLiteral("QML Error! %1").arg(error.description()));
    }
}

void Logging::messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &message)
{
    // Always use a single buffer for logging, to not allocate as much
    QByteArray& rawString{s_logMessagesBuffer.localData()};
    if (rawString.capacity() == 0 || rawString.capacity() > s_logMessagesBufferSize) {
        rawString.clear();
        rawString.reserve(s_logMessagesBufferSize);
    }
    rawString.resize(0);

    qint64 runtime = (QDateTime::currentMSecsSinceEpoch() - sRuntime);
    const QString& runtimeString{QStringLiteral("%1.%2")
        .arg(runtime / 1000ll, 4, 10, QChar(' '))
        .arg(runtime % 1000ll, 3, 10, QChar('0'))};
    rawString += runtimeString.toLatin1();

    switch (type) {
        case QtDebugMsg:    rawString += "|D|"; break;
        case QtInfoMsg:     rawString += "|I|"; break;
        case QtWarningMsg:  rawString += "|W|"; break;
        case QtCriticalMsg: rawString += "|C|"; break;
        case QtFatalMsg:    rawString += "|F|"; break;
    }

    QString messageReplacement;
    const char *category = sRegionPluginNames.value(context.category, context.category);
    if (qstrcmp(category, "qml") == 0 || qstrcmp(category, "js") == 0) {
        type = QtMsgType::QtWarningMsg;
        category = s_internalCategory;
        messageReplacement = QStringLiteral("Do not log to console with no tag: use `console.log(tag, '%1')`\n").arg(message).toUtf8();
    } else if (qstrcmp(category, "default") == 0) {
        type = QtMsgType::QtWarningMsg;
        category = s_internalCategory;
    }

    rawString.append(category, qMin(qstrlen(category), 32u));
    rawString += "| ";

    if (messageReplacement.isEmpty()) {
        rawString += message.toUtf8();
    } else {
        rawString += messageReplacement.toUtf8();
    }

#ifdef SPECCHIO_DEBUG
    if (context.file != nullptr) {
        const char *file = strrchr(context.file, '/');
        if (file == nullptr) {
            file = context.file;
        } else {
            file += 1; // Take that last slash out
        }
        rawString += " [";
        rawString += file;
        if (context.line > 0) {
            rawString += ':';
            rawString += QString::number(context.line);
        }
        rawString += ']';
    }
#endif

    rawString += '\n';
    std::cerr << rawString.constData() << std::flush;

    if (type == QtFatalMsg) {
        std::abort();
    }
}
