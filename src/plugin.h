/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#pragma once

#include <QtCore/QObject>
#include <QtCore/QSharedPointer>

class SpecchioPlugin;
class QPluginLoader;


class Plugin
{
public:
    using Ptr = QSharedPointer<Plugin>;

    Plugin(QString module, QString path, QString mainObject = QString(), QString version = QString(), QString library = QString());
    ~Plugin();

    bool isValid() const { return mValid; }
    bool load();

    const QString &module() const { return mModule; }
    const QString &path() const { return mPath; }
    const QString &mainObject() const { return mMainObject; }
    const QString &version() const { return mVersion; }
    const QString &library() const { return mLibrary; }

    Q_INVOKABLE inline const QString &name() const { return module(); }

private:
    void invalidate();

private:
    bool mLoaded = false;
    bool mValid = false;

    QString mModule;

    QString mPath;
    QString mMainObject;
    QString mVersion;
    QString mLibrary;

    QPluginLoader *mLoader = nullptr;
    SpecchioPlugin *mPluginInterface = nullptr;
};
