/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#pragma once

#include <QtCore/QMap>
#include <QtCore/QString>


class Paths
{
public:
    static const QStringList &getPluginsPaths() { return sPluginPaths; }
    static const QString &getSettingsPath() { return sSettingsBaseDir; }
    static const QString &getInternalSettingsPath() { return sInternalSettingsBaseDir; }
    static const QString &getDefaultSettingsFileName();

    static QString resource(const QString &path);

    // Testing hooks, will abort if called at runtime.
    static void setPluginsPaths(const QStringList &paths);
    static void setSettingsPath(const QString &path);
    static void setInternalSettingsPath(const QString &path);
    static void reset();

private:
    static QString sInternalSettingsBaseDir;
    static QString sSettingsBaseDir;
    static QStringList sPluginPaths;

private:
    Paths() {}
};
