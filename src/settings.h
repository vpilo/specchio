/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#pragma once

#include <QtCore/QSharedPointer>
#include <QtCore/QVariant>

class SettingsPrivate;


/**
 * Settings
 *
 * Settings in Specchio are saved in TOML format.
 *
 * The documentation for TOML can be found here: https://github.com/toml-lang/toml
 *
 * Settings can be loaded from a single file, or from two.
 * The first, the 'default', will be loaded normally. If specified, the 'user' settings file will override any settings
 * with the same name (and in the same place within the settings tree) that are present in the 'default' setting.
 *
 * For example, with a default configuration defined as following:
 * ```
 * [test]
 * ignored=false
 * key1="value"
 * ```
 * and an user configuration like this:
 * ```
 * [test]
 * key1="new"
 * ```
 * the resulting configuration will be:
 * ```
 * [test]
 * ignored=false
 * key1="new"
 * ```
 *
 * @see https://github.com/toml-lang/toml
 */
class Settings : public QVariantMap
{
public:
    using Ptr = QSharedPointer<Settings>;

public:
    Settings(const QString &defaultPath, const QString &userPath = QString());
    virtual ~Settings();

    bool valid() const { return mValid; }

    void dump() const;

private:
    bool parse(const QString &path);

private:
    bool mValid = false;

    QString mDefaultPath;
    QString mUserPath;

private:
    Q_DECLARE_PRIVATE(Settings);
    SettingsPrivate *d_ptr;
};
