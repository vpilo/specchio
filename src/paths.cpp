/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#include "paths.h"

#include <QtCore/QDir>
#include <QtCore/QLoggingCategory>


namespace {
    const QString     s_defaultInternalSettingsBaseDir { QStringLiteral(":/") };
    const QString     s_defaultSettingsBaseDir         { QDir::homePath() + QStringLiteral("/.specchio/") };
    const QString     s_defaultSettingsFileName        { QStringLiteral("default.toml") };
    const QStringList s_defaultPluginPaths             { SPECCHIO_PLUGIN_DIR, SPECCHIO_RESOURCE_DIR, s_defaultSettingsBaseDir };
} // namespace

Q_LOGGING_CATEGORY(spPaths, "Paths", QtInfoMsg);

QString Paths::sInternalSettingsBaseDir { s_defaultInternalSettingsBaseDir };
QString Paths::sSettingsBaseDir { s_defaultSettingsBaseDir };
QStringList Paths::sPluginPaths { s_defaultPluginPaths };

const QString &Paths::getDefaultSettingsFileName()
{
    return s_defaultSettingsFileName;
}

void Paths::setPluginsPaths(const QStringList &paths)
{
#ifdef SPECCHIO_TEST
    Q_FOREACH(const QString &path, paths) {
        Q_ASSERT(path.endsWith('/'));
    }
    sPluginPaths = paths;
#else
    Q_UNUSED(paths)
    qFatal("Attempt to call test hook");
#endif
}

void Paths::setSettingsPath(const QString &path)
{
#ifdef SPECCHIO_TEST
    Q_ASSERT(path.endsWith('/'));
    sSettingsBaseDir = path;
#else
    Q_UNUSED(path)
    qFatal("Attempt to call test hook");
#endif
}

void Paths::setInternalSettingsPath(const QString &path)
{
#ifdef SPECCHIO_TEST
    Q_ASSERT(path.endsWith('/'));
    sInternalSettingsBaseDir = path;
#else
    Q_UNUSED(path)
    qFatal("Attempt to call test hook");
#endif
}

void Paths::reset()
{
#ifdef SPECCHIO_TEST
    sPluginPaths = s_defaultPluginPaths;
    sSettingsBaseDir = s_defaultSettingsBaseDir;
    sInternalSettingsBaseDir = s_defaultInternalSettingsBaseDir;
#else
    qFatal("Attempt to call test hook");
#endif
}

QString Paths::resource(const QString& resourcePath)
{
    if (resourcePath.isEmpty()) {
        qCWarning(spPaths) << "Resource lookup, empty path given";
        return {};
    }

    if (QFile::exists(resourcePath)) {
        qCDebug(spPaths) << "Resource lookup, found directly:" << resourcePath;
        return resourcePath;
    }

    Q_FOREACH(const QString& pluginPath, Paths::getPluginsPaths()) {
        QString attemptedPath { pluginPath + '/' + resourcePath };
        if (QFile::exists(attemptedPath)) {
            qCDebug(spPaths) << "Resource lookup," << resourcePath << "found in" << pluginPath;
            return attemptedPath;
        }
    }

    QString globalSettingsPath { Paths::getSettingsPath() + '/' + resourcePath };
    if (QFile::exists(globalSettingsPath)) {
        qCDebug(spPaths) << "Resource lookup," << resourcePath << "found in global settings";
        return globalSettingsPath;
    }
    QString internalSettingsPath { Paths::getInternalSettingsPath() + '/' + resourcePath };
    if (QFile::exists(internalSettingsPath)) {
        qCDebug(spPaths) << "Resource lookup," << resourcePath << "found in internal settings";
        return internalSettingsPath;
    }

    qCWarning(spPaths) << "Resource lookup, not found! " << resourcePath;
    return {};
}
