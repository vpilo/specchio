/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#include "specchio.h"

#include <QtCore/QLoggingCategory>
#include <QtCore/QTimer>
#include <QtQml/QQmlContext>
#include <QtQml/QQmlEngine>


Q_LOGGING_CATEGORY(spInterf, "Interface", QtInfoMsg);

namespace {
    // Interval for the internal update timer, used for view refreshes and for all plugin update timers.
    constexpr int s_updateIntervalMs = 1000;
    // Time between view refreshes. Plugins can use this to refresh content that needs to visually change over time (like "x minutes ago").
    constexpr int s_viewRefreshIntervalSeconds = 30;
} // namespace

Specchio::Specchio(QObject *parent, Settings::Ptr settings)
:QObject(parent)
, mSettings(std::move(settings))
{
    // Validate rotation setting values
    if (!mSettings.isNull()) {
        const int originalRotation = mSettings->value(QStringLiteral("rotation"), 0).toInt();
        int rotation = originalRotation;
        if (rotation < 0) {
            rotation += 360;
        }
        if (rotation != 0 && rotation != 90 && rotation != 180 && rotation != 270) {
            qCWarning(spInterf) << "Invalid rotation setting '" << rotation << "'. Using 0°";
            rotation = 0;
        }
        if (originalRotation != rotation) {
            mSettings->insert(QStringLiteral("rotation"), rotation);
        }
    }

    connect(&mUpdateTimer, &QTimer::timeout, this, &Specchio::sendUpdates);
    mUpdateTimer.start(s_updateIntervalMs);

    connect(&mNetworkWatcher, &NetworkWatcher::connectedChanged, this, &Specchio::connectedChanged);
}

void Specchio::start()
{
    if (mStarted) {
        qCWarning(spInterf) << "Interface already in started state!";
        return;
    }
    qCInfo(spInterf) << "Starting";
    mStarted = true;
    emit starting();
}

void Specchio::stop()
{
    if (!mStarted) {
        qCWarning(spInterf) << "Interface not in started state!";
        return;
    }
    qCInfo(spInterf) << "Stopping";
    mStarted = false;
    emit stopping();
}

void Specchio::sendUpdates()
{
    qint64 now = QDateTime::currentSecsSinceEpoch();
    if (now - mLastViewRefresh > s_viewRefreshIntervalSeconds) {
        emit refreshView();
        mLastViewRefresh = now;
    }
    emit update(now);
}

QString Specchio::timeDistance(qint64 timestamp, bool isFullDay) const
{
    const QDateTime now { QDateTime::currentDateTimeUtc() };
    const QDateTime moment { QDateTime::fromMSecsSinceEpoch(timestamp, Qt::UTC) };
    const bool isFuture = now <= moment;
    qint64 distanceSeconds;
    if (isFuture) {
        distanceSeconds = now.secsTo(moment);
    } else {
        distanceSeconds = moment.secsTo(now);
    }

    constexpr qint64 s_oneMinute = 60;
    constexpr qint64 s_oneHour = s_oneMinute * 60;
    constexpr qint64 s_oneDay = s_oneHour * 24;
    constexpr qint64 s_oneWeek = s_oneDay * 7;
    constexpr qint64 s_oneMonth = s_oneDay * 31;
    constexpr qint64 s_oneYear = s_oneDay * 365;
    const qint64 distanceMinutes = distanceSeconds / s_oneMinute;
    const qint64 distanceHours = distanceSeconds / s_oneHour;
    const qint64 distanceDays = distanceSeconds / s_oneDay;
    const qint64 distanceWeeks = distanceSeconds / s_oneWeek;
    const qint64 distanceMonths = distanceSeconds / s_oneMonth;
    const qint64 distanceYears = distanceSeconds / s_oneYear;

    QString templateStr;
    if (distanceYears > 0) {
        templateStr = tr("%Ln year(s)", "distance in time", distanceYears);
    } else if (distanceMonths > 0) {
        templateStr = tr("%Ln month(s)", "distance in time", distanceMonths);
    } else if (distanceWeeks > 0) {
        templateStr = tr("%Ln week(s)", "distance in time", distanceWeeks);
    } else if (distanceDays > 0) {
        if (distanceDays == 1) {
          if (isFuture) {
            return tr("tomorrow", "distance in time");
          }
          return tr("yesterday", "distance in time");
        }
        templateStr = tr("%Ln day(s)", "distance in time", distanceDays);
    } else if (distanceHours > 0) {
        templateStr = tr("%Ln hour(s)", "distance in time", distanceHours);
    } else if (distanceMinutes > 0) {
        templateStr = tr("%Ln minute(s)", "distance in time", distanceMinutes);
    } else {
        if (isFullDay) {
            return tr("today", "distance in time");
        }
        return tr("now", "distance in time");
    }
    if (isFuture) {
        return tr("in %1", "future events, eg. 'in 5 minutes'").arg(templateStr);
    }
    return tr("%1 ago", "past events, eg. '5 minutes ago'").arg(templateStr);
}
