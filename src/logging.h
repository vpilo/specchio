/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#pragma once

#include "types.h"

#include <QtCore/QByteArray>
#include <QtCore/QLoggingCategory>
#include <QtCore/QHash>
#include <QtCore/QList>
#include <QtQml/QQmlError>

class QQmlEngine;

class Logging
{
public:
    static void initialize();
    static void setRegionName(Types::Region region, const QString &name);
    static void printQmlErrors(const QString &plugin, const QList<QQmlError> &errors);

    static void setupQmlEngine(QQmlEngine *engine);

private:
    static void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &message);

private:
    static qint64 sRuntime;
    static QHash<QByteArray,QByteArray> sRegionPluginNames;

private:
    Logging() = default;
};
