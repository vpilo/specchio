/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#pragma once

#include "plugin.h"
#include "settings.h"
#include "types.h"

#include <QtQuick/QQuickItem>
#include <QtCore/QString>
#include <QtCore/QVariantMap>

class QQmlIncubator;
class SmartTimer;

class Region : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QQuickItem* regionQmlItem READ regionQmlItem WRITE setRegionQmlItem)
    Q_PROPERTY(QQuickItem* pluginObject READ pluginObject WRITE setPluginObject)

    Q_PROPERTY(Types::Region regionId READ getRegionId WRITE setRegionId)
    Q_PROPERTY(QString pluginName READ getPluginName CONSTANT)
    Q_PROPERTY(QString pluginPath READ getPluginPath CONSTANT)
    Q_PROPERTY(QString name READ name CONSTANT)
    Q_PROPERTY(bool inUse READ isInUse NOTIFY inUseChanged)
    Q_PROPERTY(QString loadingStatusMessage READ loadingStatusMessage NOTIFY inUseChanged)

    Q_PROPERTY(bool positionedAtLeft READ positionedAtLeft CONSTANT)
    Q_PROPERTY(bool positionedAtRight READ positionedAtRight CONSTANT)
    Q_PROPERTY(bool positionedAtTop READ positionedAtTop CONSTANT)
    Q_PROPERTY(bool positionedAtBottom READ positionedAtBottom CONSTANT)
    Q_PROPERTY(bool positionedAtCenter READ positionedAtCenter CONSTANT)

public:
    Region(QObject *parent = nullptr);
    virtual ~Region() override;

    bool populate(Plugin::Ptr &plugin, Settings::Ptr &settings);
    bool activate();
    void setRegionId(Types::Region id);
    QQuickItem *regionQmlItem() const { return mRegionQmlItem; }
    void setRegionQmlItem(QQuickItem *item) { mRegionQmlItem = item; }
    QQuickItem *pluginObject() const { return mPluginObject; }
    void setPluginObject(QQuickItem *obj) { mPluginObject = obj; }

    QString getPluginName() const;
    QString getPluginPath() const;
    QVariantMap getSettings() const;
    QString name() const;
    Types::Region getRegionId() const { return mRegionId; }
    bool isInUse() const { return mIsInUse; }
    QString loadingStatusMessage() const { return mLoadingStatusMessage; }

    Q_INVOKABLE void sendBroadcast(const QString &name, const QVariant &extras = QVariant());
    Q_INVOKABLE SmartTimer* createTimer(const QString &id, quint32 intervalSeconds, bool triggerOnStart);
    Q_INVOKABLE SmartTimer* createNetworkTimer(const QString &id, quint32 intervalSeconds, bool triggerOnStart);
    Q_INVOKABLE SmartTimer *getTimer(const QString &id);
    Q_INVOKABLE void removeTimer(const QString &id);

    bool positionedAtLeft() const;
    bool positionedAtRight() const;
    bool positionedAtTop() const;
    bool positionedAtBottom() const;
    bool positionedAtCenter() const;

public slots:
    void timerDestroyed(QObject *timer);

    void onInterfaceStarting();
    void onInterfaceStopping();
    void receiveBroadcast(const QString &name, const QVariant &extras);

private:
    template<class T>
    T* internalCreateTimer(const QString &id, quint32 intervalSeconds, bool triggerOnStart);

    void setStatusError(const QString& error);
    void setStatusMessage(const QString& message);

signals:
    void timerRelay(qint64 now);
    void inUseChanged();
    // Used by plugins to receive broadcasts
    void broadcast(const QString &name, const QVariant &extras);
    // Sends out broadcasts
    void broadcastRelay(Types::Region region, const QString &name, const QVariantMap &extras);

    void onConnectedChanged(bool isOnline);

private:
    Types::Region mRegionId;
    QQuickItem *mRegionQmlItem = nullptr;
    QQuickItem *mPluginObject = nullptr;
    Plugin::Ptr mPlugin = nullptr;
    Settings::Ptr mSettings = nullptr;

    bool mIsInUse = false;
    QString mLoadingStatusMessage;

    QQmlIncubator *mIncubator;
    QQuickItem *mInstance = nullptr;

    bool mIsInterfaceStarted = false;
    std::list<SmartTimer*> mTimers;
};
