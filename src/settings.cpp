/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#include "settings.h"

#include <toml11/toml.hpp>

#include <QtCore/QDir>
#include <QtCore/QFileInfo>
#include <QtCore/QLoggingCategory>
#include <QtCore/QTemporaryFile>
#include <QtCore/QTime>

#include <fstream>

Q_LOGGING_CATEGORY(spSettings, "Settings", QtInfoMsg);


class SettingsPrivate {
    SettingsPrivate(Settings *q) : q_ptr(q) {}
    bool import(std::ifstream &ifs, const std::string &fileName);
    void convertTable(const toml::table& value, QVariantMap& dest);
    QVariant convertValue(const toml::value &value, const QString &key);
    void dump(const QString &beginKey, const QVariantMap &beginValue) const;

    Q_DECLARE_PUBLIC(Settings);
    Settings *q_ptr;
};


Settings::Settings(const QString& defaultPath, const QString& userPath)
: mDefaultPath(defaultPath)
, mUserPath(userPath)
, d_ptr(new SettingsPrivate(this))
{
    if (!QFile::exists(defaultPath)) {
        qCWarning(spSettings) << "Default settings file not found:" << defaultPath;
        return;
    }

    // Not loading successfully the default settings is fatal, but it's allowed for them to be empty
    if (!parse(defaultPath)) {
        qCWarning(spSettings) << "Unable to load default settings:" << defaultPath;
        return;
    }

    if (!userPath.isEmpty()) {
        QFileInfo userFileInfo { userPath };
        if (userFileInfo.exists(userPath)) {
            if (!parse(userPath)) {
                qCWarning(spSettings) << "Unable to load user settings:" << userPath;
            }
        } else {
            qCDebug(spSettings) << "User settings file not found:" << userPath << "copying defaults";
            if (!QDir{}.mkpath(userFileInfo.absolutePath())) {
                qCDebug(spSettings) << "Unable to create directory for defaults";
            } else if (!QFile::copy(defaultPath, userPath)) {
                qCDebug(spSettings) << "Copying defaults failed";
            }
        }
    }

    mValid = true;
}

Settings::~Settings()
{
    delete d_ptr;
}

// Transforms settings into variant map for QML consumption
bool Settings::parse(const QString &path)
{
    QTemporaryFile resourceFile;
    std::string tomlFileName;

    // The standard library doesn't know Qt resources
    if (path.startsWith(QStringLiteral(":/"), Qt::CaseInsensitive)
        ||  path.startsWith(QStringLiteral("qrc://"), Qt::CaseInsensitive)) {
        if (!resourceFile.open()) {
            qCWarning(spSettings) << "Unable to create temporary file for resource settings opening";
            return false;
        }
        QFile source(path);
        if (!source.open(QIODevice::ReadOnly)) {
            qCWarning(spSettings) << "Unable to open resource settings file";
            return false;
        }
        resourceFile.write(source.readAll());
        source.close();
        resourceFile.close();

        tomlFileName = resourceFile.fileName().toStdString();
    } else {
        tomlFileName = path.toStdString();
    }

    if (tomlFileName.empty()) {
        return false;
    }

    std::ifstream ifs(tomlFileName, std::ios_base::binary);
    if (!ifs.good()) {
        return false;
    }

    Q_D(Settings);
    return d->import(ifs, tomlFileName);
}

void Settings::dump() const
{
    qCInfo(spSettings) << "Settings dump";
    qCInfo(spSettings) << "- Default settings path:" << mDefaultPath;
    qCInfo(spSettings) << "- User settings path:" << (mUserPath.isEmpty() ? "None" : mUserPath);
    Q_D(const Settings);
    if (empty()) {
        qCInfo(spSettings) << "Nothing inside";
        return;
    }

    d->dump(QString(), *this);
}

bool SettingsPrivate::import(std::ifstream &ifs, const std::string &fileName)
{
    toml::value data;

    try {
        data = toml::parse(ifs, fileName);
    } catch (const toml::syntax_error &err) {
        qCWarning(spSettings) << "Parse error:" << err.what();
        return false;
    }

    Q_Q(Settings);
    convertTable(data.as_table(), *q);
    return true;
}

void SettingsPrivate::convertTable(const toml::table &value, QVariantMap &dest)
{
    for(const auto& item : value) {
        const QString key { QString::fromStdString(item.first) };
        if (item.second.is_table()) {
            QVariantMap group { dest[key].toMap() };
            convertTable(item.second.as_table(), group);
            dest[key] = group;
        } else {
            dest[key] = convertValue(item.second, key);
        }
    }
}

QVariant SettingsPrivate::convertValue(const toml::value &value, const QString &key)
{
    switch (value.type()) {
        case toml::value_t::boolean:
            return value.as_boolean();
        case toml::value_t::integer:
            return static_cast<qint64>(value.as_integer());
        case toml::value_t::floating:
            return value.as_floating();
        case toml::value_t::string:
            return QString::fromStdString(value.as_string());
        case toml::value_t::offset_datetime: {
            const toml::offset_datetime odt = value.as_offset_datetime();
            return QDateTime {
                { odt.date.year, odt.date.month, odt.date.day },
                { odt.time.hour, odt.time.minute, odt.time.second, odt.time.millisecond },
                Qt::OffsetFromUTC,
                odt.offset.hour * 3600 + odt.offset.minute * 60
            };
        }
        case toml::value_t::local_datetime: {
            const toml::local_datetime dt = value.as_local_datetime();
            return QDateTime {
                { dt.date.year, dt.date.month, dt.date.day },
                { dt.time.hour, dt.time.minute, dt.time.second, dt.time.millisecond }
            };
        }
        case toml::value_t::local_date: {
            const toml::local_date date = value.as_local_date();
            return QDate { date.year, date.month, date.day };
        }
        case toml::value_t::local_time: {
            const toml::local_time time = value.as_local_time();
            // Micro- and nano-seconds can't be imported in QTime, hope nobody needs THAT precision on a mirror..
            return QTime { time.hour, time.minute, time.second, time.millisecond };
        }
        case toml::value_t::array: {
            const toml::array &array { value.as_array() };
            QVariantList list;
            for (const auto &item : array) {
                list.append(convertValue(item, key));
            }
            return list;
        }
        case toml::value_t::table: {
            const toml::table &sourceTable { value.as_table() };
            QVariantMap table;
            convertTable(sourceTable, table);
            return table;
        }
        case toml::value_t::empty:
        default:
            qCWarning(spSettings) << "Unknown value read:" << key;
            return QVariant();
    }
}

void SettingsPrivate::dump(const QString &beginKey, const QVariantMap &beginValue) const
{
    const auto &keys { beginValue.keys() };

    const QString root { beginKey.isEmpty() ? QString() : beginKey + "/" };

    Q_FOREACH(const auto &key, keys) {
        const QVariant &value { beginValue.value(key) };

        qCInfo(spSettings).noquote().nospace()
        << "/" << root + key
            << ": \""
            << value.toString()
            << "\"  [" << value.type() << "]";

        if (value.type() == QVariant::Map && !value.toMap().empty()) {
            dump(root + key, value.toMap());
        }
    }
}
