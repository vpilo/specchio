/*
 * Copyright 2020 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#include "networkwatcher.h"

#include <QtCore/QLoggingCategory>
#include <QtCore/QString>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>

Q_LOGGING_CATEGORY(spNW, "Network");

namespace {
    // Time between network checks.
    constexpr int s_netCheckIntervalMsec = 30 * 1000;
    // Service to query to find out if networking is actually available.
    const QString s_dnsTestUrl { QStringLiteral("https://one.one.one.one/") };
} // namespace

NetworkWatcher::NetworkWatcher()
{
    connect(&mNetMan, &QNetworkAccessManager::finished,
            this, &NetworkWatcher::verifyResponse);

    mNetworkCheckTimer.setInterval(s_netCheckIntervalMsec);
    connect(&mNetworkCheckTimer, &QTimer::timeout, this, &NetworkWatcher::watch);
    mNetworkCheckTimer.start();

    // Only connect the online status update signal after the initial scan
    connect(&mNetworkConfig, &QNetworkConfigurationManager::updateCompleted, [this]() {
        mNetworkConfig.disconnect();
        connect(&mNetworkConfig, &QNetworkConfigurationManager::onlineStateChanged, this, &NetworkWatcher::watch);
        watch();
    });
    mNetworkConfig.updateConfigurations();
}

void NetworkWatcher::watch()
{
    bool isOnline = mNetworkConfig.isOnline();
    if (!isOnline) {
        sendUpdate(false);
        return;
    }

    QUrl url(s_dnsTestUrl);
    QNetworkReply *reply = mNetMan.get(QNetworkRequest(url));
    if (reply == nullptr || !reply->isRunning()) {
        verifyResponse(reply);
    }
}

void NetworkWatcher::verifyResponse(QNetworkReply* reply)
{
    if (reply == nullptr) {
        sendUpdate(false);
        return;
    }

    bool isOnline = reply->error() == QNetworkReply::NoError && reply->bytesAvailable() > 0;
    if (!isOnline) {
        qCDebug(spNW) << "Got reply:" << reply->error();
    }
    sendUpdate(isOnline);

    reply->abort();
    reply->deleteLater();
}

void NetworkWatcher::sendUpdate(bool isOnline)
{
    if (isOnline != mLastOnlineState) {
        mLastOnlineState = isOnline;
        qCInfo(spNW) << "Internet connection now" << (mLastOnlineState ? "available" : "unavailable");
        emit connectedChanged(mLastOnlineState);
    }
}

