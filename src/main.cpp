/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#include "loader.h"
#include "logging.h"
#include "paths.h"
#include "registry.h"
#include "signals_handler.h"
#include "specchio.h"

#ifdef SPECCHIO_DEBUG
#define QT_QML_DEBUG
#endif

#include <QtCore/QDebug>
#include <QtCore/QUrl>
#include <QtGui/QGuiApplication>
#include <QtQml/QQmlApplicationEngine>


int main(int argc, char **argv)
{
    // Force running on EGL. This doesn't preclude running on desktop (Qt will run a nested compositor).
    qputenv("QT_QPA_PLATFORM", "eglfs");

    // ShareOpenGLContexts is needed for using the threaded renderer on Nvidia EGLStreams
    QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts, true);
    // Scale contents appropriately on displays with high pixel density
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling, true);

    QCoreApplication::setOrganizationName(QStringLiteral("ColdShock Designs"));
    QCoreApplication::setOrganizationDomain(QStringLiteral("specchio.com"));
    QCoreApplication::setApplicationName(QStringLiteral("Specchio"));

    Logging::initialize();
    QGuiApplication app(argc, argv);
    SignalsHandler signalHandler;

    Registry registry;
    Specchio interface { &app, registry.settings() };
    registry.setInterface(&interface);

    Loader loader { &app, &registry, &interface };

    Logging::setupQmlEngine(loader.engine());

    loader.translate();
    loader.scanPlugins();

    if (registry.pluginsCount() == 0) {
        qCritical() << "No plugins found! This means Specchio is not installed properly.";
        qCritical() << "Please check whether the installation is located in" << SPECCHIO_INSTALL_DIR;
        qCritical() << "and plugins can be found in" << SPECCHIO_PLUGIN_DIR;
        return 1;
    }
    if (!loader.initialize(Paths::resource(QStringLiteral("DisplayCompositor.qml")))) {
        qCritical() << "Initialization failed";
        return 2;
    }
    if (!loader.loadSavedPlugins()) {
        qCritical() << "Plugin initialization failed";
        return 3;
    }

    return QGuiApplication::exec();
}
