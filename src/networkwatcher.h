/*
 * Copyright 2020 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#pragma once

#include <QtCore/QObject>
#include <QtCore/QTimer>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkConfigurationManager>

class QNetworkReply;

/**
 * Watches over the network to notify when an internet connection changes availability.
 *
 * The first network availability signal is sent as soon as possible, and then periodically a new test is made.
 * Status updates are only sent on change.
 */
class NetworkWatcher : public QObject
{
    Q_OBJECT

public:
    NetworkWatcher();

    bool isConnected() const { return mLastOnlineState; }

    void sendUpdate(bool isOnline);

private slots:
    void verifyResponse(QNetworkReply* reply);
    void watch();

signals:
    void connectedChanged(bool isOnline);

private:
    bool mLastOnlineState = false;

    QTimer mNetworkCheckTimer;

    QNetworkAccessManager mNetMan;
    QNetworkConfigurationManager mNetworkConfig;
};
