/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#include "registry.h"
#include "paths.h"
#include "region.h"
#include "specchio.h"

#include <QtCore/QDir>
#include <QtCore/QLoggingCategory>
#include <QtCore/QSignalBlocker>


namespace {
    const QString s_globalSettingsFileName { QStringLiteral("specchio.toml") };
} // namespace

Q_LOGGING_CATEGORY(spRegistry, "Registry", QtInfoMsg);

Registry::Registry()
{
    // Loading settings is done early to initialize properly the objects in the QML scene
    const QDir settingsDir { Paths::getSettingsPath() };
    if (!settingsDir.exists()) {
        qCWarning(spRegistry) << "Settings directory" << Paths::getSettingsPath() << "not found";
        if (!settingsDir.mkpath(Paths::getSettingsPath())) {
            qCWarning(spRegistry) << "Unable to create settings directory!";
        }
    }
    (void) settingsDir.mkpath(QStringLiteral("plugins"));

    mSettings = Settings::Ptr::create(*new Settings(Paths::getInternalSettingsPath() + Paths::getDefaultSettingsFileName(),
                                                    Paths::getSettingsPath() + s_globalSettingsFileName));
    if (mSettings == nullptr || mSettings->empty()) {
        qCWarning(spRegistry) << "Unable to load global settings!";
        mSettings.clear();
    }
}

void Registry::sendBroadcast(Types::Region caster, const QString& name, const QVariantMap& extras)
{
    if(name.startsWith(QStringLiteral("Specchio"), Qt::CaseInsensitive)) {
        qCWarning(spRegistry) << "Cannot send reserved broadcast name" << name;
        return;
    }
    Q_ASSERT(mRegions.contains(caster));
    const Region *casterRegion = mRegions[caster];
    QVariantMap newExtras { extras };
    newExtras[QStringLiteral("fromPlugin")] = casterRegion->getPluginName();
    newExtras[QStringLiteral("fromRegion")] = Types::regionName(caster);

    // Prevent broadcast loops
    QSignalBlocker blocker { mRegions[caster] };

    if (extras.empty()) {
        qCDebug(spRegistry).noquote().nospace() << casterRegion->name() << " sends broadcast " << name;
    } else {
        qCDebug(spRegistry).noquote().nospace() << casterRegion->name() << " sends broadcast " << name << " with extras " << extras;
    }
    Types::forEachRegion([&](Types::Region region) {
        if (region == caster) {
            return;
        }
        mRegions[region]->receiveBroadcast(name, newExtras);
    });
}

Registry::~Registry()
{
    mSettings.clear();
    mRegions.clear();
    mPlugins.clear();
}

bool Registry::isValid() const
{
    return !mSettings.isNull() && mInterface != nullptr;
}

void Registry::addPlugin(const QString &name, const Plugin::Ptr &plugin)
{
    Q_ASSERT(!name.isEmpty());
    Q_ASSERT(!plugin.isNull());
    Q_ASSERT(plugin->isValid());
    mPlugins.insert(name, plugin);
}

void Registry::setInterface(Specchio *instance)
{
    Q_ASSERT(instance != nullptr);
    Q_ASSERT(mInterface == nullptr);
    if (mInterface != nullptr) {
        return;
    }
    mInterface = instance;
}

void Registry::setRegions(const Registry::RegionMap &map)
{
    Q_ASSERT(!map.empty());
    Q_ASSERT(mRegions.empty());
    if (!mRegions.empty()) {
        return;
    }
    mRegions = map;
}

void Registry::prepare()
{
    Q_ASSERT(!mRegions.empty());
    Q_ASSERT(mInterface != nullptr);
    for(const auto &region : qAsConst(mRegions)) {
        if (region->isInUse()) {
            connect(region, &Region::broadcastRelay, this, &Registry::sendBroadcast);
            connect(mInterface, &Specchio::update, region, &Region::timerRelay);
            connect(mInterface, &Specchio::starting, region, &Region::onInterfaceStarting);
            connect(mInterface, &Specchio::stopping, region, &Region::onInterfaceStopping);
            connect(mInterface, &Specchio::connectedChanged, region, &Region::onConnectedChanged);
        }
    }
}
