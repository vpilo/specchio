/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#pragma once

#include <QtCore/QObject>

#include <unistd.h>
#include <signal.h>

class QGuiApplication;


class SignalsHandler : public QObject
{
    Q_OBJECT

public:
    explicit SignalsHandler();
    virtual ~SignalsHandler();

private:

    static void handler(int signal);

private:

    using SignalHandlerPtr = void(*)(int);

    struct sigaction mSigInt;
    struct sigaction mSigTerm;
};
