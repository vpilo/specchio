/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#include "region.h"
#include "logging.h"
#include "networksmarttimer.h"
#include "plugin.h"
#include "smarttimer.h"

#include <QtCore/QLoggingCategory>
#include <QtCore/QString>
#include <QtCore/QTime>
#include <QtQml/QQmlContext>
#include <QtQml/QQmlEngine>
#include <QtQml/QQmlIncubator>


Q_LOGGING_CATEGORY(spReg, "Region", QtInfoMsg);

Region::Region(QObject *parent)
: QObject(parent)
, mIncubator(new QQmlIncubator { QQmlIncubator::Asynchronous })
{
}

Region::Region::~Region()
{
    delete mIncubator;
}

bool Region::populate(Plugin::Ptr &plugin, Settings::Ptr &settings)
{
    if (objectName().isEmpty()) {
        qCCritical(spReg) << "Name of region is not set";
        return false;
    }
    if (plugin.isNull()) {
        qCWarning(spReg) << "Invalid plugin";
        setStatusError(tr("Unknown plugin"));
        return false;
    }
    if (settings.isNull() || !settings->valid()) {
        qCWarning(spReg) << "Invalid plugin settings";
        setStatusError(tr("Invalid plugin settings"));
        return false;
    }
    mPlugin = std::move(plugin);
    mSettings = std::move(settings);

    Logging::setRegionName(mRegionId, name());

    if (!mPlugin->load()) {
        qCWarning(spReg) << "Plugin loading failed";
        setStatusError(tr("Load failed"));
        return false;
    }

    return true;
}

bool Region::activate()
{
    Q_ASSERT(!mPlugin.isNull());
    Q_ASSERT(mPlugin->isValid());
    Q_ASSERT(!mSettings.isNull());
    Q_ASSERT(mSettings->valid());

    QTime loadTimer;
    loadTimer.start();

    if (mPluginObject == nullptr) {
        qCWarning(spReg) << name() << "QML plugin alias not set!";
        return false;
    }

    if (mRegionQmlItem == nullptr) {
        qCWarning(spReg) << name() << "Region QML item not set!";
        return false;
    }
    QQmlEngine *engine = qmlEngine(mRegionQmlItem);
    if (engine == nullptr) {
        qCWarning(spReg) << name() << "Invalid region QML item!";
        return false;
    }

    QQmlComponent component(engine);
    component.setData(QStringLiteral("import %1 %2 ; %3 { anchors.fill: parent }")
            .arg(mPlugin->module(), mPlugin->version(), mPlugin->mainObject()
        ).toLatin1(), QUrl());
    if (component.isError()) {
        Logging::printQmlErrors(name(), component.errors());
        return false;
    }

    // Relay broadcasts
    connect(this, SIGNAL(broadcast(const QString&, const QVariant&)),
            mPluginObject, SIGNAL(broadcast(const QString&, const QVariant&)));

    // Give access to the logging tag to the plugin
    const auto loggingTag = mRegionQmlItem->findChild<QObject*>("tag");
    if (loggingTag == nullptr) {
        qCWarning(spReg) << "Plugin logging connection failed";
        setStatusError(tr("Start failed"));
        return false;
    }

    auto context = new QQmlContext(engine->rootContext(), this);
    context->setContextProperty(QStringLiteral("Plugin"), mPluginObject);
    context->setContextProperty(QStringLiteral("settings"), *mSettings);
    context->setContextProperty(QStringLiteral("tag"), loggingTag);

    /*
     * A QQmlIncubator is used to be able to trap errors coming from loading this plugin and show them properly as
     * coming from this specific instance.
     * The Synchronous loading mode is necessary to immediately have feedback about the loading. At loading time, async
     * mode would not really add anything else but make startup a tiny bit faster.
     * Currently it seems unnecessary also because it involves advanced uses of QQmlIncubators (the class doesn't have
     * any signals to report on its progress and must be polled).
     */
    component.create(*mIncubator, context);
    mIncubator->forceCompletion();
    if (mIncubator->isError()) {
        Logging::printQmlErrors(name(), mIncubator->errors());
        return false;
    }

    mInstance = qobject_cast<QQuickItem*>(mIncubator->object());
    if (mInstance == nullptr) {
        setStatusError(tr("Start failed"));
        return false;
    }
    mInstance->setParent(this);
    mInstance->setParentItem(mRegionQmlItem);

    setStatusMessage(tr("OK"));

    qCDebug(spReg) << "Loaded" << name() << "in" << loadTimer.elapsed() << "ms";
    return true;
}

SmartTimer *Region::createTimer(const QString& id, quint32 intervalSeconds, bool triggerOnStart)
{
    return internalCreateTimer<SmartTimer>(id, intervalSeconds, triggerOnStart);
}

SmartTimer *Region::createNetworkTimer(const QString& id, quint32 intervalSeconds, bool triggerOnStart)
{
    auto newTimer = internalCreateTimer<NetworkSmartTimer>(id, intervalSeconds, triggerOnStart);
    if (newTimer != nullptr) {
        connect(this, &Region::onConnectedChanged, newTimer, &NetworkSmartTimer::onConnectedChanged);
    }
    return newTimer;
}

template<class T>
T *Region::internalCreateTimer(const QString& id, quint32 intervalSeconds, bool triggerOnStart)
{
        if (intervalSeconds == 0) {
        return nullptr;
    }
    if (getTimer(id) != nullptr) {
        qCWarning(spReg) << name() << "Timer with id" << id << "already present";
        return nullptr;
    }

    static_assert(std::is_base_of<SmartTimer, T>::value, "internalCreateTimer() not called with a SmartTimer type");

    auto *newTimer = new T(this, id, intervalSeconds, triggerOnStart);
    connect(this, &Region::timerRelay, newTimer, &SmartTimer::update);
    connect(newTimer, &QObject::destroyed, this, &Region::timerDestroyed);

    // Timers should only receive updates since the interface gets started.
    newTimer->blockSignals(!mIsInterfaceStarted);

    mTimers.push_back(newTimer);
    return newTimer;
}

SmartTimer *Region::getTimer(const QString& id)
{
    auto it = std::find_if(mTimers.begin(), mTimers.end(), [id](const SmartTimer* item) {
        return item->id() == id;
    });

    return it != mTimers.end() ? *it : nullptr;
}

void Region::removeTimer(const QString& id)
{
    auto found = getTimer(id);
    if (found == nullptr) {
        qCWarning(spReg) << name() << "Invalid call to removeTimer() with argument" << id;
        return;
    }
    mTimers.remove(found);
    delete found;
}

void Region::timerDestroyed(QObject* timer)
{
    auto timerChecked = qobject_cast<SmartTimer*>(timer);
    if (timerChecked == nullptr) {
        qCWarning(spReg) << name() << "Invalid call to timerDestroyed() with argument" << timer;
        return;
    }
    removeTimer(timerChecked->id());
}

void Region::onInterfaceStarting()
{
    mIsInterfaceStarted = true;
    std::for_each(mTimers.begin(), mTimers.end(), [](SmartTimer* item) {
        item->blockSignals(false);
    });
}

void Region::onInterfaceStopping()
{
    mIsInterfaceStarted = false;
    std::for_each(mTimers.begin(), mTimers.end(), [](SmartTimer* item) {
        item->blockSignals(true);
    });
}

void Region::receiveBroadcast(const QString& name, const QVariant& extras)
{
    if (signalsBlocked()) {
        qCWarning(spReg) << this->name() << "Broadcast loop detected, dropping received broadcast" << name;
        return;
    }
    emit broadcast(name, extras);
}

void Region::sendBroadcast(const QString& name, const QVariant& extras)
{
    emit broadcastRelay(mRegionId, name, extras.toMap());
}

void Region::setStatusMessage(const QString& message)
{
    mIsInUse = true;
    mLoadingStatusMessage = message;
    emit inUseChanged();
}

void Region::setStatusError(const QString& error)
{
    mIsInUse = false;
    mLoadingStatusMessage = error;
    emit inUseChanged();
}

void Region::setRegionId(Types::Region id)
{
    mRegionId = id;
    setObjectName(Types::regionName(mRegionId).toUtf8().constData());
}

QString Region::getPluginName() const
{
    if (mPlugin == nullptr) {
        return tr("None");
    }
    return mPlugin->name();
}

QString Region::getPluginPath() const
{
    if (mPlugin == nullptr) {
        return {};
    }
    return mPlugin->path();
}

QVariantMap Region::getSettings() const
{
    if (mPlugin == nullptr || mSettings.isNull()) {
        return {};
    }
    return *mSettings;
}

QString Region::name() const
{
    const QString name { Types::regionName(mRegionId) };
    if (mPlugin == nullptr) {
        return name;
    }
    return QStringLiteral("%1@%2").arg(getPluginName(), name);
}

bool Region::positionedAtLeft() const
{
    return mRegionId == Types::Region::TopLeft ||
        mRegionId == Types::Region::BottomLeft;
}

bool Region::positionedAtRight() const
{
    return mRegionId == Types::Region::TopRight ||
        mRegionId == Types::Region::BottomRight;
}

bool Region::positionedAtTop() const
{
    return mRegionId == Types::Region::TopLeft ||
        mRegionId == Types::Region::TopCenter ||
        mRegionId == Types::Region::TopRight ||
        mRegionId == Types::Region::TopBar ||
        mRegionId == Types::Region::UpperThird;
}

bool Region::positionedAtBottom() const
{
    return mRegionId == Types::Region::BottomLeft ||
        mRegionId == Types::Region::BottomCenter ||
        mRegionId == Types::Region::BottomRight ||
        mRegionId == Types::Region::BottomBar ||
        mRegionId == Types::Region::LowerThird;
}

bool Region::positionedAtCenter() const
{
    return mRegionId == Types::Region::TopCenter ||
        mRegionId == Types::Region::UpperThird ||
        mRegionId == Types::Region::MiddleCenter ||
        mRegionId == Types::Region::LowerThird ||
        mRegionId == Types::Region::BottomCenter;
}
