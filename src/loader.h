/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#pragma once

#include "plugin_interface.hpp"
#include "plugin.h"
#include "types.h"

#include <QtCore/QMap>
#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QTranslator>
#include <QtCore/QVariantMap>
#include <QtCore/QVector>

class QQmlApplicationEngine;
class Registry;
class Specchio;

class Loader : public QObject
{
    Q_OBJECT

public:
    Loader(QObject *parent, Registry *registry, Specchio *interface);

    void translate();
    bool initialize(const QString &mainQmlFile);
    void scanPlugins();
    bool loadSavedPlugins();

    QQmlApplicationEngine *engine() { return mEngine; }

private:
    bool initializeFonts();
    bool loadFont(const QString &fontFile, bool loadApplicationFont);

    Plugin::Ptr parsePlugin(const QString &qmldirPath, const QString &pluginPath);
    bool loadPlugin(Types::Region region, const QString &name);

private Q_SLOTS:
    void onQuit();

private:
    Registry *mRegistry;
    QQmlApplicationEngine *mEngine;

    QTranslator translator;

#ifdef SPECCHIO_TEST
    friend class LoaderTests;
#endif
};
