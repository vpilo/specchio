/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#include "plugin.h"
#include "plugin_interface.hpp"

#include <QtCore/QLoggingCategory>
#include <QtCore/QPluginLoader>

Q_LOGGING_CATEGORY(spPlugin, "Plugins", QtInfoMsg);

Plugin::Plugin(QString module, QString path, QString mainObject, QString version, QString library)
: mModule(std::move(module))
, mPath(std::move(path))
, mMainObject(std::move(mainObject))
, mVersion(std::move(version))
, mLibrary(std::move(library))
{
    QRegExp nameRegex { QStringLiteral("[^a-z0-9._]|\\.\\.|__"), Qt::CaseInsensitive };

    if (mModule.isEmpty() || mModule.contains(nameRegex)) {
        qCWarning(spPlugin) << "Invalid plugin name:" << mModule;
        return;
    }

    if (!mLibrary.isEmpty()) {
        mLoader = new QPluginLoader { mLibrary };
        QJsonObject metaData = mLoader->metaData();
        if (metaData.value(QStringLiteral("IID")).toString() != SPECCHIO_PLUGIN_INTERFACE_STRING) {
            qCWarning(spPlugin).noquote().nospace() << mModule << "| Native library seems not to be a Specchio plugin! "
                "Library ID:" << metaData.value(QStringLiteral("IID")).toString();
            return;
        }
    }
    mValid = true;
}

Plugin::~Plugin()
{
    invalidate();
}

void Plugin::invalidate()
{
    mValid = false;
    delete mPluginInterface;
    mPluginInterface = nullptr;

    if (mLoader != nullptr) {
        mLoader->unload();
        mLoader->deleteLater();
    }
    mLoader = nullptr;
}

bool Plugin::load()
{
    if (!isValid()) {
        return false;
    }
    if (mLoaded) {
        // Load libraries only once if the plugin is used in more than one region
        return true;
    }
    if (mLibrary.isEmpty()) {
        mLoaded = true;
        return true;
    }

    qCDebug(spPlugin).noquote().nospace() << mModule << "| Loading native library " << mLibrary;

    Q_ASSERT(mLoader != nullptr);
    Q_ASSERT(!mLibrary.isEmpty());

    if (!mLoader->load()) {
        qCWarning(spPlugin).noquote().nospace() << mModule << "| Native library cannot be loaded";
        qCWarning(spPlugin) << "Library path:" << mLibrary;
        invalidate();
        return false;
    }
    QObject *object = mLoader->instance();
    if (object == nullptr) {
        qCWarning(spPlugin).noquote().nospace() << mModule << "| Native library cannot be instantiated";
        invalidate();
        return false;
    }
    mPluginInterface = qobject_cast<SpecchioPlugin*>(object);
    if (mPluginInterface == nullptr) {
        qCWarning(spPlugin).noquote().nospace() << mModule << "| Native library cannot be instantiated as a Specchio plugin";
        qCWarning(spPlugin) << "Instance type:" << object;
        invalidate();
        return false;
    }
    if(!mPluginInterface->init()) {
        qCWarning(spPlugin).noquote().nospace() << mModule << "| Plugin initialization failed: " << mPluginInterface->error();
        invalidate();
        return false;
    }

    if (mPluginInterface->mainObject().isEmpty() || mPluginInterface->version().isEmpty()) {
        qCWarning(spPlugin).noquote().nospace() << mModule << "| Invalid plugin metadata";
        invalidate();
        return false;
    }

    if (!mMainObject.isEmpty() && mPluginInterface->mainObject() != mMainObject) {
        qCWarning(spPlugin).noquote().nospace() << mModule << "| Difference in native library main object definition! "
            "In the library: " << mPluginInterface->mainObject() << " vs. in the metadata: " << mMainObject;
        invalidate();
        return false;
    }
    mMainObject = mPluginInterface->mainObject();
    if (mMainObject.isEmpty()) {
        qCWarning(spPlugin).noquote().nospace() << mModule << "| Invalid plugin object";
        invalidate();
        return false;
    }

    if (!mVersion.isEmpty() && mPluginInterface->version() != mVersion) {
        qCWarning(spPlugin).noquote().nospace() << mModule << "| Difference in native library version definition! "
            "In the library: " << mPluginInterface->version() << " vs. in the metadata: " << mVersion;
        invalidate();
        return false;
    }
    mVersion = mPluginInterface->version();

    mValid = true;
    mLoaded = true;
    return true;
}
