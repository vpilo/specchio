/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#include "types.h"


namespace {
    const QMap<Types::Region, QString> s_regionNames {
        { Types::Region::TopBar,       QStringLiteral("TopBar") },
        { Types::Region::TopLeft,      QStringLiteral("TopLeft") },
        { Types::Region::TopCenter,    QStringLiteral("TopCenter") },
        { Types::Region::TopRight,     QStringLiteral("TopRight") },
        { Types::Region::UpperThird,   QStringLiteral("UpperThird") },
        { Types::Region::MiddleCenter, QStringLiteral("MiddleCenter") },
        { Types::Region::LowerThird,   QStringLiteral("LowerThird") },
        { Types::Region::BottomLeft,   QStringLiteral("BottomLeft") },
        { Types::Region::BottomCenter, QStringLiteral("BottomCenter") },
        { Types::Region::BottomRight,  QStringLiteral("BottomRight") },
        { Types::Region::BottomBar,    QStringLiteral("BottomBar") },
    };
} // namespace

void Types::forEachRegion(const RegionOperation &op)
{
    auto it = static_cast<int>(RegionMeta::FirstRegion);
    for ( ; it <= static_cast<int>(RegionMeta::LastRegion) ; it++) {
        op(static_cast<Region>(it));
    }
}

const QString Types::regionName(const Region region)
{
    return s_regionNames[region];
}
