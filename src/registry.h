/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#pragma once

#include "plugin.h"
#include "settings.h"
#include "types.h"

#include <QtCore/QMap>
#include <QtCore/QSharedPointer>
#include <QtCore/QString>


class Region;
class Specchio;

class Registry : public QObject
{
    Q_OBJECT

public:
    using RegionMap = QMap<Types::Region, Region*>;

public:
    Registry();
    ~Registry();

public:
    Settings::Ptr settings() const { return mSettings; }
    Plugin::Ptr plugin(const QString &name) const { return mPlugins.value(name, nullptr); }
    Specchio *interface() const { return mInterface; }
    Region *region(Types::Region regionId) const { return mRegions.value(regionId, nullptr); }

    bool isValid() const;
    int pluginsCount() const { return mPlugins.size(); }
    int regionsCount() const { return mRegions.size(); }

    void addPlugin(const QString &name, const Plugin::Ptr& plugin);
    void setInterface(Specchio *instance);
    void setRegions(const RegionMap &map);

    void prepare();

public slots:
    void sendBroadcast(Types::Region caster, const QString &name, const QVariantMap &extras = QVariantMap());

private:
    Settings::Ptr mSettings;
    RegionMap mRegions;
    QMap<QString, Plugin::Ptr> mPlugins;

    Specchio *mInterface = nullptr;
};
