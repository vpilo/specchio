/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#include "paths.h"

#include <QtTest/QtTest>


class PathsTests : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void initialState();
    void reset();
    void modifications();
};

void PathsTests::initialState()
{
    QCOMPARE(Paths::getPluginsPaths().isEmpty(), false);
    QCOMPARE(Paths::getSettingsPath().isEmpty(), false);
    QCOMPARE(Paths::getInternalSettingsPath().isEmpty(), false);
}

void PathsTests::modifications()
{
    QStringList originalList;
    originalList = Paths::getPluginsPaths();
    Paths::setPluginsPaths({"t/es/t/"});
    QVERIFY(Paths::getPluginsPaths() != originalList);
    QCOMPARE(Paths::getPluginsPaths(), QStringList{"t/es/t/"});

    QString original;
    original = Paths::getSettingsPath();
    Paths::setSettingsPath("sett/ings/");
    QVERIFY(Paths::getSettingsPath() != original);
    QCOMPARE(Paths::getSettingsPath(), QString("sett/ings/"));

    original = Paths::getInternalSettingsPath();
    QVERIFY(original.startsWith(':'));
    Paths::setInternalSettingsPath(":/inter/nal/");
    QVERIFY(Paths::getInternalSettingsPath() != original);
    QCOMPARE(Paths::getInternalSettingsPath(), QString(":/inter/nal/"));
}

void PathsTests::reset()
{
    QStringList original_pluginPaths { Paths::getPluginsPaths() };
    QString original_settingsPath { Paths::getSettingsPath() };
    QString original_internalSettingsPath { Paths::getInternalSettingsPath() };

    Paths::setPluginsPaths({"/reset/plugins/path/"});
    Paths::setSettingsPath("/reset/settings/path/");
    Paths::setInternalSettingsPath(":/reset/internal/settings/path/");

    Paths::reset();

    QCOMPARE(original_pluginPaths, Paths::getPluginsPaths());
    QCOMPARE(original_settingsPath, Paths::getSettingsPath());
    QCOMPARE(original_internalSettingsPath, Paths::getInternalSettingsPath());
}

QTEST_MAIN(PathsTests);
#include "PathsTests.moc"
