/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

import QtQuick 2.8
import QtQuick.Window 2.2

import Specchio.Internal 1.0

// The actual QML scene contains the Wayland compositor
Item {
    id: fakeCompositor

    Window {
        TestRegion { regionId: RegionId.TopBar       }
        TestRegion { regionId: RegionId.TopLeft      }
        TestRegion { regionId: RegionId.TopCenter    }
        TestRegion { regionId: RegionId.TopRight     }
        TestRegion { regionId: RegionId.UpperThird   }
        TestRegion { regionId: RegionId.MiddleCenter }
        TestRegion { regionId: RegionId.LowerThird   }
        TestRegion { regionId: RegionId.BottomLeft   }
        TestRegion { regionId: RegionId.BottomCenter }
        TestRegion { regionId: RegionId.BottomRight  }
        TestRegion { regionId: RegionId.BottomBar    }
    }
}
