/*
 * Copyright 2020 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

import QtQuick 2.8
import QtQuick.Window 2.2

import Specchio.Internal 1.0

Item {
    id: root
    property alias regionId: impl.regionId
    LoggingCategory { name: 'aName' ; objectName: 'tag' }
    RegionImpl {
        id: impl
        regionQmlItem: root
        pluginObject: Item {
            // TODO Add broadcast testing
        }
    }
}
