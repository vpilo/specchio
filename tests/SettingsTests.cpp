/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#include "settings.h"

#include <QtTest/QtTest>
#include <QtCore/QTemporaryFile>

class SettingsTests : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void invalid_resource();
    void invalid_file();
    void invalid_syntax();
    void defaults_map();
    void custom();
    void types();
    void user_copy_defaults();
    void user_cannot_copy_defaults();
};


void SettingsTests::invalid_resource()
{
    Settings settings { ":/settings/invalid_resource.toml", QString() };
    QCOMPARE(settings.valid(), false);
    QCOMPARE(settings.empty(), true);
}

void SettingsTests::invalid_file()
{
    Settings settings { "/tmp/invalid_file.toml", QString() };
    QCOMPARE(settings.valid(), false);
    QCOMPARE(settings.empty(), true);
}

void SettingsTests::invalid_syntax()
{
    Settings settings { ":/settings/invalid_syntax.toml", QString() };
    QCOMPARE(settings.valid(), false);
    QCOMPARE(settings.empty(), true);
}

void SettingsTests::defaults_map()
{
    Settings settings { ":/settings/default.toml", QString() };
    QCOMPARE(settings.valid(), true);
    QCOMPARE(settings.empty(), false);

    QCOMPARE(settings["generalSetting"].toString(), QString("A"));

    // Test normal grouped settings
    QVERIFY(settings.contains("regions"));
    QCOMPARE((QMetaType::Type)settings["regions"].type(), QMetaType::QVariantMap);
    QVariantMap regions { settings["regions"].toMap() };
    QCOMPARE(regions.count(), 11);
    QVERIFY(regions.contains("BottomBar"));
    QCOMPARE(regions["BottomBar"].toString(), QString("WrongPlugin"));
    QVERIFY(regions.contains("MiddleCenter"));
    QCOMPARE(regions["MiddleCenter"].toString(), QString("MiddleCenterPlugin"));
    QVERIFY(regions.contains("TopRight"));
    QCOMPARE(regions["TopRight"].toString(), QString(""));

    // Test nested subgroups
    QCOMPARE(settings.contains("empty"), true);
    QCOMPARE(settings["empty"].toMap()["nested"].toMap().contains("group"), true);
    QCOMPARE(settings["empty"].toMap()["nested"].toMap()["group"].toMap().isEmpty(), true);
    QCOMPARE(settings["normal"].toMap()["nested"].toMap()["group"].toMap()["hasContents"].toString(), QString("yes"));

    // Test same-named settings in different groups
    QCOMPARE(settings["sameNameGroup1"].toMap()["same_name_setting"].toString(), QString("value1"));
    QCOMPARE(settings["sameNameGroup2"].toMap()["same_name_setting"].toString(), QString("value2"));
}

void SettingsTests::custom()
{
    Settings settings { ":/settings/default.toml", ":/settings/user.toml" };
    QCOMPARE(settings.valid(), true);
    QCOMPARE(settings.empty(), false);

    // Settings can be changed in type
    QCOMPARE(settings["generalSetting"].toInt(), 192);

    QVERIFY(settings.contains("regions"));
    QCOMPARE((QMetaType::Type)settings["regions"].type(), QMetaType::QVariantMap);
    QVariantMap regions { settings["regions"].toMap() };
    QCOMPARE(regions.count(), 11);
    // Empty values changed to empty stay empty
    QCOMPARE(regions["TopBar"].toString(), QString(""));
    // Empty values changed to non empty
    QCOMPARE(regions["TopRight"].toString(), QString("WrongPlugin"));
    // Unchanged values stay as they were
    QCOMPARE(regions["TopLeft"].toString(), QString("TopLeftPlugin"));
    QCOMPARE(regions["BottomBar"].toString(), QString("WrongPlugin"));
    // A value can be overridden to an empty string
    QVERIFY(regions.contains("BottomLeft"));
    QCOMPARE(regions["BottomLeft"].toString(), QString(""));

    // User values can be introduced, but nothing will care about them
    QCOMPARE(settings["userIntroducedSetting"].toString(), QString("nobody will care"));

    QCOMPARE(settings["normal"].toMap()["nested"].toMap()["group"].toMap()["hasContents"].toString(), QString("yes, but different"));
}

void SettingsTests::types()
{
    Settings settings { ":/settings/types/default.toml", QString() };
    QCOMPARE(settings.valid(), true);
    QCOMPARE(settings.empty(), false);

    QCOMPARE(settings["string"], QVariant("this should be a string"));
    QCOMPARE(settings["integer"], QVariant(1234567));
    QCOMPARE(settings["double"], QVariant(1122334455.6677));
    QCOMPARE(settings["yes"], QVariant(true));
    QCOMPARE(settings["no"], QVariant(false));
    QCOMPARE(settings["empty"], QVariant(""));
    QCOMPARE(settings["alsoEmpty"], QVariant(""));
    QCOMPARE(settings["stringlist"], QVariant(QStringList{"this","should","be","a","string","list"}));
    QCOMPARE(settings["not_a_stringlist"], QVariant("this,should,not,be,a,string,list"));

    QCOMPARE(settings["string"].toString(), QString("this should be a string"));
    QCOMPARE(settings["integer"].toInt(), 1234567);
    QCOMPARE(settings["integer"].toString(), QString("1234567"));
    QCOMPARE(settings["double"].toDouble(), 1122334455.6677);
    QCOMPARE(settings["double"].toString(), QString("1122334455.6677"));
    QCOMPARE(settings["yes"].toBool(), true);
    QCOMPARE(settings["yes"].toString(), QString("true"));
    QCOMPARE(settings["no"].toBool(), false);
    QCOMPARE(settings["no"].toString(), QString("false"));
    QCOMPARE(settings["empty"].toString(), QString(""));
    QCOMPARE(settings["stringlist"].toStringList(), (QStringList{"this","should","be","a","string","list"}));
    QCOMPARE(settings["not_a_stringlist"].toString(), QString("this,should,not,be,a,string,list"));
}

void SettingsTests::user_copy_defaults()
{
    QString tempSettingsPath { QDir::tempPath() + "/specchio-test/specchio.test.toml" };
    QFile tempSettingsFile { tempSettingsPath };

    {
        if (tempSettingsFile.remove()) {
            qDebug() << "Temp file was already present";
        }
        Settings settings { ":/settings/default.toml", tempSettingsPath };
        QCOMPARE(settings.valid(), true);
        QVERIFY(tempSettingsFile.exists());
        QCOMPARE(tempSettingsFile.size(), QFileInfo(":/settings/default.toml").size());
    }

    // Try to load it back
    {
        Settings settings_reload { ":/settings/default.toml", tempSettingsPath };
        QCOMPARE(settings_reload.valid(), true);
        QVERIFY(tempSettingsFile.exists());
    }

    // Try to load the user settings file alone
    {
        QVERIFY(tempSettingsFile.exists());
        Settings settings_reload_again { tempSettingsPath, QString() };
        QCOMPARE(settings_reload_again.valid(), true);
    }

    // Cleanup
    (void) tempSettingsFile.remove();
}

void SettingsTests::user_cannot_copy_defaults()
{
    // Place the file in a directory that should not be creatable or writable
    QString tempSettingsPath { "/dev/null/specchio-test/specchio.test.toml" };

    QFile tempSettingsFile { tempSettingsPath };
    QVERIFY(!tempSettingsFile.exists());
    Settings settings { ":/settings/default.toml", tempSettingsPath };
    QCOMPARE(settings.valid(), true);
    QVERIFY(!tempSettingsFile.exists());
}

QTEST_MAIN(SettingsTests);
#include "SettingsTests.moc"
