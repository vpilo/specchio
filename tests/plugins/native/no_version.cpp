/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#include "plugin_interface.hpp"

#include <QtQml/QQmlExtensionPlugin>


#define PLUGIN_MAIN_OBJECT NativeTestMainObject

#define _TO_STRING_DONTUSE_(s) #s
#define TO_STRING(s) _TO_STRING_DONTUSE_(s)

namespace {
    const QString s_mainObject { QStringLiteral(TO_STRING(PLUGIN_MAIN_OBJECT)) };
    const QString s_version;
} // namespace


class NoVersion : public QQmlExtensionPlugin, public SpecchioPlugin
{
    Q_OBJECT
    Q_INTERFACES(SpecchioPlugin)
    Q_PLUGIN_METADATA(IID SPECCHIO_PLUGIN_INTERFACE_NAME)

public:
    void registerTypes(const char *uri) override {
        Q_UNUSED(uri)
    }

    const QString &mainObject() override {
        return s_mainObject;
    }

    const QString &version() override {
        return s_version;
    }
};

#include "no_version.moc"
