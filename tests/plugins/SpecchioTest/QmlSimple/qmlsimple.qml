/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

import QtQuick 2.9

Text {
    text: "test"
    color: "white"
    style: Text.Raised
    font.pointSize: 42
}
