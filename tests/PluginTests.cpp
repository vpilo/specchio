/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#include "plugin.h"

#include <QtCore/QPluginLoader>
#include <QtCore/QTemporaryDir>
#include <QtTest/QtTest>

namespace {
    const QString s_pluginPathNativeGood { QStringLiteral(SPECCHIO_BUILD_DIR "/tests/plugins/native/libnative_good.so") };
    const QString s_pluginPathNativeWrongInterface { QStringLiteral(SPECCHIO_BUILD_DIR "/tests/plugins/native/libnative_wrong_interface.so") };
    const QString s_pluginPathNativeNotAPlugin { QStringLiteral(SPECCHIO_BUILD_DIR "/tests/plugins/native/libnative_not_a_plugin.so") };
    const QString s_pluginPathNativeInitError { QStringLiteral(SPECCHIO_BUILD_DIR "/tests/plugins/native/libnative_init_error.so") };
    const QString s_pluginPathNativeNoMainOnbject { QStringLiteral(SPECCHIO_BUILD_DIR "/tests/plugins/native/libnative_no_main_object.so") };
    const QString s_pluginPathNativeNoVersion { QStringLiteral(SPECCHIO_BUILD_DIR "/tests/plugins/native/libnative_no_version.so") };
} // namespace

class PluginTests : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void names_data();
    void names();

    void nativeLibs_data();
    void nativeLibs();
};

void PluginTests::names_data()
{
    QTest::addColumn<QString>("name");
    QTest::addColumn<bool>("result");

    QTest::newRow("empty") << "" << false;
    QTest::newRow("lowercase_only") << "testing" << true;
    QTest::newRow("uppercase_only") << "PLUGINNAME" << true;
    QTest::newRow("upper_lower_case_mix") << "PlUgInNaMe" << true;
    QTest::newRow("with_dots") << "A.Plugin.Name" << true;
    QTest::newRow("with_multiple_dots") << "A.Plugin..Name" << false;
    QTest::newRow("with_numbers") << "133t5p34kP1ug1N" << true;
    QTest::newRow("with_underscores") << "Plugin_Name" << true;
    QTest::newRow("with_multiple_underscores") << "Plugin__Name" << false;
    QTest::newRow("with_dashes") << "oh-my-dog" << false;
    QTest::newRow("with_other_chars") << "Clearly, not a good name! ^_^" << false;
}

void PluginTests::names()
{
    QFETCH(QString, name);
    QFETCH(bool, result);

    Plugin plugin(name, "");
    QCOMPARE(plugin.load(), result);
}

void PluginTests::nativeLibs_data()
{
    QTest::addColumn<QString>("module");
    QTest::addColumn<QString>("mainObject");
    QTest::addColumn<QString>("version");
    QTest::addColumn<QString>("path");
    QTest::addColumn<bool>("result");

    QTest::newRow("file_not_found")
    << "file_not_found"
    << "NativeTestMainObject"
    << "0.1"
    << "/wrong"
    << false;

    QTest::newRow("wrong_interface")
    << "wrong_interface"
    << "NativeTestMainObject"
    << "0.1"
    << s_pluginPathNativeWrongInterface
    << false;

    QTest::newRow("not_a_plugin")
    << "not_a_plugin"
    << "NativeTestMainObject"
    << "0.1"
    << s_pluginPathNativeNotAPlugin
    << false;

    QTest::newRow("init_error")
    << "init_error"
    << "NativeTestMainObject"
    << "0.1"
    << s_pluginPathNativeInitError
    << false;

    QTest::newRow("no_main_object")
    << "no_main_object"
    << "NativeTestMainObject"
    << "0.1"
    << s_pluginPathNativeNoMainOnbject
    << false;

    QTest::newRow("no_version")
    << "no_version"
    << "NativeTestMainObject"
    << "0.1"
    << s_pluginPathNativeNoVersion
    << false;

    QTest::newRow("wrong_main_object")
    << "wrong_main_object"
    << "WrongMainObject"
    << "0.1"
    << s_pluginPathNativeGood
    << false;

    QTest::newRow("wrong_version")
    << "wrong_version"
    << "NativeTestMainObject"
    << "99999"
    << s_pluginPathNativeGood
    << false;

    QTest::newRow("okay")
    << "okay"
    << "NativeTestMainObject"
    << "0.1"
    << s_pluginPathNativeGood
    << true;
}

void PluginTests::nativeLibs()
{
    QFETCH(QString, module);
    QFETCH(QString, mainObject);
    QFETCH(QString, version);
    QFETCH(QString, path);
    QFETCH(bool, result);

    QTemporaryDir tempDir;
    QVERIFY(tempDir.isValid());
    Plugin plugin(module, tempDir.path(), mainObject, version, path);
    QCOMPARE(plugin.load(), result);
}

QTEST_MAIN(PluginTests);
#include "PluginTests.moc"
