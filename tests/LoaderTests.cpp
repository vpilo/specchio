/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#include "loader.h"
#include "paths.h"
#include "registry.h"
#include "specchio.h"
#include "types.h"

#include <QtCore/QTemporaryDir>
#include <QtCore/QTemporaryFile>
#include <QtQml/QQmlApplicationEngine>
#include <QtTest/QtTest>

namespace {
    const QString s_mainQmlFile = QStringLiteral("qrc:///loader/qml/main.qml");
} // namespace

class LoaderTests : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void init();
    void cleanup();

    void settings_invalid_globals_resource();

    void initialization_data();
    void initialization();
    void initialization_multiple_roots();

    void plugins_load();
    void plugins_instance();

    void fonts_default_broken();
    void fonts_default_good();
    void fonts_user_broken();
    void fonts_user_good();

private:
    // Changing anything in Paths makes the settings in mRegistry outdated.
    // In every test, call only after changing the paths.
    void prepare_instances();

    void copy(QTemporaryDir &tempDir, const QString& sourceFileName);
private:
    Registry *mRegistry = nullptr;
    Specchio *mInterface = nullptr;
};

void LoaderTests::init()
{
    Paths::setInternalSettingsPath(":/loader/");
    Paths::setSettingsPath("/tmp/settings/");
    Paths::setPluginsPaths({"/tmp/plugins/"});
    prepare_instances();
}

void LoaderTests::cleanup()
{
    delete mRegistry;
    mRegistry = nullptr;
    delete mInterface;
    mInterface = nullptr;
}

void LoaderTests::prepare_instances()
{
    cleanup();
    mRegistry = new Registry;
    mInterface = new Specchio(nullptr, mRegistry->settings());
    mRegistry->setInterface(mInterface);
}


void LoaderTests::settings_invalid_globals_resource()
{
    Paths::setInternalSettingsPath(":/invalid/");
    prepare_instances();

    Loader loader { this, mRegistry, mInterface };
    QCOMPARE(loader.initialize(s_mainQmlFile), false);
}

void LoaderTests::initialization_data()
{
    QTest::addColumn<QString>("qmlFile");
    QTest::addColumn<bool>("result");

    QTest::newRow("correct")
        << s_mainQmlFile
        << true;
    QTest::newRow("empty_string")
        << ""
        << false;
    QTest::newRow("empty_url")
        << "qrc://"
        << false;
    QTest::newRow("root_url")
        << "qrc:///"
        << false;
    QTest::newRow("empty_qml")
        << "qrc:///loader/qml/main_empty.qml"
        << false;
    QTest::newRow("broken_qml")
        << "qrc:///loader/qml/main_broken.qml"
        << false;
    QTest::newRow("qml_no_window")
        << "qrc:///loader/qml/main_no_window.qml"
        << false;
}

void LoaderTests::initialization()
{
    QFETCH(QString, qmlFile);
    QFETCH(bool, result);

    QTemporaryDir tempDir;
    QVERIFY(tempDir.isValid());
    Paths::setSettingsPath(tempDir.path() + '/');
    Paths::setPluginsPaths({});
    prepare_instances();

    Loader loader { this, mRegistry, mInterface };
    QCOMPARE(loader.initialize(qmlFile), result);
}

void LoaderTests::initialization_multiple_roots()
{
    QTemporaryDir tempDir;
    QVERIFY(tempDir.isValid());
    Paths::setSettingsPath(tempDir.path() + '/');
    Paths::setPluginsPaths({});
    prepare_instances();

    Loader loader { this, mRegistry, mInterface };
    QCOMPARE(loader.initialize("qrc:///loader/qml/main_multiple_roots.qml"), false);
    QCOMPARE(loader.initialize("qrc:///loader/qml/main_multiple_roots.qml"), false);
}

void LoaderTests::plugins_load()
{
    QTemporaryDir tempDir;
    QVERIFY(tempDir.isValid());
    Paths::setSettingsPath(tempDir.path() + '/');
    Paths::setPluginsPaths({ QStringLiteral(SPECCHIO_SOURCE_DIR) + "/tests/plugins/" /* Must end with slash */ });
    prepare_instances();

    Loader loader { this, mRegistry, mInterface };
    QVERIFY(loader.initialize(s_mainQmlFile));

    loader.scanPlugins();

    QCOMPARE(mRegistry->pluginsCount(), 1);
    QVERIFY(mRegistry->plugin("Invalid").isNull());
    QVERIFY(mRegistry->plugin("WrongPlugin").isNull());
    QVERIFY(!mRegistry->plugin("SpecchioTest.QmlSimple").isNull());

    QVariantMap settings = mRegistry->interface()->settings();
    QVariantMap regions { settings["regions"].toMap() };
    QCOMPARE(regions["TopLeft"].toString(), QString("SpecchioTest.QmlSimple"));
    QCOMPARE(regions["MiddleCenter"].toString(), QString("Compliments"));
    QCOMPARE(regions["BottomBar"].toString(), QString("WrongPlugin"));
}

void LoaderTests::plugins_instance()
{
    QTemporaryDir tempDir;
    QVERIFY(tempDir.isValid());
    Paths::setSettingsPath(tempDir.path() + '/');
    Paths::setPluginsPaths({ QStringLiteral(SPECCHIO_SOURCE_DIR) + "/tests/plugins/" /* Must end with slash */ });
    prepare_instances();

    Loader loader { this, mRegistry, mInterface };

    loader.scanPlugins();
    QVERIFY(loader.initialize(s_mainQmlFile));
    QCOMPARE(mRegistry->pluginsCount(), 1);
    QVERIFY(!mRegistry->plugin("SpecchioTest.QmlSimple").isNull());

    QCOMPARE(loader.loadPlugin(Types::Region::BottomBar, QStringLiteral("SpecchioTest.QmlSimple")), true);
    QCOMPARE(loader.loadPlugin(Types::Region::BottomBar, "WrongPlugin"), false);
    QCOMPARE(loader.loadPlugin(Types::Region::MiddleCenter, "SpecchioTest.QmlSimple"), true);
    QCOMPARE(loader.loadPlugin(Types::Region::MiddleCenter, "WrongPlugin"), false);
}

void LoaderTests::fonts_default_broken()
{
    QTemporaryDir tempDir;
    QVERIFY(tempDir.isValid());
    Paths::setInternalSettingsPath(":/loader/fonts/defaultBroken/");
    Paths::setSettingsPath(tempDir.path() + '/');
    Paths::setPluginsPaths({});
    prepare_instances();

    Loader loader { this, mRegistry, mInterface };
    QCOMPARE(loader.initialize(s_mainQmlFile), false);
    QCOMPARE(mRegistry->interface()->font(), QFont());
}

void LoaderTests::fonts_default_good()
{
    QTemporaryDir tempDir;
    QVERIFY(tempDir.isValid());
    Paths::setSettingsPath(tempDir.path() + '/');
    Paths::setPluginsPaths({});
    prepare_instances();

    Loader loader { this, mRegistry, mInterface };
    QCOMPARE(loader.initialize(s_mainQmlFile), true);
    QCOMPARE(mRegistry->interface()->font().family(), QString("Noto Sans"));
}

void LoaderTests::fonts_user_broken()
{
    QTemporaryDir tempDir;
    copy(tempDir, ":/loader/fonts/userBroken/specchio.toml");
    Paths::setSettingsPath(tempDir.path() + '/');
    Paths::setPluginsPaths({});
    prepare_instances();

    Loader loader { this, mRegistry, mInterface };
    QCOMPARE(loader.initialize(s_mainQmlFile), true);
    QCOMPARE(mRegistry->interface()->font().family(), QString("Noto Sans"));
}

void LoaderTests::fonts_user_good()
{
    QTemporaryDir tempDir;
    copy(tempDir, ":/loader/fonts/user/specchio.toml");
    Paths::setSettingsPath(tempDir.path() + '/');
    Paths::setPluginsPaths({});
    prepare_instances();

    Loader loader { this, mRegistry, mInterface };
    QCOMPARE(loader.initialize(s_mainQmlFile), true);
    QCOMPARE(mRegistry->interface()->font().family(), QString("Tiny"));
}

void LoaderTests::copy(QTemporaryDir &tempDir, const QString& sourceFileName)
{
    QVERIFY(tempDir.isValid());

    QFile source(sourceFileName);
    QFile dest(tempDir.path() + "/specchio.toml");
    QCOMPARE(dest.fileName().isEmpty(), false);

    QVERIFY(source.open(QIODevice::ReadOnly));
    QVERIFY(dest.open(QIODevice::WriteOnly));

    dest.write(source.readAll());
    source.close();
    dest.close();

    QFileInfo destInfo(dest.fileName());
    QCOMPARE(destInfo.size(), QFileInfo(sourceFileName).size());
}

QTEST_MAIN(LoaderTests);
#include "LoaderTests.moc"
