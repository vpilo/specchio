/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#include "specchio.h"
#include "settings.h"
#include "types.h"

#include <QtTest/QtTest>

class InterfaceTests : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void settings_rotation_none();
    void settings_rotation_data();
    void settings_rotation();

    void lifecycle();

    void time_distance_data();
    void time_distance();

private:
    Settings::Ptr emptySettings();
};

void InterfaceTests::settings_rotation_none()
{
    Settings::Ptr settings = emptySettings();
    Specchio specchio(this, settings);
    QCOMPARE(settings->contains("rotation"), false);
}

void InterfaceTests::settings_rotation_data()
{
    QTest::addColumn<int>("given");
    QTest::addColumn<int>("expected");

    QTest::newRow("0")
    << 0
    << 0;
    QTest::newRow("90")
    << 90
    << 90;
    QTest::newRow("180")
    << 180
    << 180;
    QTest::newRow("270")
    << 270
    << 270;
    QTest::newRow("360")
    << 360
    << 0;
    QTest::newRow("-90")
    << -90
    << 270;
    QTest::newRow("-180")
    << -180
    << 180;
    QTest::newRow("-270")
    << -270
    << 90;
    QTest::newRow("-360")
    << 360
    << 0;
    QTest::newRow("-360")
    << -360
    << 0;
    QTest::newRow("-45")
    << -45
    << 0;
    QTest::newRow("-32")
    << -32
    << 0;
    QTest::newRow("6")
    << 6
    << 0;
    QTest::newRow("9999")
    << 9999
    << 0;
}

void InterfaceTests::settings_rotation()
{
    QFETCH(int, given);
    QFETCH(int, expected);

    Settings::Ptr settings = emptySettings();
    settings->insert("rotation", given);

    Specchio specchio(this, settings);

    bool ok;
    int actual = settings->value("rotation", -INT_MAX).toInt(&ok);
    QCOMPARE(ok, true);
    QCOMPARE(actual, expected);
}

void InterfaceTests::lifecycle()
{
    Specchio specchio(this, emptySettings());

    QSignalSpy spyStart(&specchio, SIGNAL(starting()));
    QSignalSpy spyStop(&specchio, SIGNAL(stopping()));

    QList<QVariant> arguments;

    specchio.stop();

    QVERIFY(spyStart.count() == 0);
    QVERIFY(spyStop.count() == 0);

    specchio.start();

    QVERIFY(spyStart.count() == 1);
    QVERIFY(spyStop.count() == 0);;

    specchio.start();

    QVERIFY(spyStart.count() == 1);
    QVERIFY(spyStop.count() == 0);

    specchio.stop();

    QVERIFY(spyStart.count() == 1);
    QVERIFY(spyStop.count() == 1);

    specchio.stop();

    QVERIFY(spyStart.count() == 1);
    QVERIFY(spyStop.count() == 1);
}

void InterfaceTests::time_distance_data()
{
    QTest::addColumn<qint64>("timestamp");
    QTest::addColumn<QString>("expected");

    const qint64 now { QDateTime::currentDateTime().toMSecsSinceEpoch() };

    const auto toMillisPast = [](qint64 minutes) -> qint64 { return minutes * 60 * 1000; };
    qint64 hour = toMillisPast(60);
    qint64 day = toMillisPast(1440);
    qint64 month = toMillisPast(1440*31);

    // Past events
    QTest::newRow("-now") << now - toMillisPast(0) << "now";
    QTest::newRow("-1min") << now - toMillisPast(1) << "1 minute(s) ago";
    QTest::newRow("-1min") << now - toMillisPast(2) << "2 minute(s) ago";
    QTest::newRow("-10min") << now - toMillisPast(10) << "10 minute(s) ago";
    QTest::newRow("-58min") << now - toMillisPast(58) << "58 minute(s) ago";
    QTest::newRow("-59min") << now - toMillisPast(59) << "59 minute(s) ago";
    QTest::newRow("-1hr") << now - hour << "1 hour(s) ago";
    QTest::newRow("-2hr") << now - hour*2 << "2 hour(s) ago";
    QTest::newRow("-18hr") << now - hour*18 << "18 hour(s) ago";
    QTest::newRow("-23hr") << now -  hour*23 << "23 hour(s) ago";
    QTest::newRow("-1d") << now - day << "yesterday";
    QTest::newRow("-2d") << now - day*2 << "2 day(s) ago";
    QTest::newRow("-6d") << now - day*6 << "6 day(s) ago";
    QTest::newRow("-1w") << now - day*7 << "1 week(s) ago";
    QTest::newRow("-1w-") << now - day*7-1 << "1 week(s) ago";
    QTest::newRow("-2w") << now - day*14 << "2 week(s) ago";
    QTest::newRow("-4w") << now - day*28 << "4 week(s) ago";
    QTest::newRow("-1m") << now - month << "1 month(s) ago";
    QTest::newRow("-1m+") << now - (month+(day*3)) << "1 month(s) ago"; // just a bit more than a month
    QTest::newRow("-1m++") << now - (month+(day*7)) << "1 month(s) ago"; // still a bit more than a month
    QTest::newRow("-2m") << now - (month*2) << "2 month(s) ago";
    QTest::newRow("-4m") << now - (month*4) << "4 month(s) ago";
    QTest::newRow("-11m") << now - (month*11) << "11 month(s) ago";
    QTest::newRow("-11m+") << now - (month*11+day*15) << "11 month(s) ago";
    QTest::newRow("-1y") << now - (month*12) << "1 year(s) ago";
    QTest::newRow("-1.9y") << now - (month*23) << "1 year(s) ago";
    QTest::newRow("-2y") << now - (month*24) << "2 year(s) ago";

    // Future events

    // Add an extra second to the input to match time distances in the future (e.g. second 0)
    const auto toMillisFuture = [](qint64 minutes) -> qint64 { return (minutes * 60 + 1) * 1000 ; };
    hour = toMillisFuture(60);
    day = toMillisFuture(1440);
    month = toMillisFuture(1440*31);

    QTest::newRow("+now") << now + toMillisFuture(0) << "now";
    QTest::newRow("+1min") << now + toMillisFuture(1) << "in 1 minute(s)";
    QTest::newRow("+1.999min") << now + toMillisFuture(2) - 1000 << "in 1 minute(s)";
    QTest::newRow("+2min") << now + toMillisFuture(2) << "in 2 minute(s)";
    QTest::newRow("+10min") << now + toMillisFuture(10) << "in 10 minute(s)";
    QTest::newRow("+58min") << now + toMillisFuture(58) << "in 58 minute(s)";
    QTest::newRow("+59min") << now + toMillisFuture(59) << "in 59 minute(s)";
    QTest::newRow("+1hr") << now + hour << "in 1 hour(s)";
    QTest::newRow("+2hr") << now + hour*2 << "in 2 hour(s)";
    QTest::newRow("+18hr") << now + hour*18 << "in 18 hour(s)";
    QTest::newRow("+23hr") << now +  hour*23 << "in 23 hour(s)";
    QTest::newRow("+1d") << now + day << "tomorrow";
    QTest::newRow("+2d") << now + day*2 << "in 2 day(s)";
    QTest::newRow("+5d") << now + day*5 << "in 5 day(s)";
    QTest::newRow("+1w") << now + day*7 << "in 1 week(s)";
    QTest::newRow("+1w+") << now + day*13 << "in 1 week(s)";
    QTest::newRow("+3w") << now + day*21 << "in 3 week(s)";
    QTest::newRow("+4w") << now + day*28 << "in 4 week(s)";
    QTest::newRow("+1m") << now + month << "in 1 month(s)";
    QTest::newRow("+1m+") << now + (month+(day*3)) << "in 1 month(s)"; // just a bit more than a month
    QTest::newRow("+1m++") << now + (month+(day*7)) << "in 1 month(s)"; // still a bit more than a month
    QTest::newRow("+2m") << now + (month*2) << "in 2 month(s)";
    QTest::newRow("+4m") << now + (month*4) << "in 4 month(s)";
    QTest::newRow("+11m") << now + (month*11) << "in 11 month(s)";
    QTest::newRow("+11m+") << now + (month*11+day*15) << "in 11 month(s)";
    QTest::newRow("+1y") << now + (month*12) << "in 1 year(s)";
    QTest::newRow("+1.9y") << now + (month*23) << "in 1 year(s)";
    QTest::newRow("+2y") << now + (month*24) << "in 2 year(s)";
}

void InterfaceTests::time_distance()
{
    QFETCH(qint64, timestamp);
    QFETCH(QString, expected);

    Specchio specchio(this, emptySettings());
    QCOMPARE(specchio.timeDistance(timestamp).toLower(), expected.toLower());
}

Settings::Ptr InterfaceTests::emptySettings()
{
    return Settings::Ptr::create(*new Settings(":/settings/empty.toml"));
}

QTEST_MAIN(InterfaceTests);
#include "InterfaceTests.moc"
