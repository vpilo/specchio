/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#include "registry.h"

#include <QtTest/QtTest>
#include <QtQuick/QQuickItem>

#include "paths.h"
#include "region.h"

namespace {
    const QString s_pluginPathQmlSimple { QStringLiteral(SPECCHIO_BUILD_DIR "/tests/plugins/qmlsimple") };
} // namespace

Q_DECLARE_METATYPE(Plugin::Ptr)
Q_DECLARE_METATYPE(Settings::Ptr)

class Specchio
{
    // Stub
};

class RegistryTests : public QQuickItem
{
    Q_OBJECT

private Q_SLOTS:
    void initTestCase();
    void init();
    void cleanup();

    void no_settings();
    void empty();

    void interface();
    void plugins();
    void regions();


    void tooEarlyBroadcast();
    void broadcastDelivery();
    void broadcastReception();
    void broadcastLoopDetection();

private:
    void setupData();

private:
    QTemporaryDir tempDir;
    QMap<Types::Region,QSignalSpy*> spies;
    Registry::RegionMap testRegions;
};

void RegistryTests::initTestCase()
{
    Q_ASSERT(tempDir.isValid());
}

void RegistryTests::init()
{
    Paths::setInternalSettingsPath(":/loader/");
    Paths::setSettingsPath(tempDir.path() + '/');
}

void RegistryTests::cleanup()
{
    qDeleteAll(spies);
    spies.clear();
    qDeleteAll(testRegions);
    testRegions.clear();
}

void RegistryTests::setupData()
{
    Types::forEachRegion([this](Types::Region region) {
        testRegions[region] = new Region;
        testRegions[region]->setRegionId(region);
        spies.insert(region, new QSignalSpy { testRegions[region], &Region::broadcast });
    });
}

void RegistryTests::no_settings()
{
    Paths::setInternalSettingsPath(":/invalid/");

    Registry registry;
    QCOMPARE(registry.isValid(), false);
    QCOMPARE(registry.settings().isNull(), true);
    QCOMPARE(registry.interface(), nullptr);
    QCOMPARE(registry.pluginsCount(), 0);
    QCOMPARE(registry.regionsCount(), 0);
}

void RegistryTests::empty()
{
    Registry registry;
    QCOMPARE(registry.isValid(), false);
    QCOMPARE(registry.settings().isNull(), false);
    QCOMPARE(registry.settings()->valid(), true);
    QCOMPARE(registry.interface(), nullptr);
    QCOMPARE(registry.pluginsCount(), 0);
    QCOMPARE(registry.regionsCount(), 0);
}

void RegistryTests::interface()
{
    Registry registry;
    Specchio testIface;
    registry.setInterface(&testIface);
    QCOMPARE(registry.isValid(), true);
    QCOMPARE(registry.interface(), &testIface);
}

void RegistryTests::plugins()
{
    Registry registry;
    registry.addPlugin("test1", Plugin::Ptr::create("module1", "path1"));
    QCOMPARE(registry.pluginsCount(), 1);
    registry.addPlugin("test2", Plugin::Ptr::create("module2", "path2"));
    QCOMPARE(registry.pluginsCount(), 2);
}

void RegistryTests::regions()
{
    Region region;
    Registry::RegionMap testRegions {
        {Types::Region::LowerThird, &region },
        {Types::Region::TopRight, &region },
    };

    Registry registry;
    registry.setRegions(testRegions);
    QCOMPARE(registry.regionsCount(), 2);
}

void RegistryTests::tooEarlyBroadcast()
{
    setupData();

    Registry registry;
    registry.setRegions(testRegions);
    QCOMPARE(registry.regionsCount(), Types::RegionMeta::NumRegions);

    Types::Region sender = Types::Region::BottomCenter;
    emit testRegions[sender]->broadcastRelay(sender, "test", {});
    QCOMPARE(spies[Types::Region::UpperThird]->count(), 0);
}

void RegistryTests::broadcastDelivery()
{
    setupData();

    Registry registry;
    Specchio testIface;
    registry.setInterface(&testIface);
    registry.setRegions(testRegions);
    QCOMPARE(registry.regionsCount(), Types::RegionMeta::NumRegions);

    QString broadcast = "A_BROADCAST";
    QVariantMap broadcastExtras = { { "name", "value" } };

    Types::Region sender = Types::Region::LowerThird;
    registry.sendBroadcast(sender, broadcast, broadcastExtras);

    spies[sender]->wait(50);
    Types::forEachRegion([this, sender](Types::Region region) {
        if (sender == region) {
            QCOMPARE(spies[region]->count(), 0);
        } else {
            QCOMPARE(spies[region]->count(), 1);
        }
    });
}

void RegistryTests::broadcastReception()
{
    setupData();

    Types::Region sender = Types::Region::BottomCenter;
    testRegions[sender]->receiveBroadcast("test", {});
    QCOMPARE(spies[sender]->count(), 1);
}

void RegistryTests::broadcastLoopDetection()
{
    setupData();

    Types::Region sender = Types::Region::BottomCenter;
    QSignalBlocker blocker{ testRegions[sender] };
    testRegions[sender]->receiveBroadcast("test", {});
    QCOMPARE(spies[sender]->count(), 0);
}

QTEST_MAIN(RegistryTests);
#include "RegistryTests.moc"
