/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#include "region.h"
#include "paths.h"
#include "smarttimer.h"

#include <QtTest/QtTest>
#include <QtQml/QQmlEngine>
#include <QtQuick/QQuickItem>

// TODO test the presence of QML aliases to the C++ class
// TODO test the broadcasts between C++ and QML

namespace {
    const QString s_pluginPathQmlSimple { QStringLiteral(SPECCHIO_SOURCE_DIR "/tests/plugins/SpecchioTest/QmlSimple") };
} // namespace

Q_DECLARE_METATYPE(Plugin::Ptr)
Q_DECLARE_METATYPE(Settings::Ptr)

class RegionTests : public QQuickItem
{
    Q_OBJECT

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();

    void no_name_set();

    void population_data();
    void population();

    void activation_data();
    void activation();

    void timerCreate();

private:
    Plugin::Ptr makeInvalidPlugin() { return Plugin::Ptr{}; }
    Settings::Ptr makeInvalidSettings() { return Settings::Ptr{}; }
    Plugin::Ptr makeValidPlugin();
    Settings::Ptr makeValidSettings();

    QQuickItem *makeValidPluginObject();
    QQuickItem *makeValidRegionQmlItem();
    QQuickItem *makeInvalidRegionQmlItem();

private:
    QQmlEngine *mEngine = nullptr;
};

void RegionTests::no_name_set()
{
    Region region {};
    Plugin::Ptr plugin = makeValidPlugin();
    Settings::Ptr settings = makeValidSettings();
    QCOMPARE(region.populate(plugin, settings), false);
}

void RegionTests::population_data()
{
    QTest::addColumn<Plugin::Ptr>("plugin");
    QTest::addColumn<Settings::Ptr>("settings");
    QTest::addColumn<bool>("result");

    QTest::newRow("both empty") << makeInvalidPlugin() << makeInvalidSettings() << false;
    QTest::newRow("plugin empty") << makeInvalidPlugin() << makeValidSettings() << false;
    QTest::newRow("settings empty") << makeValidPlugin() << makeInvalidSettings() << false;
    QTest::newRow("okay") << makeValidPlugin() << makeValidSettings() << true;
}

void RegionTests::population()
{
    QFETCH(Plugin::Ptr, plugin);
    QFETCH(Settings::Ptr, settings);
    QFETCH(bool, result);

    Region region {};
    region.setRegionId(Types::Region::BottomLeft);

    QCOMPARE(region.populate(plugin, settings), result);
}

void RegionTests::activation_data()
{
    QTest::addColumn<QQuickItem*>("pluginObject");
    QTest::addColumn<QQuickItem*>("regionQmlItem");
    QTest::addColumn<bool>("result");

    QQuickItem* nothing = nullptr;
    QTest::newRow("invalid plugin obj, invalid region qml") << nothing << makeInvalidRegionQmlItem() << false;
    QTest::newRow("invalid plugin obj, valid region qml") << nothing << makeValidRegionQmlItem() << false;
    QTest::newRow("valid plugin obj, invalid region qml") << makeValidPluginObject() << makeInvalidRegionQmlItem() << false;
    QTest::newRow("valid plugin obj, valid region qml") << makeValidPluginObject() << makeValidRegionQmlItem() << true;
}

void RegionTests::activation()
{
    QFETCH(QQuickItem*, pluginObject);
    QFETCH(QQuickItem*, regionQmlItem);
    QFETCH(bool, result);

    Region region {};
    region.setRegionId(Types::Region::BottomLeft);

    Plugin::Ptr plugin { makeValidPlugin() };
    Settings::Ptr settings { makeValidSettings() };
    QCOMPARE(region.populate(plugin, settings), true);

    region.setRegionQmlItem(regionQmlItem);
    region.setPluginObject(pluginObject);

    QCOMPARE(region.activate(), result);

    delete pluginObject;
    delete regionQmlItem;
}

void RegionTests::timerCreate()
{
    Region region {};
    SmartTimer *timer = region.createTimer("test", 1000, false);
    QVERIFY(timer != nullptr);
    QCOMPARE(timer->id(), "test");

}

Plugin::Ptr RegionTests::makeValidPlugin()
{
    Plugin::Ptr ptr = QSharedPointer<Plugin>::create("SpecchioTest.QmlSimple", s_pluginPathQmlSimple, "QmlSimple", "0.1", QString(/*library*/));
    Q_ASSERT(ptr.isNull() == false);
    Q_ASSERT(ptr->load() == true);
    Q_ASSERT(ptr->isValid() == true);
    Q_ASSERT(ptr->version() == QStringLiteral("0.1"));
    return ptr;
}

Settings::Ptr RegionTests::makeValidSettings()
{
    Settings::Ptr ptr = QSharedPointer<Settings>::create(":/plugin/default.toml", QString());
    Q_ASSERT(ptr.isNull() == false);
    Q_ASSERT(ptr->valid() == true);
    return ptr;
}

QQuickItem *RegionTests::makeValidPluginObject()
{
  auto item = new QQuickItem{};
  return item;
}

QQuickItem *RegionTests::makeInvalidRegionQmlItem()
{
  auto item = new QQuickItem{};
  return item;
}

QQuickItem *RegionTests::makeValidRegionQmlItem()
{
    Q_ASSERT(mEngine != nullptr);
    QQmlComponent component { mEngine, ":/region/qml/main.qml" };
    auto item = qobject_cast<QQuickItem*>(component.create());
    if (item == nullptr) {
        Q_FOREACH(QQmlError error, component.errors()) {
            qWarning() << "QML loading error:" << error.description();
        }
        Q_ASSERT_X(false, "createQmlInterface", "QML loading failed");
    }
    return item;
}

void RegionTests::initTestCase()
{
    Paths::setPluginsPaths({ QStringLiteral(SPECCHIO_SOURCE_DIR) + "/tests/plugins/" /* Must end with slash */ });

    mEngine = new QQmlEngine(this);
    mEngine->addImportPath(Paths::getPluginsPaths().first());
}

void RegionTests::cleanupTestCase()
{
    if (mEngine != nullptr) {
        delete mEngine;
        mEngine = nullptr;
    }
}

QTEST_MAIN(RegionTests);
#include "RegionTests.moc"
