<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>Calendar</name>
    <message>
        <location filename="../plugins/Specchio.Calendar/Calendar.qml" line="22"/>
        <source>Loading calendars...</source>
        <translation>Caricamento calendari...</translation>
    </message>
    <message>
        <location filename="../plugins/Specchio.Calendar/Calendar.qml" line="28"/>
        <source>No events</source>
        <translation>Nessun evento</translation>
    </message>
</context>
<context>
    <name>Compliments</name>
    <message>
        <location filename="../plugins/Specchio.Compliments/Compliments.qml" line="34"/>
        <source>Hello</source>
        <translation>Ciao</translation>
    </message>
</context>
<context>
    <name>DigitalClock</name>
    <message>
        <location filename="../plugins/Specchio.DigitalClock/DigitalClock.qml" line="139"/>
        <source>MMMM d</source>
        <comment>Localized date format, see https://doc.qt.io/qt-5/qml-qtqml-date.html#format-strings</comment>
        <translation>d MMMM</translation>
    </message>
</context>
<context>
    <name>EventsModel</name>
    <message>
        <location filename="../plugins/Specchio.Calendar/eventsmodel.cpp" line="125"/>
        <source>MMM d &apos;yy</source>
        <comment>QDateTime format string, future date with year</comment>
        <translation>d MMM &apos;yy</translation>
    </message>
    <message>
        <location filename="../plugins/Specchio.Calendar/eventsmodel.cpp" line="127"/>
        <source>tomorrow</source>
        <comment>future date</comment>
        <translation>domani</translation>
    </message>
    <message>
        <location filename="../plugins/Specchio.Calendar/eventsmodel.cpp" line="129"/>
        <source>today</source>
        <comment>future date</comment>
        <translation>oggi</translation>
    </message>
    <message>
        <location filename="../plugins/Specchio.Calendar/eventsmodel.cpp" line="131"/>
        <source>MMM d</source>
        <comment>QDateTime format string, future date without year</comment>
        <translation>d MMM</translation>
    </message>
    <message>
        <location filename="../plugins/Specchio.Calendar/eventsmodel.cpp" line="155"/>
        <source>HH:mm</source>
        <comment>QTime format string, future time</comment>
        <translation>HH:mm</translation>
    </message>
    <message numerus="yes">
        <location filename="../plugins/Specchio.Calendar/eventsmodel.cpp" line="141"/>
        <source>%Ln day(s)</source>
        <comment>distance in time</comment>
        <translation>
            <numerusform>%Ln giorno</numerusform>
            <numerusform>%Ln giorni</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../plugins/Specchio.Calendar/eventsmodel.cpp" line="143"/>
        <source>%Ln hour(s)</source>
        <comment>distance in time</comment>
        <translation>
            <numerusform>un&apos;ora</numerusform>
            <numerusform>%Ln ore</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../plugins/Specchio.Calendar/eventsmodel.cpp" line="145"/>
        <source>%Ln minute(s)</source>
        <comment>distance in time</comment>
        <translation>
            <numerusform>%Ln minuto</numerusform>
            <numerusform>%Ln minuti</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../plugins/Specchio.Calendar/eventsmodel.cpp" line="149"/>
        <source>%1 (%2)</source>
        <comment>1: future date without time; 2: duration</comment>
        <translation>%1 (%2)</translation>
    </message>
    <message>
        <location filename="../plugins/Specchio.Calendar/eventsmodel.cpp" line="153"/>
        <source>%1, %2 (%3)</source>
        <comment>1: future date; 2: time; 3: duration</comment>
        <translation>%1, %2 (%3)</translation>
    </message>
</context>
<context>
    <name>GPIO</name>
    <message>
        <location filename="../plugins/Specchio.GPIO/GPIO.qml" line="26"/>
        <source>GPIO: unavailable</source>
        <translation>GPIO: non disponibile</translation>
    </message>
    <message>
        <location filename="../plugins/Specchio.GPIO/GPIO.qml" line="32"/>
        <source>GPIO: Invalid configuration</source>
        <translation>GPIO: Configurazione non valida</translation>
    </message>
    <message>
        <location filename="../plugins/Specchio.GPIO/GPIO.qml" line="88"/>
        <source>None</source>
        <translation>Nessuno</translation>
    </message>
    <message>
        <location filename="../plugins/Specchio.GPIO/GPIO.qml" line="92"/>
        <source>Active pins: %1</source>
        <translation>Pin attivi: %1</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../resources/qml/Main.qml" line="15"/>
        <source>Specchio</source>
        <translation>Specchio</translation>
    </message>
    <message>
        <location filename="../resources/qml/Main.qml" line="53"/>
        <source>Internet connection unavailable</source>
        <translation>Connessione ad Internet non disponibile</translation>
    </message>
</context>
<context>
    <name>News</name>
    <message>
        <location filename="../plugins/Specchio.News/News.qml" line="53"/>
        <location filename="../plugins/Specchio.News/News.qml" line="131"/>
        <source>Loading news...</source>
        <translation>Caricamento notizie...</translation>
    </message>
    <message>
        <location filename="../plugins/Specchio.News/News.qml" line="137"/>
        <source>Invalid settings</source>
        <translation>Impostazioni non valide</translation>
    </message>
    <message>
        <location filename="../plugins/Specchio.News/News.qml" line="144"/>
        <source>Cannot get any news at the moment</source>
        <translation>Impossibile caricare le notizie al momento</translation>
    </message>
    <message>
        <location filename="../plugins/Specchio.News/News.qml" line="186"/>
        <source>%1 - %2</source>
        <translation>%1 - %2</translation>
    </message>
</context>
<context>
    <name>PiDisplay</name>
    <message>
        <location filename="../plugins/Specchio.PiDisplay/PiDisplay.qml" line="36"/>
        <source>Display off %1</source>
        <translation>Schermo spento %1</translation>
    </message>
    <message>
        <location filename="../plugins/Specchio.PiDisplay/PiDisplay.qml" line="126"/>
        <source>Display on</source>
        <translation>Schermo acceso</translation>
    </message>
    <message>
        <location filename="../plugins/Specchio.PiDisplay/PiDisplay.qml" line="136"/>
        <source>Display off</source>
        <translation>Schermo spento</translation>
    </message>
    <message>
        <location filename="../plugins/Specchio.PiDisplay/PiDisplay.qml" line="149"/>
        <source>now</source>
        <translation>ora</translation>
    </message>
    <message numerus="yes">
        <location filename="../plugins/Specchio.PiDisplay/PiDisplay.qml" line="151"/>
        <source>in %Ln second(s)</source>
        <comment>time to future moment</comment>
        <translation>
            <numerusform>tra %Ln secondo</numerusform>
            <numerusform>tra %Ln secondi</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../plugins/Specchio.PiDisplay/PiDisplay.qml" line="153"/>
        <source>in %Ln minute(s)</source>
        <comment>time to future moment</comment>
        <translation>
            <numerusform>tra %Ln minuto</numerusform>
            <numerusform>tra %Ln minuti</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../plugins/Specchio.PiDisplay/PiDisplay.qml" line="155"/>
        <source>in %Ln hour(s)</source>
        <comment>time to future moment</comment>
        <translation>
            <numerusform>tra un&apos;ora</numerusform>
            <numerusform>tra %Ln ore</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>in %Ln second(s)</source>
        <comment>time from future moment</comment>
        <translation type="vanished">
            <numerusform>tra %Ln secondo</numerusform>
            <numerusform>tra %Ln secondi</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>in %Ln minute(s)</source>
        <comment>time from future moment</comment>
        <translation type="vanished">
            <numerusform>tra %Ln minuto</numerusform>
            <numerusform>tra %Ln minuti</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>in %Ln hour(s)</source>
        <comment>time from future moment</comment>
        <translation type="vanished">
            <numerusform>tra %Ln ora</numerusform>
            <numerusform>tra %Ln ore</numerusform>
        </translation>
    </message>
    <message>
        <source>in %1 seconds</source>
        <translation type="vanished">in %1 secondi</translation>
    </message>
    <message>
        <source>in %1 minutes</source>
        <translation type="vanished">in %1 minuti</translation>
    </message>
    <message>
        <source>in %1 hours</source>
        <translation type="vanished">in %1 ore</translation>
    </message>
</context>
<context>
    <name>Provider</name>
    <message>
        <location filename="../plugins/Specchio.Weather/provider.cpp" line="60"/>
        <source>Weather: Configure your city</source>
        <translation>Meteo: Configura la tua città</translation>
    </message>
    <message>
        <location filename="../plugins/Specchio.Weather/provider.cpp" line="64"/>
        <source>Weather: Configure your API key</source>
        <translation>Meteo: Configura la tua chiave API</translation>
    </message>
    <message>
        <location filename="../plugins/Specchio.Weather/provider.cpp" line="122"/>
        <source>Weather: Invalid API key</source>
        <translation>Meteo: Chiave API non valida</translation>
    </message>
</context>
<context>
    <name>Region</name>
    <message>
        <location filename="../resources/qml/Region.qml" line="109"/>
        <source>&lt;%1: %2&gt;</source>
        <translation>&lt;%1: %2&gt;</translation>
    </message>
    <message>
        <location filename="../resources/qml/Region.qml" line="109"/>
        <source>&lt;Empty&gt;</source>
        <translation>&lt;Vuota&gt;</translation>
    </message>
    <message>
        <location filename="../src/region.cpp" line="41"/>
        <source>Unknown plugin</source>
        <translation>Plugin sconosciuto</translation>
    </message>
    <message>
        <location filename="../src/region.cpp" line="46"/>
        <source>Invalid plugin settings</source>
        <translation>Impostazioni plugin non valide</translation>
    </message>
    <message>
        <location filename="../src/region.cpp" line="56"/>
        <source>Load failed</source>
        <translation>Caricamento fallito</translation>
    </message>
    <message>
        <location filename="../src/region.cpp" line="105"/>
        <location filename="../src/region.cpp" line="131"/>
        <source>Start failed</source>
        <translation>Avvio fallito</translation>
    </message>
    <message>
        <location filename="../src/region.cpp" line="137"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/region.cpp" line="264"/>
        <source>None</source>
        <translation>Nessuno</translation>
    </message>
</context>
<context>
    <name>Specchio</name>
    <message numerus="yes">
        <source>%n year(s)</source>
        <comment>distance in time</comment>
        <translation type="vanished">
            <numerusform>%n anno</numerusform>
            <numerusform>%n anni</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n month(s)</source>
        <comment>distance in time</comment>
        <translation type="vanished">
            <numerusform>%n mese</numerusform>
            <numerusform>%n mesi</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n week(s)</source>
        <comment>distance in time</comment>
        <translation type="vanished">
            <numerusform>%n settimana</numerusform>
            <numerusform>%n settimane</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/specchio.cpp" line="116"/>
        <source>tomorrow</source>
        <comment>distance in time</comment>
        <translation>domani</translation>
    </message>
    <message>
        <location filename="../src/specchio.cpp" line="118"/>
        <source>yesterday</source>
        <comment>distance in time</comment>
        <translation>ieri</translation>
    </message>
    <message numerus="yes">
        <source>%n day(s)</source>
        <comment>distance in time</comment>
        <translation type="vanished">
            <numerusform>%n giorno</numerusform>
            <numerusform>%n giorni</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n hour(s)</source>
        <comment>distance in time</comment>
        <translation type="vanished">
            <numerusform>%n ora</numerusform>
            <numerusform>%n ore</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n minute(s)</source>
        <comment>distance in time</comment>
        <translation type="vanished">
            <numerusform>%n minuto</numerusform>
            <numerusform>%n minuti</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/specchio.cpp" line="108"/>
        <source>%Ln year(s)</source>
        <comment>distance in time</comment>
        <translation>
            <numerusform>%Ln anno</numerusform>
            <numerusform>%Ln anni</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/specchio.cpp" line="110"/>
        <source>%Ln month(s)</source>
        <comment>distance in time</comment>
        <translation>
            <numerusform>%Ln mese</numerusform>
            <numerusform>%Ln mesi</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/specchio.cpp" line="112"/>
        <source>%Ln week(s)</source>
        <comment>distance in time</comment>
        <translation>
            <numerusform>%Ln settimana</numerusform>
            <numerusform>%Ln settimane</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/specchio.cpp" line="120"/>
        <source>%Ln day(s)</source>
        <comment>distance in time</comment>
        <translation>
            <numerusform>%Ln giorno</numerusform>
            <numerusform>%Ln giorni</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/specchio.cpp" line="122"/>
        <source>%Ln hour(s)</source>
        <comment>distance in time</comment>
        <translation>
            <numerusform>un&apos;ora</numerusform>
            <numerusform>%Ln ore</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/specchio.cpp" line="124"/>
        <source>%Ln minute(s)</source>
        <comment>distance in time</comment>
        <translation>
            <numerusform>%Ln minuto</numerusform>
            <numerusform>%Ln minuti</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/specchio.cpp" line="127"/>
        <source>today</source>
        <comment>distance in time</comment>
        <translation>oggi</translation>
    </message>
    <message>
        <location filename="../src/specchio.cpp" line="129"/>
        <source>now</source>
        <comment>distance in time</comment>
        <translation>adesso</translation>
    </message>
    <message>
        <location filename="../src/specchio.cpp" line="132"/>
        <source>in %1</source>
        <comment>future events, eg. &apos;in 5 minutes&apos;</comment>
        <translation>tra %1</translation>
    </message>
    <message>
        <location filename="../src/specchio.cpp" line="134"/>
        <source>%1 ago</source>
        <comment>past events, eg. &apos;5 minutes ago&apos;</comment>
        <translation>%1 fa</translation>
    </message>
</context>
<context>
    <name>WakeUpWord</name>
    <message>
        <location filename="../plugins/Specchio.WakeUpWord/WakeUpWord.qml" line="20"/>
        <source>Listening</source>
        <translation>Ascolto</translation>
    </message>
    <message>
        <location filename="../plugins/Specchio.WakeUpWord/WakeUpWord.qml" line="25"/>
        <source>WuW not ready</source>
        <translation>WuW non pronto</translation>
    </message>
    <message>
        <location filename="../plugins/Specchio.WakeUpWord/WakeUpWord.qml" line="30"/>
        <source>Invalid WuW settings</source>
        <translation>Impostazioni WuW non valide</translation>
    </message>
</context>
<context>
    <name>Weather</name>
    <message>
        <location filename="../plugins/Specchio.Weather/Weather.qml" line="26"/>
        <source>Loading weather...</source>
        <translation>Caricamento meteo...</translation>
    </message>
</context>
</TS>
