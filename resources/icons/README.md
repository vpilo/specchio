# Specchio Icon

## Source

The Specchio icon is composed by the following icons:

* "Mirror free icon"
  An icon made by [smalllikeart](https://www.flaticon.com/authors/smalllikeart) from [www.flaticon.com](https://www.flaticon.com)
* "Brain free icon"
  An icon made by [twitter](https://www.flaticon.com/authors/twitter) from [www.flaticon.com](https://www.flaticon.com)

## Licensing

* Both icons are licensed under the [FlatIcon Basic License](https://file000.flaticon.com/downloads/license/license.pdf)
