/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

import QtQuick 2.8
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2

import Specchio 1.0
import Specchio.Internal 1.0

Window {
    id: rootWindow
    title: QT_TR_NOOP('Specchio')

    minimumWidth: 480
    minimumHeight: 480

    Rectangle {
        id: rootRect

        transform: Rotation {
            id: rootRectRotate
            angle: Specchio.rotation
            origin.x: rootWindow.width / 2
            origin.y: rootWindow.height / 2
        }

        x: d.rotationOffset
        y: d.rotationOffset
        width: d.isVertical ? rootWindow.width : rootWindow.height
        height: d.isVertical ? rootWindow.height : rootWindow.width

        QtObject {
            id: d
            property bool isVertical: Specchio.rotation == 0 || Specchio.rotation == 180
            property int rotationOffset: Math.sin(Specchio.rotation * Math.PI / 180) * (rootWindow.width - rootWindow.height) / 2
        }

        color: Specchio.backgroundColor

        RegionGrid {
            anchors.fill: parent
            anchors.margins: Units.spacing.medium
        }

        AdaptableText {
            anchors { left: parent.horizontalCenter ; right: parent.right ; bottom: parent.bottom }
            visible: !Specchio.connected
            opacity: .5

            text: Specchio.connected ? "" : qsTr("Internet connection unavailable")
            color: "darkgray"
            maximumSize: Units.fontSize.small
            horizontalAlignment: Text.AlignHCenter

            Rectangle {
                anchors.fill: parent
                color: "#400"
                z: -1
            }
        }
    }
}
