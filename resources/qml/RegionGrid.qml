/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

import QtQuick 2.8
import QtQuick.Layouts 1.3

import Specchio 1.0
import Specchio.Internal 1.0

GridLayout {
    columnSpacing: Units.spacing.small
    rowSpacing: Units.spacing.small

    columns: 3
    rows: 7

    Region {
        Layout.row: 0
        Layout.column: 0
        Layout.columnSpan: 3

        regionId: RegionId.TopBar
        color: 'lightgray'
    }

    Region {
        Layout.row: 1
        Layout.column: 0

        regionId: RegionId.TopLeft
        color: 'lightcoral'
    }

    Region {
        Layout.row: 1
        Layout.column: 1

        regionId: RegionId.TopCenter
        color: 'lightblue'
    }

    Region {
        Layout.row: 1
        Layout.column: 2

        regionId: RegionId.TopRight
        color: 'lightgreen'
    }

    Region {
        Layout.row: 2
        Layout.column: 0
        Layout.columnSpan: 3

        regionId: RegionId.UpperThird
        color: 'yellow'
    }

    Region {
        Layout.row: 3
        Layout.column: 0
        Layout.columnSpan: 3

        regionId: RegionId.MiddleCenter
        color: 'cyan'
    }

    Region {
        Layout.row: 4
        Layout.column: 0
        Layout.columnSpan: 3

        regionId: RegionId.LowerThird
        color: 'magenta'
    }

    Region {
        Layout.row: 5
        Layout.column: 0

        regionId: RegionId.BottomLeft
        color: 'darkred'
    }

    Region {
        Layout.row: 5
        Layout.column: 1

        regionId: RegionId.BottomCenter
        color: 'darkblue'
    }

    Region {
        Layout.row: 5
        Layout.column: 2

        regionId: RegionId.BottomRight
        color: 'darkgreen'
    }

    Region {
        Layout.row: 6
        Layout.column: 0
        Layout.columnSpan: 3

        regionId: RegionId.BottomBar
        color: 'darkgray'
    }
}
