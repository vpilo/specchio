/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

import QtQuick 2.8
import QtWayland.Compositor 1.1

WaylandCompositor {
    id: compositorRoot

    useHardwareIntegrationExtension: true

    XdgShellV6 {
        onToplevelCreated: screen.handleShellSurface(xdgSurface)
    }

    DisplayOutput {
        id: screen
        compositor: compositorRoot
    }
}
