/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

import QtQuick 2.8

import Specchio 1.0

Text {
    id: root

    property real maximumSize: Units.fontSize.large
    property real minimumSize: Units.fontSize.tiny

    minimumPointSize: minimumSize
    font.pointSize: maximumSize
    font.family: Specchio.font.family + ', Twitter Color Emoji'

    fontSizeMode: Text.Fit
    wrapMode: Text.WordWrap
    elide: Text.ElideRight
}
