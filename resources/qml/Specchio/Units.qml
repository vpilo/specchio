/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

pragma Singleton

import QtQuick 2.6
import QtQuick.Window 2.0

import Specchio 1.0

/**
 * Definitions of standard units (spaces, sizes) to allow the various plugins to look similar,
 * and for fonts to adapt to different screen sizes properly.
 *
 * Unless otherwise noted, all screen-related units are device-independent.
 */
QtObject {

    /**
     * Standardized sizes for icons.
     * Unit is pixels.
     */
    property QtObject iconSize: QtObject {
        /// The smallest icon size. Unit is pixels.
        property int small:        __private__.iconSize(16)
        /// Very small icon size. Unit is pixels.
        property int smallMedium:  __private__.iconSize(32)
        /// Medium icon size. Unit is pixels.
        property int medium:       __private__.iconSize(48)
        /// Large icon size. Unit is pixels.
        property int large:        __private__.iconSize(64)
        /// The biggest icon size. Unit is pixels.
        property int huge:         __private__.iconSize(128)
    }

    /**
     * Standardized sizes for text.
     * Unit is pixels.
     */
    property QtObject fontSize: QtObject {
        /// The smallest font size. Unit is pixels.
        property int tiny:         __private__.fontSize(8)
        /// Very small font size. Unit is pixels.
        property int small:        __private__.fontSize(16)
        /// Small font size. Unit is pixels.
        property int smallMedium:  __private__.fontSize(24)
        /// Medium font size. Unit is pixels.
        property int medium:       __private__.fontSize(36)
        /// Large font size. Unit is pixels.
        property int large:        __private__.fontSize(48)
        /// The biggest font size. Unit is pixels.
        property int huge:         __private__.fontSize(64)
    }

    /**
     * UI spacings between elements.
     * Unit is pixels.
     */
    property QtObject spacing: QtObject {
        /// Large spacing. Unit is pixels.
        property int large:   __private__.baseSize
        /// Medium spacing. Unit is pixels.
        property int medium:  Math.floor(__private__.baseSize / 2)
        /// Small spacing. Unit is pixels.
        property int small:   Math.floor(__private__.baseSize / 4)
        /// Tiny spacing. For a faint but present UI element separation. Unit is pixels.
        property int tiny:    Math.floor(__private__.baseSize / 12)
    }

    /**
     * Durations of time.
     * Can be used for determining timer event delays, or for UI animations.
     * Unit is milliseconds.
     */
    property QtObject duration: QtObject {
        /// Long duration. Use for fading between carousel items. Unit is milliseconds.
        property int lenghty:  500
        /// Medium duration. Use for UI fading or not instant animations. Unit is milliseconds.
        property int medium:   250
        /// Short duration. Use for nicely updating UI elements. Unit is milliseconds.
        property int brief:    150
        /// A duration of a second. Unit is milliseconds.
        property int second:   1000
        /// A duration of a minute. Unit is milliseconds.
        property int minute:   second * 60
        /// A duration of a minute. Unit is milliseconds.
        property int hour:     minute * 60
    }

    property QtObject __private__: Item {
        /**
        * Font metrics.
        * Values depend on which default font is in use by the user.
        * Most values in the application depend on these.
        */
        property variant metrics: TextMetrics {
            text: 'M'
        }

        /**
         * Basic element to define space.
         * Unit is pixels.
         */
        property int baseSize: metrics.height

        /**
         * Ratio between physical and device-independent pixels.
         */
        property real devicePixelRatio: Math.max(1, (metrics.font.pixelSize * 0.75) / metrics.font.pointSize)

        /**
         * Adapt a font size to the screen.
         * Unit is pixels.
         */
        function fontSize(size) {
            return Math.floor(size * Screen.devicePixelRatio * devicePixelRatio)
        }

        /**
         * Adapt an icon's dimensions to the screen.
         * Unit is pixels.
         */
        function iconSize(size) {
            return Math.floor(size * Screen.devicePixelRatio * devicePixelRatio)
        }
    }
}
