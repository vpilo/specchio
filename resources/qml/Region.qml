/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

import QtQuick 2.8
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.1

import Specchio 1.0
import Specchio.Internal 1.0

Item {
    id: root

    property alias regionId : priv.regionId

    Item {
        id: pluginProperties

        property alias regionName : priv.name
        property alias pluginName : priv.pluginName
        property alias pluginPath : priv.pluginPath

        property alias positionedAtLeft : priv.positionedAtLeft
        property alias positionedAtRight : priv.positionedAtRight
        property alias positionedAtTop : priv.positionedAtTop
        property alias positionedAtBottom : priv.positionedAtBottom
        property alias positionedAtCenter : priv.positionedAtCenter

        function sendBroadcast(name, extras) {
            priv.sendBroadcast(name, extras)
        }
        signal broadcast(string name, var extras)

        function timer(id, intervalSeconds, triggeredImmediately) {
            return priv.createTimer(id, intervalSeconds, triggeredImmediately)
        }
        function networkTimer(id, intervalSeconds, triggerOnStart) {
            return priv.createNetworkTimer(id, intervalSeconds, triggerOnStart)
        }
        function getTimer(id) {
            return priv.getTimer(id)
        }
        function removeTimer(id) {
            return priv.removeTimer(id)
        }
    }

    RegionImpl {
        id: priv
        regionQmlItem: root
        pluginObject: pluginProperties
    }

    // Plugin's debug logging tag
    property var tag: LoggingCategory {
        name: priv.name
        objectName: 'tag'
    }

    // Background color to distinguish between regions in debug mode
    property string color

    visible: priv.inUse || Specchio.debugMode

    Layout.fillWidth: true
    Layout.fillHeight: true
    anchors.margins: Units.spacing.small

    Rectangle {
        id: background
        visible: Specchio.debugMode
        anchors.fill: parent

        opacity: .1
        z: 0

        color: parent.color
    }

    Text {
        id: regionNameLabel
        visible: Specchio.debugMode
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.bottom: pluginNameLabel.top

        opacity: .4
        z: 2

        text: priv.name
        color: "darkgray"
        style: Text.Outline
        styleColor: "black"
        font.pointSize: Units.fontSize.tiny
        font.weight: Font.Thin
    }

    Text {
        id: pluginNameLabel
        visible: Specchio.debugMode
        anchors.right: parent.right
        anchors.bottom: parent.bottom

        opacity: .4
        z: 3

        text: priv.inUse ? qsTr('<%1: %2>').arg(priv.pluginName).arg(priv.loadingStatusMessage) : qsTr('<Empty>')
        color: "darkgray"
        style: Text.Outline
        styleColor: "black"
        font.pointSize: Units.fontSize.tiny
        font.weight: Font.Thin
    }
}
