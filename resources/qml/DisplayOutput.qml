/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

import QtQuick 2.8
import QtQuick.Window 2.2
import QtWayland.Compositor 1.1

import Specchio 1.0

WaylandOutput {
    id: displayOutput

    sizeFollowsWindow: true

    property bool isNestedCompositor: Qt.platform.pluginName.startsWith("wayland") || Qt.platform.pluginName === "xcb"

    window: Main {
        id: main

        visible: true
        visibility: (Specchio.debugMode || isNestedCompositor) ? 'Windowed' : 'FullScreen'

        LoggingCategory {
            id: __specchio_tag
            name: 'Specchio.Core'
        }

        Component.onCompleted: {
            if (isNestedCompositor) {
                console.info(__specchio_tag, "Running in nested mode: resizing to window")
                main.width = 1366
                main.height = 768
            }
        }

        onClosing: Qt.quit();

        Shortcut {
            sequence: "Esc"
            context: Qt.ApplicationShortcut
            onActivated: main.close()
        }
        Shortcut {
            sequence: "Alt+F4"
            context: Qt.ApplicationShortcut
            onActivated: main.close()
        }
    }
}
