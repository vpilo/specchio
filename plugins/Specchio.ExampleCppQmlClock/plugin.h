/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#pragma once

#include "plugin_interface.hpp"

#include <QtQml/QQmlExtensionPlugin>


class CppQmlClockPlugin : public QQmlExtensionPlugin, public SpecchioPlugin
{
    Q_OBJECT
    Q_INTERFACES(SpecchioPlugin)
    Q_PLUGIN_METADATA(IID SPECCHIO_PLUGIN_INTERFACE_NAME)

public:
    void registerTypes(const char *uri) override;
    virtual const QString &mainObject() override;
    virtual const QString &version() override;
};
