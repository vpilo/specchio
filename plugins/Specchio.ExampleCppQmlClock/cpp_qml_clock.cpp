/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#include "cpp_qml_clock.h"

#include <QDebug>
#include <QQmlContext>
#include <QQmlEngine>
#include <QTimer>

namespace {
    const int s_updateIntervalMsec = 1000;
} // namespace


CppQmlClock::CppQmlClock(QQuickItem *parent)
: QQuickItem(parent)
, mUpdateTimer(new QTimer(this))
{
    connect(mUpdateTimer, &QTimer::timeout, this, &CppQmlClock::update);
    mUpdateTimer->start(s_updateIntervalMsec);
}

void CppQmlClock::update()
{
    // The QML context & engine are not available in the constructor
    if (mClock == nullptr) {
        QQmlEngine *engine = qmlEngine(this);
        engine->rootContext()->setContextProperty("plugin", this);

        QQmlComponent component(engine, QUrl(QStringLiteral("qrc:///qml/CppQmlClock.qml")));
        mClock = qobject_cast<QQuickItem*>(component.create());
        if (mClock == nullptr) {
            qWarning() << "Component initialization failed";
            Q_FOREACH(const QQmlError &err, component.errors()) {
                qWarning() << "->" << err;
            }
            mUpdateTimer->stop();
            return;
        }

        mClock->setParentItem(this);
        qDebug() << "CppQmlClock set up:" << mClock;
    }

    emit timeChanged();

    QDate today = QDate::currentDate();
    if (mLastDay != today) {
        emit dateChanged();
    }
    mLastDay = today;
}

const QString CppQmlClock::clockTime() const
{
    return QTime::currentTime().toString(Qt::SystemLocaleLongDate);
}

const QString CppQmlClock::clockDate() const
{
    return QDate::currentDate().toString(Qt::SystemLocaleLongDate);
}
