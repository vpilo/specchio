/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

import QtQuick 2.9
import QtQuick.Layouts 1.1

ColumnLayout {
    anchors.margins: 10

    Text {
        Layout.alignment: Qt.AlignHCenter
        text: plugin.clockTime
        color: "white"
        style: Text.Raised
        styleColor: "lightgray"
        font.weight: Font.Bold
        font.pointSize: 24
    }
    Text {
        Layout.alignment: Qt.AlignHCenter
        text: plugin.clockDate
        color: "lightgray"
        style: Text.Raised
        styleColor: "darkgray"
        font.weight: Font.Light
        font.pointSize: 16
    }
}
