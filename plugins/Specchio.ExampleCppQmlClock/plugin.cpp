/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#include "plugin.h"
#include "cpp_qml_clock.h"

#include <QtQml/QQmlEngine>


#define PLUGIN_OBJECT CppQmlClock
#define PLUGIN_VERSION_MAJOR 1
#define PLUGIN_VERSION_MINOR 0


#define _TO_STRING_DONTUSE_(s) #s
#define TO_STRING(s) _TO_STRING_DONTUSE_(s)


namespace {
    const QString s_mainObject { QStringLiteral(TO_STRING(PLUGIN_OBJECT)) };
    const QString s_version { QStringLiteral("%1.%2").arg(PLUGIN_VERSION_MAJOR).arg(PLUGIN_VERSION_MINOR) };
}


void CppQmlClockPlugin::registerTypes(const char *uri)
{
    qmlRegisterType<PLUGIN_OBJECT>(uri, 1, 0, TO_STRING(PLUGIN_OBJECT) );
}

const QString &CppQmlClockPlugin::mainObject()
{
    return s_mainObject;
}

const QString &CppQmlClockPlugin::version()
{
    return s_version;
}

