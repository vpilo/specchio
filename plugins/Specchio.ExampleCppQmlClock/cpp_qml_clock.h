/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#pragma once

#include <QtCore/QDateTime>
#include <QtCore/QString>
#include <QtQuick/QQuickItem>

class CppQmlClock : public QQuickItem
{
    Q_OBJECT

    Q_PROPERTY(QString clockTime READ clockTime NOTIFY timeChanged);
    Q_PROPERTY(QString clockDate READ clockDate NOTIFY dateChanged);

public:
    CppQmlClock(QQuickItem *parent = 0);

    const QString clockTime() const;
    const QString clockDate() const;

signals:
    void timeChanged();
    void dateChanged();

private Q_SLOTS:
    void update();

private:
    QTimer *mUpdateTimer;

    QDate mLastDay;
    QString mLastTime;
    QString mLastDate;

    QQuickItem *mClock = nullptr;
};
