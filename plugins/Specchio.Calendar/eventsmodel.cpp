
#include "eventsmodel.h"

#include <QtCore/QLoggingCategory>
#include <QtCore/QMapIterator>


Q_LOGGING_CATEGORY(plMod, "Specchio.Calendar.Model", QtInfoMsg);

namespace {
    const QHash<int, QByteArray> s_dataRoles {
        { TitleRole, "title" },
        { DateBeginRole, "dateBegin" },
        { DateEndRole, "dateEnd" },
        { LocationRole, "location" },
        { WholeDayRole, "isWholeDay" },
        { FullDateStringRole, "fullDateString" },
        { ColorRole, "calendarColor" },
    };
} // namespace

EventsModel::EventsModel(QObject *parent)
: QAbstractListModel(parent)
{
}

void EventsModel::clear()
{
    beginResetModel();
    mEvents.clear();
    endResetModel();
}

void EventsModel::insertEvents(const Events &events)
{
    int count = rowCount();
    beginInsertRows(QModelIndex(), count, count + events.count() - 1);

    Q_FOREACH(auto ev, events) {
        mEvents.insertMulti(ev.dateBegin.toMSecsSinceEpoch(), ev);
    }

    endInsertRows();
}

bool EventsModel::hasEvents(const QString& ownerId) const
{
    return std::any_of(mEvents.begin(), mEvents.end(), [ownerId](const EventData& entry) { return entry.owner->id == ownerId; });
}

void EventsModel::removeEvents(const QString &ownerId)
{
    beginResetModel();
    QMap<qint64, EventData>::iterator it = mEvents.begin();
    while (it != mEvents.end()) {
        if (it.value().owner->id == ownerId) {
            it = mEvents.erase(it);
        } else {
            ++it;
        }
    }
    endResetModel();
}

int EventsModel::rowCount(const QModelIndex &parent) const {
    Q_UNUSED(parent);
    return mEvents.count();
}

QVariant EventsModel::data(const QModelIndex &index, int role) const {
    int current = 0;
    const int row = index.row();
    if (row < 0 || row >= mEvents.count() || index.column() != 0) {
        return {};
    }

    const EventData *event = nullptr;
    QMapIterator<qint64, EventData> it(mEvents);
    while (it.hasNext()) {
        it.next();
        if (current == row) {
            event = &it.value();
            break;
        }
        current++;
    }
    if (event == nullptr) {
        qCWarning(plMod) << "Unable to find element at row" << row << "of" << mEvents.count();
        return {};
    }

    switch (static_cast<EventDataRole>(role)) {
        case TitleRole:      return event->title;
        case DateBeginRole:  return event->dateBegin;
        case DateEndRole:    return event->dateEnd;
        case LocationRole:   return event->location;
        case WholeDayRole:   return event->isWholeDay;
        case FullDateStringRole: return timestamp(event->dateBegin.toLocalTime(), event->dateEnd.toLocalTime(), event->isWholeDay);
        case ColorRole:      return event->calendarColor;
    }

    return {};
}

QHash<int, QByteArray> EventsModel::roleNames() const {
    return s_dataRoles;
}

void EventsModel::refresh()
{
    // The dataChanged signal cannot be used, since the data itself did not actually change.
    emit layoutAboutToBeChanged();
    emit layoutChanged();
}

QString EventsModel::timestamp(const QDateTime &start, const QDateTime &end, bool isFullDay) const
{
    const QDateTime now { QDateTime::currentDateTimeUtc() };
    const qint64 daysTo = now.daysTo( start );
    qint64 durationSeconds = abs( start.secsTo( end ));

    QString templateDate;

    if (now.date().year() != start.date().year() && daysTo > 180) {
        templateDate = start.toString(tr("MMM d 'yy", "QDateTime format string, future date with year"));
    } else if (daysTo == 1) {
        templateDate = tr("tomorrow", "future date");
    } if (daysTo == 0) {
        templateDate = tr("today", "future date");
    } else {
        templateDate = start.toString(tr("MMM d", "QDateTime format string, future date without year"));
    }

    constexpr qint64 s_oneMinute = 60;
    constexpr qint64 s_oneHour = s_oneMinute * 60;
    constexpr qint64 s_oneDay = s_oneHour * 24;

    QString templateDuration;

    if (durationSeconds > s_oneDay) {
        templateDuration = tr("%Ln day(s)", "distance in time", durationSeconds / s_oneDay);
    } else if (durationSeconds >= s_oneHour) {
        templateDuration = tr("%Ln hour(s)", "distance in time", durationSeconds / s_oneHour);
    } else {
        templateDuration = tr("%Ln minute(s)", "distance in time", durationSeconds / s_oneMinute);
    }

    if (isFullDay) {
        return tr("%1 (%2)", "1: future date without time; 2: duration")
        .arg(templateDate)
        .arg(templateDuration);
    }
    return tr("%1, %2 (%3)", "1: future date; 2: time; 3: duration")
        .arg(templateDate)
        .arg(start.toString(tr("HH:mm", "QTime format string, future time")))
        .arg(templateDuration);
}
