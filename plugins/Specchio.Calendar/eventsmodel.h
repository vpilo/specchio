
#pragma once

#include "types.h"

#include <QtCore/QAbstractListModel>
#include <QtCore/QModelIndex>
#include <QtCore/QVariant>


class EventsModel : public QAbstractListModel {
    Q_OBJECT

public:
    EventsModel(QObject *parent = nullptr);

    void clear();
    void insertEvents(const Events &events);
    bool hasEvents(const QString &ownerId) const;
    void removeEvents(const QString &ownerId);
    void refresh();

    QString timestamp(const QDateTime &start, const QDateTime &end, bool isFullDay) const;

public:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

private:
    QMap<qint64, EventData> mEvents;
};
