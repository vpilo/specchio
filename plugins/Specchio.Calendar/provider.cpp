
#include "eventsmodel.h"
#include "provider.h"
#include "parser.h"

#include <QtCore/QLoggingCategory>
#include <QtCore/QUrlQuery>
#include <QtCore/QRandomGenerator>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>


namespace {
    /// Interval between attempts to re-download a calendar which could not be downloaded.
    const int s_retryTimeoutMilliseconds = 1000 * 60 * 15;
} // namespace


Q_LOGGING_CATEGORY(plCal, "Specchio.Calendar", QtInfoMsg);


Provider::Provider(QObject *parent)
: QObject(parent)
{
    mNetMan = new QNetworkAccessManager(this);
    connect(mNetMan, &QNetworkAccessManager::finished,
            this, &Provider::parseReply);
}

Provider::~Provider()
{
    // Delete the events model before the calendars
    if (mModel != nullptr) {
        mModel->clear();
    }
    mCalendars.clear();
}

bool Provider::isReady() const
{
    return !mCalendars.isEmpty();
}

void Provider::setCalendars(const QVariantMap &calendars)
{
    if (calendars.isEmpty()) {
        qCWarning(plCal) << "Empty calendars list given!";
        return;
    }
    QMapIterator<QString, QVariant> it { calendars };
    while (it.hasNext()) {
        it.next();
        const auto &id { it.key() };
        const auto &settings { it.value().toMap() };

        CalendarData::Ptr cal = QSharedPointer<CalendarData>::create();
        cal->id = id;
        cal->name = settings["name"].toString();
        cal->enabled = settings.contains("enabled") ? settings["enabled"].toBool() : true;
        cal->loading = false;
        cal->url = settings["url"].toUrl();

        int refreshIntervalMinutes = settings.contains("refreshMinutes") ? settings["refreshMinutes"].toInt() : 60;
        cal->refreshIntervalMinutes = refreshIntervalMinutes;

        if (settings.contains("color")) {
            cal->color = settings["color"].toString();
        } else {
            QRandomGenerator rand(qHash(cal->name));
            cal->color = QStringLiteral("#%1%2%3")
                .arg(rand.bounded(127, 256), 2, 16)
                .arg(rand.bounded(127, 256), 2, 16)
                .arg(rand.bounded(127, 256), 2, 16);
        }

        mCalendars[id] = cal;
    }
}

void Provider::update(const QString &id)
{
    if (!isReady()) {
        qCDebug(plCal) << "Provider not ready";
        return;
    }
    if (!mCalendars.contains(id)) {
        qCDebug(plCal) << "Calendar" << id << "not found!";
        return;
    }
    auto &cal = mCalendars[id];
    if (!cal->enabled) {
        return;
    }
    if (cal->loading) {
        return;
    }

    qCDebug(plCal) << "Updating calendar" << cal->name;
    QUrl url(cal->url);
    // TODO support for authentication
    //         QUrlQuery query;
    //         query.addQueryItem(QStringLiteral("username"), user);
    //         query.addQueryItem(QStringLiteral("password"), password);
    //         url.setQuery(query);

    QNetworkReply *reply = mNetMan->get(QNetworkRequest(url));

    // When the reply comes back, keep track of which calendar it belongs to
    reply->setProperty("id", cal->id);

    if (!reply->isRunning()) {
        retryDownloadLater(cal->id, QStringLiteral("Network unavailable"));
    } else {
        cal->loading = true;
    }
}

void Provider::retryDownloadLater(const QString &id, const QString &error)
{
    qCInfo(plCal) << "Download failed for calendar" << id << " error" << error << "- retrying soon";
    QTimer::singleShot(s_retryTimeoutMilliseconds, [=]() { update(id); });
}

void Provider::parseReply(QNetworkReply* reply)
{
    Q_ASSERT(mModel != nullptr);
    if (reply == nullptr) {
        return;
    }

    bool failed = true;
    QString error;
    const QString &id { reply->property("id").toString() };

    if (id.isEmpty() || !mCalendars.contains(id)) {
        qCInfo(plCal) << "Received reply for invalid calendar id" << id;
    } else if (reply->error() != QNetworkReply::NoError) {
        error = reply->errorString();
    } else {
        Parser parser(mCalendars[id]);

        if (!parser.parse(*reply)) {
            error = QStringLiteral("Unable to read data");
        } else {
            const Events &events = parser.getEvents();
            if (events.isEmpty()) {
                qCInfo(plCal) << "No events in calendar" << id;
            } else {
                qCDebug(plCal) << "Found" << events.count() << "events in calendar" << id;
                mModel->removeEvents(id);
                mModel->insertEvents(events);
            }
            failed = false;
        }
    }

    if (failed) {
        retryDownloadLater(id, error);
    } else {
        mCalendars[id]->loading = false;
        emit updated(id);
    }

    reply->deleteLater();
}

bool Provider::loading() const
{
    for (auto &cal : mCalendars) {
        if(cal->loading) {
            return true;
        }
    }
    return false;
}

void Provider::setModel(QAbstractItemModel* model)
{
    Q_ASSERT(model != nullptr);
    mModel = qobject_cast<EventsModel*>(model);
}

QVariantMap Provider::calendars() const
{
    QVariantMap results;
    for (const auto &item : mCalendars) {
        results[item->id] = item->refreshIntervalMinutes;
    }
    return results;
}

void Provider::refreshView()
{
    if (mModel != nullptr) {
        mModel->refresh();
    }
}
