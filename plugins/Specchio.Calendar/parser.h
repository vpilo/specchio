#pragma once

#include "types.h"

#include <QtCore/QIODevice>
#include <QtCore/QObject>
#include <QtCore/QSharedPointer>

class ParserPrivate;


class Parser
{
public:
    Parser(CalendarData::Ptr parentCalendar);
    virtual ~Parser();

    // Returns true if parsing succeeded and at least one event was found
    bool parse(QIODevice &data);

    Events getEvents() { return mEvents; };

private:
    Events mEvents;

    /**
     * Shared pointer to parent calendar.
     * All parsed events will be matched to this calendar.
     */
    const CalendarData::Ptr mParentCalendar;

    Q_DECLARE_PRIVATE(Parser);
    ParserPrivate *d_ptr;
};
