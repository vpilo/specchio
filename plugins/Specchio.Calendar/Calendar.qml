import QtQuick 2.6
import QtQuick.Layouts 1.1

import Specchio 1.0

import Specchio.Calendar 1.0

Item {
    id: root

    state: 'loading'
    states: [
        State {
            name: 'ready'
            PropertyChanges { target: readyState; opacity: 1 }
            PropertyChanges { target: loadingState; opacity: 0 }
        },
        State {
            name: 'loading'
            PropertyChanges { target: readyState; opacity: 0 }
            PropertyChanges { target: loadingState; opacity: 1 }
            PropertyChanges { target: loadingState; text: qsTr('Loading calendars...') }
        },
        State {
            name: 'none'
            PropertyChanges { target: readyState; opacity: 0 }
            PropertyChanges { target: loadingState; opacity: 1 }
            PropertyChanges { target: loadingState; text: qsTr('No events') }
        }
    ]

    AdaptableText {
        id: loadingState
        anchors.fill: parent

        color: 'white'
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.weight: Font.Thin
        maximumSize: Units.fontSize.medium
    }

    // Main info
    Item {
        id: readyState
        anchors.fill: parent

        Text {
            id: title
            anchors { top: parent.top ; left: parent.left ; right: parent.right }

            color: 'lightgray'
            font.pointSize: Units.fontSize.small
            text: settings.title
        }

        Rectangle {
            id: divider
            anchors { top: title.bottom ; left: parent.left ; right: parent.right }

            height: Units.spacing.tiny
            width: parent.width
            color: 'dimgray'
        }

        ListView {
            id: listView
            anchors { top: divider.bottom ; left: parent.left ; right: parent.right ; bottom: parent.bottom ; topMargin: Units.spacing.small }

            clip: true
            enabled: false
            snapMode: ListView.SnapOneItem
            boundsBehavior: Flickable.StopAtBounds
            orientation: ListView.Vertical

            model: events

            delegate: RowLayout {
                width: listView.width
                visible: (y + height < (ListView.view.contentY + ListView.view.height)) && (y >= ListView.view.contentY)

                Rectangle {
                    id: itemColor
                    width: Units.fontSize.small
                    height: Units.fontSize.small
                    border.width: Units.spacing.tiny
                    border.color: 'dimgray'
                    color: calendarColor
                }
                Text {
                    Layout.fillWidth: true
                    id: itemTitle
                    text: title
                    color: 'lightgray'
                    elide: Text.ElideRight
                    textFormat: Text.PlainText
                }
                Text {
                    color: 'darkgray'
                    text: fullDateString
                }
            }
        }
    }

    Provider {
        id: provider
        model: events
        calendars: settings.calendars

        onUpdated: {
            var count = events.rowCount()
            if (count == 0) {
                root.state = provider.loading() ? 'loading' : 'none'
            } else {
                root.state = 'ready'
                Plugin.sendBroadcast('CALENDAR_UPDATED', { id: calendarId })
            }
        }
    }
    EventsModel {
        id: events
    }

    Connections {
        target: Specchio
        onStarting: {
            if (!provider.ready) {
                return
            }
            for (var calId in provider.calendars) {
                let refreshIntervalMinutes = provider.calendars[calId]
                let timer = Plugin.networkTimer(calId, refreshIntervalMinutes * 60, true)
                timer.triggered.connect(function (id) { provider.update(id) })
            }
        }
        onRefreshView: provider.refreshView()
    }
}
