#pragma once

#include "types.h"

#include <QtCore/QAbstractItemModel>
#include <QtCore/QIODevice>
#include <QtCore/QObject>

class QNetworkAccessManager;
class QNetworkReply;
class QNetworkSession;
class EventsModel;


class Provider : public QObject {
    Q_OBJECT

    Q_PROPERTY(QVariantMap calendars READ calendars WRITE setCalendars);
    Q_PROPERTY(QAbstractItemModel *model MEMBER mModel WRITE setModel);
    Q_PROPERTY(bool ready READ isReady CONSTANT);

public:
    Provider(QObject *parent = nullptr);
    virtual ~Provider();

    bool isReady() const;
    void setCalendars(const QVariantMap &calendars);
    void setModel(QAbstractItemModel *model);

    QVariantMap calendars() const;

public Q_SLOTS:

    Q_INVOKABLE void update(const QString &id);
    Q_INVOKABLE bool loading() const;
    Q_INVOKABLE void refreshView();

private Q_SLOTS:
    void parseReply(QNetworkReply *reply);

private:
    void retryDownloadLater(const QString &id, const QString &error);

private:
    QNetworkAccessManager *mNetMan;

    Calendars mCalendars;
    EventsModel *mModel = nullptr;

Q_SIGNALS:
    void updated(const QString &calendarId);
};
