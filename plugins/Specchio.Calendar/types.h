
#pragma once

#include <QtCore/QDateTime>
#include <QtCore/QMetaType>
#include <QtCore/QSharedPointer>
#include <QtCore/QString>
#include <QtCore/QTimer>
#include <QtCore/QUrl>


enum EventDataRole {
    TitleRole = Qt::UserRole + 1,
    DateBeginRole,
    DateEndRole,
    LocationRole,
    WholeDayRole,
    FullDateStringRole,
    ColorRole
};

struct CalendarData {
    QString id;
    QString name;
    bool enabled;
    bool loading;
    QString color;
    QUrl url;
    int refreshIntervalMinutes;

    /**
     * Shared pointer to a calendar.
     */
    typedef QSharedPointer<CalendarData> Ptr;
};

struct EventData {
    QString title;
    QString location;
    // True if the event lasts from 00.00 of dateBegin to 23.59 of dateEnd
    bool isWholeDay = false;
    QDateTime dateBegin;
    QDateTime dateEnd;

    QString calendarColor;

    CalendarData::Ptr owner;
};

using Calendars = QMap<QString, CalendarData::Ptr>;
using Events = QList<EventData>;
