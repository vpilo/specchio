# Specchio plugin: Calendar

Shows your next calendar events and appointments.

Example:

![Plugin screenshot](calendar-screenshot-1.png)

## Settings

The configuration file contains the following settings:

| Name | Type | Optional/default value | Description |
| --- | --- | --- | --- |
| title | `String` | Yes: `"Upcoming events"` | Text shown above the events. |
| calendars | `Table` | No | The calendars to fetch. |

Every calendar group has the following options:

| Name | Type | Optional/default value | Description |
| --- | --- | --- | --- |
| name | `String` | No | Name of the calendar. |
| url | `String` | No | Where to fetch the calendar from. Accepted URL schemas are `http://` and `https://`. |
| enabled | `Boolean` | Yes: `true` | Whether this calendar is periodically fetched. If false, the calendar will not be fetched nor displayed. |
| color | `String` | Yes: automatic | The color of this calendar. If omitted, it will be randomly generated. |
| refreshMinutes | `Integer` | Yes: `60`, one hour | Interval in minutes between attempts to refresh the calendar events. |

## Example Configurations

Here is an example configuration which disables one of the two default calendars, 'NASA Historic Events' and 'Moon Pheases',
both of which are enabled by default.

```toml
title="Almost Default"

[calendars.moon]
enabled=false
```

This example has only one calendar, a Google private calendar, refreshed twice every hour:

```toml
title="Google Calendar"
[calendars.mine]
name="Google"
url="https://calendar.google.com/calendar/ical/my_google_account%40gmail.com/private-59bcc3ad6775562f845953cf01624225/basic.ics"
refreshMinutes=30
```
