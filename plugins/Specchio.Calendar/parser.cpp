
#include "parser.h"

#include "libical/ical.h"

#include <QtCore/QLoggingCategory>


Q_LOGGING_CATEGORY(plPar, "Specchio.Calendar.Parser", QtInfoMsg);


namespace {
    const int s_maxLineLength = 256;
} // namespace

class ParserPrivate {
    ParserPrivate(Parser *q) : q_ptr(q) {}
    bool parseComponent(icalcomponent *comp);
    void parseEvent(icalcomponent *comp);

    QDateTime convertDate(const icaltimetype &date);
    void addEvent(EventData &event, const icaltimetype &startDate, const icaltimetype &endDate);

    Q_DECLARE_PUBLIC(Parser);
    Parser *q_ptr;
};

Parser::Parser(CalendarData::Ptr parentCalendar)
: mParentCalendar(std::move(parentCalendar))
, d_ptr(new ParserPrivate(this))
{
}

Parser::~Parser()
{
    delete d_ptr;
}

bool Parser::parse(QIODevice &data)
{
    Q_D(Parser);

    mEvents.clear();

    if (!data.isReadable()) {
        qCWarning(plPar) << "Unable to read from incoming calendar data";
        return false;
    }

    icalparser *parser = icalparser_new();

    bool result = false;
    QByteArray line { s_maxLineLength, '\0' };
    while (!data.atEnd()) {
        qint64 bytesRead = data.readLine(line.data(), s_maxLineLength);
        if (bytesRead < 0) {
            qCWarning(plPar) << "Error while reading calendar data";
            break;
        }

        // Where we're going, libical, there is no constness. anywhere.
        icalcomponent *comp = icalparser_add_line(parser, line.data());
        if (comp != nullptr) {
            if (icalcomponent_is_valid(comp) == 1) {
                result = result ||  d->parseComponent(comp);
            }
            icalparser_clean(parser);
            icalcomponent_free(comp);
        }
    }

    icalparser_free(parser);

    return result;
}

bool ParserPrivate::parseComponent(icalcomponent* comp)
{
    icalcomponent_kind kind = icalcomponent_isa(comp);
    if (kind == ICAL_XROOT_COMPONENT || kind == ICAL_VCALENDAR_COMPONENT) {
        bool result = false;
        qCDebug(plPar) << "Found component of type:" << icalcomponent_kind_to_string(kind);

        icalcomponent *iter = icalcomponent_get_first_real_component(comp);
        while (iter != nullptr) {
            qCDebug(plPar) << "Descending into type:" << icalcomponent_kind_to_string(icalcomponent_isa(iter));
            result = parseComponent(iter) || result;
            iter = icalcomponent_get_next_component(comp, ICAL_ANY_COMPONENT);
        }
        return result;
    }

    switch (kind) {
        case ICAL_VEVENT_COMPONENT:
            parseEvent(comp);
            return true;
        case ICAL_VTODO_COMPONENT:
            qCDebug(plPar) << "Found todo";
            qCDebug(plPar) << icalcomponent_as_ical_string(comp);
//             parseEvent(comp);
            return true;
        default:
            qCDebug(plPar) << "Unsupported component type" << icalcomponent_kind_to_string(kind);
            return false;
    }
}

void ParserPrivate::parseEvent(icalcomponent* comp)
{
    bool recurring = false;
    icalrecurrencetype icalRecurType;
    icaltimetype icalDateBegin = icaltime_null_date();
    icaltimetype icalDateEnd = icaltime_null_date();
    icaldurationtype icalDuration = icaldurationtype_bad_duration();

    EventData event;

    icalproperty *prop = icalcomponent_get_first_property(comp, ICAL_ANY_PROPERTY);
    while (prop != nullptr) {
        if (icalproperty_get_value(prop) == nullptr) {
            continue;
        }
        icalproperty_kind kind = icalproperty_isa(prop);
        switch (kind) {
            case ICAL_DURATION_PROPERTY:
                icalDuration = icalproperty_get_duration(prop);
                break;
            case ICAL_DTSTART_PROPERTY:
                icalDateBegin = icalproperty_get_dtstart(prop);
                event.isWholeDay = (icalDateBegin.is_date == 1);
                break;
            case ICAL_DTEND_PROPERTY:
                icalDateEnd = icalproperty_get_dtend(prop);
                event.isWholeDay = (icalDateEnd.is_date == 1);
                break;
            case ICAL_RRULE_PROPERTY:
                recurring = true;
                icalRecurType = icalproperty_get_rrule(prop);
                break;
            case ICAL_SUMMARY_PROPERTY:
                event.title = QString::fromUtf8(icalproperty_get_summary(prop));
                break;
            case ICAL_LOCATION_PROPERTY:
                event.location = QString::fromUtf8(icalproperty_get_location(prop));
                break;

            default:
                break;
        }
        prop = icalcomponent_get_next_property(comp, ICAL_ANY_PROPERTY);
    }

    // Events lasting all day long without further specification
    if (!event.dateEnd.isValid()) {
        int durationSeconds = 0;
        if (icaldurationtype_is_bad_duration(icalDuration) == 0) {
            durationSeconds = icaldurationtype_as_int(icalDuration);
        }
        if (durationSeconds != 0) {
            event.dateEnd = event.dateBegin.addSecs(durationSeconds);
        } else {
            event.dateEnd = event.dateBegin.addDays(1);
        }
    }

    if (recurring) {
        // Find the next occurrences in the event's period
        icaltimetype next;
        icaltimetype now = icaltime_convert_to_zone(icaltime_today(), icaltimezone_get_utc_timezone());
        icaltimetype next_year = icaltime_add(now, icaldurationtype_from_int(31536000 /* One year */));
        icalrecur_iterator *it = icalrecur_iterator_new(icalRecurType, icalDateBegin);
        next = icalrecur_iterator_next(it);
        while (icaltime_is_null_time(next) == 0) {
            icaltimetype nextPeriodEnd = icaltime_add(next, icalDuration);
            if (icaltime_compare(next, now) >= 0 && icaltime_compare(next, next_year) < 0) {
                addEvent(event, next, nextPeriodEnd);
            }
            next = icalrecur_iterator_next(it);
        }
        icalrecur_iterator_free(it);
    } else {
        addEvent(event, icalDateBegin, icalDateEnd);
    }
}

void ParserPrivate::addEvent(EventData &event, const icaltimetype &startDate, const icaltimetype &endDate)
{
    event.dateBegin = convertDate(startDate);
    event.dateEnd = convertDate(endDate);

    if (event.dateEnd < QDateTime::currentDateTimeUtc()) {
        qCDebug(plPar) << "Ignoring old/expired event:" << event.title << "on" << event.dateBegin;
        return;
    }

    Q_Q(Parser);

    event.calendarColor = q->mParentCalendar->color;
    event.owner = q->mParentCalendar;

    q->mEvents.append(event);

    qCDebug(plPar) << "New event:" << event.title << "from" << event.dateBegin << "to" << event.dateEnd;
}

QDateTime ParserPrivate::convertDate(const icaltimetype &date)
{
    const icaltimetype utcDate = icaltime_convert_to_zone(date, icaltimezone_get_utc_timezone());
    QTime time;
    if (utcDate.is_date == 0) {
        time = QTime { utcDate.hour, utcDate.minute, utcDate.second };
    } else {
        time = QTime { 0, 0, 0 };
    }
    return QDateTime { { utcDate.year, utcDate.month, utcDate.day }, time, Qt::UTC };
}
