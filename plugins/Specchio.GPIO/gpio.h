/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#pragma once

#include <QtCore/QObject>
#include <QtCore/QTimer>
#include <QtCore/QVariant>


class Gpio : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool available READ checkPlatform CONSTANT)
    Q_PROPERTY(QVariantMap gpio READ gpioMap WRITE setGpioMap)

public:
    explicit Gpio();
    ~Gpio() override;

public:
    static bool checkPlatform();

    Q_INVOKABLE void start();
    Q_INVOKABLE void stop();

    // Never read
    QVariantMap gpioMap() const { return QVariantMap(); }
    void setGpioMap(const QVariantMap &map);

private:
    void notifyActivity();

    static void pigpioStaticCallback(int handle, unsigned gpio, unsigned level, uint32_t tick, void *rawInstancePtr);

private slots:
    void healthCheck();
    void pigpioCallback(unsigned gpio, unsigned level);

signals:
    void gpioChanged(int gpio, int level, const QString &broadcast);
    void activity(const QList<int> activePins);
    void connected();
    void disconnected();
    void configError();

private:
    struct GpioConfig {
        int gpio;
        QString broadcast;
        char edge;
        int value;
        QString shell;
        QStringList shellArguments;
        QString shellWorkingDirectory;
    };
    int mHandle;
    QMap<int, GpioConfig> mGpioMap;
    QList<int> mCallbacks;

    QTimer mWatchdog;

    static QMap<int, void*> mInstances;
};
