# General Purpose I/O Plugin

This Specchio plugin sends broadcasts whenever a GPIO changes state.

The only prerequisite is to have `pigpiod` installed on the machine.

For the Raspberry Pi, on Raspbian, it's pretty easy:

```bash
sudo apt install pigpiod
sudo systemctl enable pigpiod
sudo systemctl start pigpiod
```

## GPIO Configuration

Any Pi GPIO configurable for reading can be used by this plugin.
The configuration file must contain a "gpio" group. Inside of it create groups named after the GPIO numbers you want to
listen for.

For example:

```
[gpio.2]
...options...
[gpio.23]
...options..
```

Each GPIO group can contain the following settings:

| Setting          | Optional | Default value    | Description                                                                       |
| ---------------- |:--------:|:----------------:| --------------------------------------------------------------------------------- |
| `edge`           | Yes      | `"either"`       | Edge on which the GPIO gets triggered. Can be `rising`, `falling`, `either`.      |
| `broadcast`      | Yes      | `"GPIO_CHANGED"` | Name of the broadcast emitted when the GPIO changes value. See below for details. |
| `shell`          | Yes      | `""`             | Shell command to launch when the GPIO is triggered. Should be an absolute path.   |
| `shellArguments` | Yes      | `[]`             | Shell command arguments, in an array of strings. See below for details.           |
| `shellWorkDir`   | Yes      | `""`             | Working directory from where to launch the shell command.                         |

### `broadcast` Setting

The broadcast emitted upon change of the GPIO value can be renamed with this config setting.

Its contents, however, can not be configured. When a broadcast is emitted, its extras will be as follows:

```
{
    'gpio': <integer, number of the triggered GPIO>,
    'level': <integer, value containing the changed GPIO level>
}
```

### `shellArguments*` Setting

A shell command can be specified to be launched whenever the GPIO changes value.

The shell arguments are specified in an array of strings. A few special values will be replaced within all provided
arguments, to pass level and gpio number to the script:

 * A string `"{gpio}"` will be replaced with the triggered GPIO number.
 * A string `"{levelInt}"` will be replaced with the triggered GPIO value, as a raw integer.
 * A string `"{levelBool}"` will be replaced with the triggered GPIO value, as a boolean, e.g. with `true` or `false`.
 * A string `"{levelFloat}"` will be replaced with the triggered GPIO value, with a 0.0-1.0 interval. This assumes the maximum value is 255.
 * A string `"{levelPerc}"` will be replaced with the triggered GPIO value, with a 0-100 interval. This assumes the maximum GPIO value is 255.

## Quiet time configuration

The plugin can be disabled in a certain part of the day, for example to no react to movement sensors output during the
night.

The setting is controlled by the following options, which can be put at the beginning of the configuration file, outside
of any group:

| Name | Type | Optional/default value | Description |
| --- | --- | --- | --- |
| quietTimeEnabled | `Boolean` | Yes: `false` | Whether to observe a quiet time. |
| quietTimeStart | `Integer` | Yes: `0` | Hour when to begin the quiet period. |
| quietTimeEnd | `Integer` | Yes: `7` | Hour when to end the quiet period. |

## Example

This example configuration file listens on three GPIOs. GPIO 2 has a PIR sensor to detect presence, GPIO 23 has a rotary
encoder to change the brightness, and GPIO 24 has another rotary encoder to change the volume.

Quiet time is here disabled.

```
[gpio.2]
broadcast = "PresenceDetected"
shell = "/opt/my_scripts/toggle_hotword_detection"
shellArguments = [ "--enable={levelBool}" ]
shellWorkDir = "/opt/my_scripts"

[gpio.23]
edge = "rising"
shell = "/usr/bin/xrandr"
shellArguments = [ "--output", "LVDS-0", "--brightness", "{levelFloat}"]

[gpio.24]
edge = "falling"
shell = "/usr/bin/amixer"
shellArguments = [ "cset", "numid=1", "{levelPerc}%" ]
```

In this example, quiet time is on from 11pm to 6am:

```
quietTimeEnabled = true
quietTimeStart = 23
quietTimeEnd = 6
...
```
