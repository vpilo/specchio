/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#include "gpio.h"

#include <QtCore/QDebug>
#include <QtCore/QFile>
#include <QtCore/QLoggingCategory>
#include <QtCore/QMap>
#include <QtCore/QProcess>
#include <QtCore/QtMath>

#include <pigpiod_if2.h>

namespace {
    /// Time between health checks of the pigpiod socket connection.
    const int s_watchdogTimeoutMilliseconds = 15 * 1000;
    /// Number of GPIOs available on the Raspberry Pi.
    const int s_numberRaspberryGpio = 54;
} // namespace

QMap<int, void*> Gpio::mInstances;

Q_LOGGING_CATEGORY(plGpio, "Specchio.GPIO", QtInfoMsg);


void Gpio::pigpioStaticCallback(int handle, unsigned int gpio, unsigned int level, uint32_t tick, void *rawInstancePtr)
{
    Q_UNUSED(tick);

    // Ignore events not meant for this instance
    if (!mInstances.contains(handle) || mInstances[handle] != rawInstancePtr) {
        return;
    }
    // Invoke the callback asynchronously to run it in the right context.
    Gpio *instance = static_cast<Gpio*>(rawInstancePtr);
    QMetaObject::invokeMethod(instance, "pigpioCallback", Qt::QueuedConnection,
                              Q_ARG(unsigned int, gpio),
                              Q_ARG(unsigned int, level));
}

void Gpio::pigpioCallback(unsigned int gpio, unsigned int level)
{
    if (!mGpioMap.contains(gpio)) {
        qCWarning(plGpio) << "GPIO" << gpio << "was not registered!";
        return;
    }

    notifyActivity();

    const GpioConfig &config { mGpioMap[gpio] };
    emit gpioChanged(gpio, level, config.broadcast);

    if (!config.shell.isEmpty()) {
        QStringList args = config.shellArguments;
        auto levelDouble = static_cast<double>(level);
        args.replaceInStrings(QStringLiteral("{gpio}"), QString::number(gpio))
        .replaceInStrings(QStringLiteral("{levelInt}"), QString::number(level))
        .replaceInStrings(QStringLiteral("{levelBool}"), (level != 0) ? QStringLiteral("true") : QStringLiteral("false"))
        .replaceInStrings(QStringLiteral("{levelFloat}"), QString::number(levelDouble / 255.0))
        .replaceInStrings(QStringLiteral("{levelPerc}"), QString::number(qFloor(100 * (levelDouble / 255))));
        qint64 pid;
        bool started = QProcess::startDetached(config.shell, args, config.shellWorkingDirectory, &pid);
        if (started) {
            qCDebug(plGpio) << "Started process" << pid << "on GPIO" << gpio << "level" << level;
        } else {
            qCWarning(plGpio) << "Unable to start process" << config.shell << "for activity on GPIO" << gpio;
        }
    }
}

void Gpio::setGpioMap(const QVariantMap &map)
{
    stop();

    QMapIterator<QString, QVariant> it { map };
    while (it.hasNext()) {
        it.next();
        bool ok = false;
        const auto &gpio { it.key().toInt(&ok) };
        const auto &settings { it.value().toMap() };
        const auto &edgeString { settings["edge"].toString() };
        auto broadcast { settings["broadcast"].toString() };
        auto shell { settings["shell"].toString() };
        auto shellArgs { settings["shellArguments"].toStringList() };
        auto shellWorkDir { settings["shellWorkDir"].toString() };

        char edge;
        if (edgeString.compare(QStringLiteral("rising"), Qt::CaseInsensitive) == 0) {
            edge = RISING_EDGE;
        } else if (edgeString.compare(QStringLiteral("falling"), Qt::CaseInsensitive) == 0) {
            edge = FALLING_EDGE;
        } else {
            edge = EITHER_EDGE;
        }
        if (broadcast.isEmpty()) {
            broadcast = QStringLiteral("GPIO_CHANGED");
        }

        // Ensure the requested shell command will work.
        if (!shell.isEmpty()) {
            const QFile shellCommandFile { shell };
            if (!shellCommandFile.exists() || (shellCommandFile.permissions() & QFileDevice::ExeUser) == 0) {
                qCWarning(plGpio) << "Invalid executable" << shell;
                shell.clear();
                shellArgs.clear();
                shellWorkDir.clear();
            }
        }

        if (!ok || gpio < 0 || gpio >= s_numberRaspberryGpio) {
            qCWarning(plGpio) << "Invalid GPIO specified in settings for broadcast" << broadcast;
        } else {
            qCDebug(plGpio) << "Changes to GPIO" << gpio << "notified via broadcast" << broadcast;
            mGpioMap.insert(gpio, {gpio, broadcast, edge, 0, shell, shellArgs, shellWorkDir});
        }
    }

    if (mGpioMap.isEmpty()) {
        qCWarning(plGpio) << "Invalid configuration";
        emit configError();
    } else {
        start();
    }
}

Gpio::Gpio()
: mHandle(-1)
{
    mWatchdog.setInterval(s_watchdogTimeoutMilliseconds);
    connect(&mWatchdog, &QTimer::timeout, this, &Gpio::healthCheck);
};

Gpio::~Gpio()
{
    stop();
}

void Gpio::start()
{
    if (mHandle >= 0) {
        return;
    }
    if (mGpioMap.isEmpty()) {
        qCWarning(plGpio) << "Invalid configuration";
        emit configError();
        return;
    }
    mWatchdog.start();

    mHandle = pigpio_start(nullptr, nullptr);
    if (mHandle < 0) {
        qCDebug(plGpio) << "Cannot connect to pigpiod";
        return;
    }

    qCDebug(plGpio) << "Connected to pigpiod";
    mInstances.insert(mHandle, this);

    for (const int gpio : mGpioMap.keys()) {
        qCDebug(plGpio) << "Now watching GPIO:" << gpio;
        int callbackId = callback_ex(mHandle, gpio, EITHER_EDGE, &Gpio::pigpioStaticCallback, this);
        mCallbacks.append(callbackId);
    }

    emit connected();
    notifyActivity();
}

void Gpio::stop()
{
    for (const int callback : mCallbacks) {
        callback_cancel(callback);
    }
    mCallbacks.clear();

    mWatchdog.stop();

    if (mHandle < 0) {
        return;
    }
    pigpio_stop(mHandle);
    mHandle = -1;
    emit disconnected();
}

void Gpio::notifyActivity()
{
    QList<int> activePins;
    for (const int gpio : mGpioMap.keys()) {
        int value = gpio_read(mHandle, gpio);
        mGpioMap[gpio].value = value;
        if (value != 0) {
            activePins.append(gpio);
        }
    }
    emit activity(activePins);
}

void Gpio::healthCheck()
{
    if (mHandle < 0) {
        start();
        return;
    }

    if (get_mode(mHandle, mGpioMap.first().gpio) != PI_INPUT) {
        qCDebug(plGpio) << "Health check failed!";
        stop();
        start();
    }
}

bool Gpio::checkPlatform() {
    int handle = pigpio_start(nullptr, nullptr);
    if (handle < 0) {
        qCInfo(plGpio) << "Unable to connect to pigpio. Assuming server is available";
        return true;
    }

    int hwRevision = get_hardware_revision(handle);
    if (hwRevision == 0) {
        qCWarning(plGpio) << "pigpio is unable to find an HW revision!";
        return false;
    }

    pigpio_stop(handle);
    return true;
}
