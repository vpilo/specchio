/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

import QtQuick 2.8
import QtQuick.Layouts 1.1

import Specchio 1.0
import Specchio.GPIO 1.0

Item {
    id: gpio

    state: 'unavailable'
    states: [
        State {
            name: 'ready'
            PropertyChanges { target: main; opacity: 1 }
            PropertyChanges { target: unavailable; opacity: 0 }
        },
        State {
            name: 'unavailable'
            PropertyChanges { target: main; opacity: 0 }
            PropertyChanges { target: unavailable; opacity: 1 }
            PropertyChanges { target: unavailable; text: qsTr('GPIO: unavailable') }
        },
        State {
            name: 'config_error'
            PropertyChanges { target: main; opacity: 0 }
            PropertyChanges { target: unavailable; opacity: 1 }
            PropertyChanges { target: unavailable; text: qsTr('GPIO: Invalid configuration') }
        }
    ]

    AdaptableText {
        id: unavailable
        anchors.fill: parent

        color: 'white'
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.weight: Font.Thin
        maximumSize: Units.fontSize.medium
    }

    AdaptableText {
        id: main
        anchors.fill: parent
        visible: Specchio.debugMode

        color: 'white'
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.weight: Font.Thin
        maximumSize: Units.fontSize.small
    }

    Gpio {
        id: controller

        gpio: settings.gpio

        onGpioChanged: {
            if (settings.quietTimeEnabled) {
                var hours = new Date().getHours()
                if (settings.quietTimeStart < settings.quietTimeEnd) {
                    // Cases as 00-07, 08-23
                    if (hours >= settings.quietTimeStart && hours < settings.quietTimeEnd) {
                        console.debug(tag, "Quiet time...")
                        return
                    }
                } else {
                    // Cases as 22-06
                    if (hours >= settings.quietTimeStart || hours < settings.quietTimeEnd) {
                        console.debug(tag, "Quiet time...")
                        return
                    }
                }
            }

            Plugin.sendBroadcast(broadcast, { 'gpio': gpio, 'level': level })
        }

        onActivity: {
            var pinsStr
            if (activePins.length == 0) {
                pinsStr = qsTr("None")
            } else {
                pinsStr = activePins.join(', ')
            }
            main.text = qsTr("Active pins: %1").arg(pinsStr)
        }
        onConnected: {
            gpio.state = 'ready'
        }
        onDisconnected: {
            gpio.state = 'unavailable'
        }
        onConfigError: {
            gpio.state = 'config_error'
        }
    }

    Connections {
        target: Specchio
        onStarting: {
            if (settings.quietTimeEnabled && settings.quietTimeStart == settings.quietTimeEnd) {
                console.warning(tag, "Quiet time start and end time cannot be the same! Ignoring.")
                settings.quietTimeEnabled = false
            }
            if (controller.available) {
                controller.start()
            }
        }
        onStopping: {
            if (controller.available) {
                controller.stop()
            }
        }
    }
}
