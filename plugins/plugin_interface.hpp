/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#pragma once

#include <QtCore/QtPlugin>


#define SPECCHIO_PLUGIN_INTERFACE_NAME  "Specchio.PluginInterface/1.0"
#define SPECCHIO_PLUGIN_INTERFACE_STRING  QLatin1Literal(SPECCHIO_PLUGIN_INTERFACE_NAME)

class SpecchioPlugin
{
public:
    virtual ~SpecchioPlugin() = default;

    virtual bool init() { return true; };
    virtual QString error() { return QString(); };

    virtual const QString &mainObject() = 0;
    virtual const QString &version() = 0;
};

Q_DECLARE_INTERFACE(SpecchioPlugin, SPECCHIO_PLUGIN_INTERFACE_NAME)
