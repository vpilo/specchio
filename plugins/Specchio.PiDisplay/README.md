# Specchio plugin: PiDisplay

Control your Raspberry Pi's display output automatically.

This plugin listens for broadcasts, and when it receives the specified one, it will use it to turn your display on or
off.

## Settings

The configuration file contains the following settings:

| Name | Type | Optional/default value | Description |
| --- | --- | --- | --- |
| broadcasts | `Array of Tables` | No | Broadcasts to listen to. See below. |
| timeoutSeconds | `Integer` | Yes: `60` | After receiving an "on" broadcast", if no other broadcasts are received, the display will be turned off after this many seconds. If 0, the display will only be turned on or off in direct response to a broadcast. |
| commands.method | `String` | Yes: `"TvService"` | There are at least two ways to turn the screen off. This string chooses which method is used. See below for more information. |
| command.**name**.shell | `String` | No | Shell command to launch. |
| command.**name**.shellArguments | `Array` of `String` | No | List of arguments for the launched command. Can be empty, e.g. `shellArguments = []`. |

### Broadcasts setting

The `broadcasts` setting is an array of tables, which must be defined as follows. The example defines two entries:

```toml
[[broadcasts]]
name = "a name"
[[broadcasts]]
name = "another name"
```

These are the values that can specified in each `[[broadcasts]]` entry:

| Name | Type | Optional/default value | Description |
| --- | --- | --- | --- |
| name | `String` | No | Name of the broadcast to look for. |
| extra | `String` | Yes: `""` | When the `name` broadcast is received, if this is set, then the broadcast will trigger the plugin. |
| extraValue | `Any` | Yes: `""` | If this is set, then the broadcast's `extra` will be tested whether it has this value for triggering the plugin or not. |
| turnsOn | `Boolean` | Yes: `true` | When true (the default), receiving this broadcast will turn the screen on immediately. When false, it will turn the screen off immediately. |

### Examples

To turn on and off the display when receiving input from a certain GPIO:

```toml
[[broadcasts]]
name = "MY_GPIO"
extra = "level"
extraValue = "1"

[[broadcasts]]
name = "MY_GPIO"
extra = "level"
extraValue = "0"
turnsOn = false
```

To turn off the display when receiving a broadcast, without caring for the broadcast's contents:

```toml
[[broadcasts]]
name = "DISPLAY_OFF_GPIO"
turnsOn = false
```

## Predefined Display Control Methods

The default control method is `VCGen`. On the Raspberry Pi, this command will not be able to turn off some displays.
If that is your case, try `TvService` instead. Both `TvService` and `VCGen` are preconfigured, and you only need to select
the method you want (`VCGen` is the default, and doesn't need manual set up):

```toml
[commands]
method="TvService"
```

## Control The Display Your Own Way

You can also create your own display control method. There need to be two groups under your new method, one called `on`
and another called `off`. The `shell` string and the `shellArguments` array of strings control which command is launched
and how. Note that you can execute raw shell scripts by using `bash -c`, like in the example below:

```toml
[commands]
method="CEC"

[commands.CEC.on]
shell = "/bin/bash"
shellArguments = [ "-c", "'echo 'on 0' | /usr/bin/cec-client -s -d 1'" ]

[commands.CEC.off]
shell = "/bin/bash"
shellArguments = [ "-c", "'echo 'standby 0' | /usr/bin/cec-client -s -d 1'" ]
```
