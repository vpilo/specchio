/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

import QtQuick 2.6

import Specchio 1.0
import Specchio.PiDisplay 1.0

Item {
    id: display

    anchors.fill: parent

    AdaptableText {
        id: label
        anchors.fill: parent

        visible: Specchio.debugMode && (timeoutTime != 0)

        color: "gray"
        padding: Units.spacing.small
        style: Text.Raised
        styleColor: "black"
        maximumSize: Units.fontSize.medium
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter

        Timer {
            id: labelUpdateTimer
            running: Specchio.debugMode && timer.running
            repeat: true
            triggeredOnStart: true
            interval: Units.duration.second
            onTriggered: label.text = qsTr("Display off %1").arg(timeLeft())
        }
    }

    Connections {
        target: Specchio
        onStarting: {
            // Settings sanity check
            for (var idx in settings.broadcasts) {
                var entry = settings.broadcasts[idx]
                if (entry.name == undefined) {
                    console.warn(tag, "Invalid broadcasts configuration!")
                    return
                }
            }
            if (settings.commands[settings.commands.method] == undefined) {
                console.warn(tag, "Command '" + settings.commands.method + "' not found!")
            } else if(settings.commands[settings.commands.method].on.shell == undefined ||
                settings.commands[settings.commands.method].off.shell == undefined) {
                console.warn(tag, "Command '" + settings.commands.method + "' not valid!")
            } else {
                turnOn()
            }
        }
        onStopping: {
            // Ensure the screen is on after quitting
            turnOn()
            timer.stop()
        }
    }
    Connections {
        target: Plugin
        onBroadcast: {
            for (var idx in settings.broadcasts) {
                var entry = settings.broadcasts[idx]
                if (entry.name != name) {
                    continue
                }

                // If an extra and its value are specified, use them both to trigger the condition.
                // If only an extra is specified, if it's present then that's enough to trigger the condition.
                // If neither are, just the broadcast is enough to trigger the condition.
                var entryIsMatching = false
                if (entry.hasOwnProperty('extra')) {
                    if (entry.hasOwnProperty('extraValue')) {
                        entryIsMatching = extras[ entry.extra ] == entry.extraValue
                    } else {
                        entryIsMatching = true
                    }
                } else {
                    entryIsMatching = true
                }
                if (!entryIsMatching) {
                    continue
                }

                var shouldTurnOn = entry.hasOwnProperty('turnsOn') ? entry.turnsOn : true
                if (shouldTurnOn) {
                    turnOn()
                } else {
                    turnOff()
                }
                break
            }
        }
    }

    Launcher {
        id: launcher
    }

    property var timeoutTime: 0

    Timer {
        id: timer
        running: false
        repeat: false
        triggeredOnStart: false
        interval: settings.timeoutSeconds * Units.duration.second
        onTriggered: turnOff()
    }

    function turnOn() {
        startTimeout()
        var configOn = settings.commands[settings.commands.method].on
        if (configOn == undefined || configOn.shell == undefined) {
            console.warn(tag, "Turn on command is not valid!")
            return
        }
        launcher.launch(configOn.shell, configOn.shellArguments)
        label.text = qsTr("Display on")
    }
    function turnOff() {
        timer.stop()
        var configOff = settings.commands[settings.commands.method].off
        if (configOff == undefined || configOff.shell == undefined) {
            console.warn(tag, "Turn off command is not valid!")
            return
        }
        launcher.launch(configOff.shell, configOff.shellArguments)
        label.text = qsTr("Display off")
    }

    function startTimeout() {
        if (settings.timeoutSeconds > 0) {
            timeoutTime = Date.now() + timer.interval
            timer.restart()
        }
    }

    function timeLeft() {
        var remainingSeconds = (timeoutTime - Date.now()) / Units.duration.second
        if (remainingSeconds <= 0) {
            return qsTr('now')
        } else if (remainingSeconds < 60) {
            return qsTr('in %Ln second(s)', 'time to future moment', Math.floor(remainingSeconds))
        } else if (remainingSeconds < 3600) {
            return qsTr('in %Ln minute(s)', 'time to future moment', Math.floor(Number(remainingSeconds) / 60))
        }
        return qsTr('in %Ln hour(s)', 'time to future moment', Math.floor(Number(remainingSeconds) / 3600))
    }
}
