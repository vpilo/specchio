
#include "launcher.h"

#include <QtCore/QLoggingCategory>
#include <QtCore/QProcess>


Q_LOGGING_CATEGORY(plPd, "Specchio.PiDisplay", QtInfoMsg);

void Launcher::launch(const QString &command, const QStringList &arguments)
{
    qint64 pid;
    bool started = QProcess::startDetached(command, arguments, QString(), &pid);
    if (started) {
        qCDebug(plPd) << "Started" << command << "with PID" << pid;
    } else {
        qCWarning(plPd) << "Unable to start process" << command << "with arguments" << arguments;
    }
}
