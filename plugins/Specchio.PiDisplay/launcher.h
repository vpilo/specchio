#pragma once

#include <QtCore/QObject>
#include <QtCore/QStringList>


class Launcher : public QObject {
    Q_OBJECT

public:
    Q_INVOKABLE void launch(const QString &command, const QStringList &arguments);
};
