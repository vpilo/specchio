/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

import QtQuick 2.6
import QtQuick.Layouts 1.1

import Specchio 1.0
import Specchio.Weather 1.0

Item {
    id: weather

    state: 'loading'
    states: [
        State {
            name: 'ready'
            PropertyChanges { target: main; opacity: 1 }
            PropertyChanges { target: loading; opacity: 0 }
        },
        State {
            name: 'loading'
            PropertyChanges { target: main; opacity: 0 }
            PropertyChanges { target: loading; opacity: 1 }
            PropertyChanges { target: loading; text: qsTr('Loading weather...') }
        },
        State {
            name: 'error'
            PropertyChanges { target: main; opacity: 0 }
            PropertyChanges { target: loading; opacity: 1 }
        }
    ]

    AdaptableText {
        id: loading
        anchors.fill: parent

        color: 'white'
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.weight: Font.Thin
        maximumSize: Units.fontSize.medium
    }

    // Main info
    Flow {
        id: main
        anchors.fill: parent
        spacing: Units.spacing.large
        layoutDirection: Plugin.positionedAtLeft ? Qt.LeftToRight : Qt.RightToLeft

        Item {
            width: childrenRect.width
            height: childrenRect.height

            AdaptableText {
                anchors { top: parent.top ; left: parent.left }
                id: city
                color: 'lightgray'
                maximumSize: Units.fontSize.smallMedium
            }
            Image {
                anchors { top: city.baseline ; left: parent.left }
                id: weatherIcon
                sourceSize.width: Units.iconSize.huge
                sourceSize.height: Units.iconSize.huge
                mipmap: true
            }
            AdaptableText {
                anchors { verticalCenter: weatherIcon.verticalCenter ; left: weatherIcon.right }
                id: temp
                color: 'white'
                maximumSize: Units.fontSize.huge
                font.weight: Font.Bold
            }
            Image {
                anchors { bottom: temp.bottom ; left: temp.right }
                id: tempUnits
                sourceSize.width: Units.iconSize.medium
                sourceSize.height: Units.iconSize.medium
                mipmap: true
            }
        }

        Item {
            width: childrenRect.width
            height: parent.height

            // Wind
            Row {
                id: windRow
                anchors { top: parent.top ; left: parent.left }
                width: childrenRect.width
                height: childrenRect.height
                spacing: Units.spacing.small

                Image {
                    id: windIcon
                    sourceSize.width: Units.iconSize.medium
                    sourceSize.height: Units.iconSize.medium
                    source: 'qrc:///wi-strong-wind'
                    mipmap: true
                }
                Text {
                    id: windSpeed
                    anchors.verticalCenter: windIcon.verticalCenter
                    color: 'lightgray'
                    font.pointSize: Units.fontSize.smallMedium
                    verticalAlignment: Text.AlignVCenter
                }
                Image {
                    id: windDirIcon
                    mipmap: true
                    anchors.verticalCenter: windIcon.verticalCenter
                    sourceSize.width: Units.iconSize.smallMedium
                    sourceSize.height: Units.iconSize.smallMedium
                    source: 'qrc:///wi-direction-up'

                    transform: Rotation {
                        id: windDirIconRotation
                        origin.x: windDirIcon.sourceSize.width / 2
                        origin.y: windDirIcon.sourceSize.height / 2
                        angle: 0
                    }
                }
            }

            // Min/max temp
            Row {
                anchors { top: windRow.bottom ; left: parent.left }
                width: childrenRect.width
                height: childrenRect.height
                spacing: Units.spacing.small

                Image {
                    id: tempIcon
                    sourceSize.width: Units.iconSize.medium
                    sourceSize.height: Units.iconSize.medium
                    source: 'qrc:///wi-thermometer'
                    mipmap: true
                }
                Text {
                    id: tempMin
                    anchors.verticalCenter: tempIcon.verticalCenter
                    color: 'skyblue'
                    font.pointSize: Units.fontSize.smallMedium
                    verticalAlignment: Text.AlignVCenter
                }
                Text {
                    text: '/'
                    anchors.verticalCenter: tempIcon.verticalCenter
                    color: 'lightgray'
                    font.pointSize: Units.fontSize.smallMedium
                    verticalAlignment: Text.AlignVCenter
                }
                Text {
                    id: tempMax
                    anchors.verticalCenter: tempIcon.verticalCenter
                    color: 'salmon'
                    font.pointSize: Units.fontSize.smallMedium
                    verticalAlignment: Text.AlignVCenter
                }
            }
        }
    }

    Provider {
        id: provider

        cityId: settings.cityId
        apiKey: settings.openWeatherMapApiKey
        unitsType: settings.unitsType

        onWeatherChanged: {
            city.text = info.city
            temp.text = info.temp
            tempUnits.source = 'qrc:///' + (info.units == 'k' ? 'wi-fahrenheit' : 'wi-celsius')
            windDirIconRotation.angle = info.windDirection
            windSpeed.text = info.wind
            tempMin.text = info.tempMin
            tempMax.text = info.tempMax

            if (info.weatherIcon) {
                weatherIcon.source = 'qrc:///' + info.weatherIcon
            } else {
                weatherIcon.source = ''
            }

            weather.state = 'ready'
        }

        onError: {
            weather.state = 'error'
            loading.text = message
            if (isFatal) {
                updateTimer.stop()
            }
        }
    }

    property SmartTimer updateTimer: Plugin.networkTimer("update", 900 /* 15 min */, true)
    Connections {
        target: updateTimer
        onTriggered: {
            weather.state = 'loading'
            provider.update()
        }
    }
}
