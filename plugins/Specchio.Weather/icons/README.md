# Weather Icons

## Source

These icons come from project: http://erikflowers.github.io/weather-icons/
at commit 58cb6a2 (branch master)

## Credits
The icon designs are originally by [Lukas Bischoff](http://www.twitter.com/artill). Icon art for v1.1 forward, HTML, Less, and CSS are by [Erik](http://www.helloerik.com).

## Licensing

* Weather Icons licensed under [SIL OFL 1.1](http://scripts.sil.org/OFL)
