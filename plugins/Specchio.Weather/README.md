# Specchio plugin: Weather

A weather station.

This plugin uses the [OpenWeatherMap APIs](https://openweathermap.org/). To use them, you need [to register](https://openweathermap.org/appid).

Example:

![Plugin screenshot](weather-screenshot-1.png)

## Settings

The configuration file contains the following settings:

| Name | Type | Optional/default value | Description |
| --- | --- | --- | --- |
| openWeatherMapApiKey | `String` | No | The API key from OpenWeatherMap. See below for more information. |
| unitsType | `String` | `"c"` | Displayed units of temperature. Either `"c"` for Celsius, or `"f"` for Fahrenheit. |
| cityId | `Integer` | No | The ID of the city to get weather about. See below for more information. |

### API Key

[Registration](https://openweathermap.org/appid) is required to obtain an API key. It looks like this: `b1b15e88fa797225412429c1c50c122a1`.
In the configuration file, it will look like this:

```toml
openWeatherMapApiKey="b1b15e88fa797225412429c1c50c122a1"
```

### Finding Your City

The `cityId` field must be a number. You can quickly discover which id corresponds to your city [by going here](https://openweathermap.org/find?q=)
and entering the name of your city in the search field. When you find it, click on it: the URL of the results is the
value you need to place in the `cityId` field.

For example, for Amsterdam, Netherlands, the results page will bring you to this link:

`https://openweathermap.org/city/2759794`

The configuration will then be:

```toml
cityId=2759794
```
