/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#pragma once

#include <QtCore/QVariant>


struct WeatherInfo {
    Q_GADGET
public:

    bool mValid = false;
    Q_PROPERTY(bool valid MEMBER mValid)

    int mTimestamp = 0;
    Q_PROPERTY(int timestamp MEMBER mTimestamp)

    QString mCity;
    Q_PROPERTY(QString city MEMBER mCity)
    QChar mUnits;
    Q_PROPERTY(QChar units MEMBER mUnits)

    QString mWeather;
    Q_PROPERTY(QString weather MEMBER mWeather)
    QString mWeatherDescription;
    Q_PROPERTY(QString weatherDesc MEMBER mWeatherDescription)
    QString mWeatherIcon;
    Q_PROPERTY(QString weatherIcon MEMBER mWeatherIcon)

    QString mTemp;
    Q_PROPERTY(QString temp MEMBER mTemp)
    QString mTempMax;
    Q_PROPERTY(QString tempMax MEMBER mTempMax)
    QString mTempMin;
    Q_PROPERTY(QString tempMin MEMBER mTempMin)

    QString mWind;
    Q_PROPERTY(QString wind MEMBER mWind)
    int mWindDirection = 0;
    Q_PROPERTY(int windDirection MEMBER mWindDirection)

    int mTimeSunrise = 0;
    Q_PROPERTY(int timeSunrise MEMBER mTimeSunrise)
    int mTimeSunset = 0;
    Q_PROPERTY(int timeSunset MEMBER mTimeSunset)
};

Q_DECLARE_METATYPE(WeatherInfo);
