/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#pragma once

#include "weatherinfo.h"

class QNetworkAccessManager;
class QNetworkReply;


class Provider : public QObject {
    Q_OBJECT

    Q_PROPERTY(int cityId READ city WRITE setCityId);
    Q_PROPERTY(QChar unitsType READ unitsType WRITE setUnitsType);
    Q_PROPERTY(QString apiKey READ apiKey WRITE setApiKey);
    Q_PROPERTY(WeatherInfo weather READ weather NOTIFY weatherChanged);

public:
    Provider(QObject *parent = nullptr);

    int city() const { return mCityId; }
    QChar unitsType() const { return mUnitsType; }
    const QString &apiKey() const { return mApiKey; }
    const WeatherInfo weather() const { return mWeatherNow; }
    void setCityId(int cityId) { mCityId = cityId; }
    void setApiKey(QString apiKey) { mApiKey = std::move(apiKey); }
    void setUnitsType(QChar unitsType) { mUnitsType = unitsType.toLower(); }

public Q_SLOTS:
    Q_INVOKABLE bool update();

private:
    QString round(const QVariant &value);

private Q_SLOTS:
    void parseReply(QNetworkReply *reply);

private:
    QNetworkAccessManager *mNetMan;

    QString mApiKey;
    int mCityId = -1;
    QChar mUnitsType;

    WeatherInfo mWeatherNow;

Q_SIGNALS:
    void configChanged();
    void weatherChanged(WeatherInfo info);

    void error(const QString &message, bool isFatal);
};
