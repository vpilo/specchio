set(Weather_SOURCES
    plugin.cpp
    provider.cpp
    weatherinfo.h
)

set(Weather_FILES
    qmldir
    default.toml
    Weather.qml
)

qt5_add_resources(RCC_SOURCES weather.qrc)

add_library(weather SHARED ${Weather_SOURCES} ${RCC_SOURCES})
target_link_libraries(weather
    Qt5::Quick
)

set(PLUGIN_INSTALL_DIR ${SPECCHIO_PLUGIN_DIR}/Specchio/Weather)
install(TARGETS weather DESTINATION ${PLUGIN_INSTALL_DIR})
install(FILES ${Weather_FILES} DESTINATION ${PLUGIN_INSTALL_DIR})
