/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#include "provider.h"
#include "weatherinfo.h"

#include <QtCore/QJsonArray>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>
#include <QtCore/QLoggingCategory>
#include <QtCore/QUrlQuery>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkConfigurationManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkSession>

#include <cmath>


Q_LOGGING_CATEGORY(plWtr, "Specchio.Weather", QtInfoMsg);

namespace {
    const QString s_unknownWeatherIcon { QStringLiteral( "wi-na") };
    const QMap<QString, QString> s_iconMap {
        { "01d", "wi-day-sunny" },
        { "02d", "wi-day-cloudy" },
        { "03d", "wi-cloudy" },
        { "04d", "wi-cloudy-windy" },
        { "09d", "wi-showers" },
        { "10d", "wi-rain" },
        { "11d", "wi-thunderstorm" },
        { "13d", "wi-snow" },
        { "50d", "wi-fog" },
        { "01n", "wi-night-clear" },
        { "02n", "wi-night-cloudy" },
        { "03n", "wi-night-cloudy" },
        { "04n", "wi-night-cloudy" },
        { "09n", "wi-night-showers" },
        { "10n", "wi-night-rain" },
        { "11n", "wi-night-thunderstorm" },
        { "13n", "wi-night-snow" },
        { "50n", "wi-night-alt-cloudy-windy" }
    };
} // namespace

Provider::Provider(QObject *parent)
: QObject(parent)
{
    mNetMan = new QNetworkAccessManager(this);
    connect(mNetMan, &QNetworkAccessManager::finished,
            this, &Provider::parseReply);
}

bool Provider::update()
{
    if (mCityId < 0) {
        emit error(tr("Weather: Configure your city"), true);
        return false;
    }
    if (mApiKey.isEmpty()) {
        emit error(tr("Weather: Configure your API key"), true);
        return false;
    }

    qCDebug(plWtr) << "Updating weather";

    QUrl url(QStringLiteral("https://api.openweathermap.org/data/2.5/weather"));
    QUrlQuery query;
    query.addQueryItem(QStringLiteral("id"), QString().setNum(mCityId));
    query.addQueryItem(QStringLiteral("mode"), QStringLiteral("json"));
    query.addQueryItem(QStringLiteral("appid"), mApiKey);

    switch (mUnitsType.cell()) {
        case 'k':
            // No parameter given gets you kelvins
            break;
        case 'f':
            query.addQueryItem(QStringLiteral("units"), QStringLiteral("imperial"));
            break;
        case 'c':
        default:
            mUnitsType = 'c';
            query.addQueryItem(QStringLiteral("units"), QStringLiteral("metric"));
            break;
    }

    url.setQuery(query);

    QNetworkReply *reply = mNetMan->get(QNetworkRequest(url));
    return reply->isRunning();
}

void Provider::parseReply(QNetworkReply* reply)
{
    if (reply == nullptr) {
        return;
    }
    if (reply->error() != QNetworkReply::NoError) {
        qCInfo(plWtr) << "Received error" << reply->error();
        reply->deleteLater();
        return;
    }

    QJsonDocument document = QJsonDocument::fromJson(reply->readAll());
    reply->deleteLater();

    if (!document.isObject()) {
        qCWarning(plWtr) << "Invalid response";
        return;
    }

    const QVariantMap data { document.object().toVariantMap() };

    int code = data.value(QStringLiteral("cod")).toInt();
    if (code != 200) {
        QString message = data.value(QStringLiteral("message")).toString();
        qCWarning(plWtr) << "Error response" << code << "- error:" << message;
        if (code == 401 || message.contains(QStringLiteral("Invalid API key"))) {
            emit error(tr("Weather: Invalid API key"), true);
            mApiKey.clear();
        } else {
            emit error(message, false);
        }
        return;
    }

    qCDebug(plWtr) << "Weather data received";

    WeatherInfo now;
    now.mTimestamp = QDateTime::currentMSecsSinceEpoch();
    now.mCity = data.value(QStringLiteral("name")).toString();
    now.mUnits = mUnitsType;

    if (data.contains(QStringLiteral("weather"))) {
        const QVariantList weatherObjects { data.value(QStringLiteral("weather")).toList() };
        if (weatherObjects.count() > 0) {
            now.mValid = true;
            const QVariantMap weather { weatherObjects.first().toMap() };

            now.mWeather = weather.value(QStringLiteral("main")).toString();
            now.mWeatherDescription = weather.value(QStringLiteral("description")).toString();
            now.mWeatherIcon = s_iconMap.value(weather.value(QStringLiteral("icon")).toString(), s_unknownWeatherIcon);
        } else {
            qCDebug(plWtr) << "No weather objects found";
        }
    } else {
        qCDebug(plWtr) << "No weather data received";
        return;
    }

    if (data.contains(QStringLiteral("main"))) {
        now.mValid = true;
        const QVariantMap main { data.value(QStringLiteral("main")).toMap() };

        now.mTemp = round(main.value(QStringLiteral("temp")));
        now.mTempMin = round(main.value(QStringLiteral("temp_min")));
        now.mTempMax = round(main.value(QStringLiteral("temp_max")));
    }
    if (data.contains(QStringLiteral("wind"))) {
        const QVariantMap wind { data.value(QStringLiteral("wind")).toMap() };

        now.mWind = round(wind.value(QStringLiteral("speed")));
        now.mWindDirection = wind.value(QStringLiteral("deg")).toInt();
    }
    if (data.contains(QStringLiteral("sys"))) {
        const QVariantMap sys { data.value(QStringLiteral("sys")).toMap() };

        now.mTimeSunrise = sys.value(QStringLiteral("sunrise")).toInt();
        now.mTimeSunset = sys.value(QStringLiteral("sunset")).toInt();
    }

    if (!now.mValid) {
        qCWarning(plWtr) << "Invalid info received";
        return;
    }

    emit weatherChanged(now);
    mWeatherNow = now;
}

QString Provider::round(const QVariant &value)
{
    // TODO: Find out why, on armhf, this direct qt float conversion to string works,
    // but not any others like QString::number(value.toFloat(), 'f', 1) or just handling floats
    // all the way to Qt
    return value.toString().section('.', 0, 0);
}

