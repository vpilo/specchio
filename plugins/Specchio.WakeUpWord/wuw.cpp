/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#include "wuw.h"

#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtCore/QFileInfo>
#include <QtCore/QLoggingCategory>
#include <QtCore/QStringList>

#include "listener.h"


namespace {
    const QString s_userModelDirectory = QStringLiteral("%1/.specchio/Specchio.WakeUpWord").arg(QDir::homePath());
    const int s_defaultModelSensitivity = 5;
    const int s_snowboySampleRate = 16000;
    const int s_snowboyBitsPerSample = 16;
    const int s_snowboyChannelCount = 1;
} // namespace

Q_LOGGING_CATEGORY(plWuW, "Specchio.WuW", QtInfoMsg);


WakeUpWordModule::~WakeUpWordModule()
{
    stop();
}

void WakeUpWordModule::setConfiguration(const QVariantMap &config)
{
    stop();

    if (!config.contains("device") ||
        !config.contains("models")) {
        qCWarning(plWuW) << "Missing configuration keys!";
        emit errorInvalidConfiguration();
        return;
    }

    mDeviceName = config.value("device", QStringLiteral("default")).toString();
    mCreateDumpFile = config.value("dumpToFile", false).toBool();
    mAudioGain = config.value("audioGain", 1).toUInt();
    mUsePreprocessing = config.value("preprocessing", true).toBool();

    mFormat.setSampleRate(s_snowboySampleRate);
    mFormat.setSampleSize(s_snowboyBitsPerSample);
    mFormat.setChannelCount(s_snowboyChannelCount);
    mFormat.setCodec("audio/pcm");
    mFormat.setByteOrder(QAudioFormat::LittleEndian);
    mFormat.setSampleType(QAudioFormat::SignedInt);

    QMapIterator<QString, QVariant> it { config["models"].toMap() };
    while (it.hasNext()) {
        it.next();
        const auto &name { it.key() };
        const auto &model { it.value().toMap() };

        if (name.isEmpty() || !model.contains("path")) {
            qCWarning(plWuW) << "Ignoring invalid model" << model;
            continue;
        }
        if (model.value("enabled", true) == false) {
            continue;
        }

        // Can't check the paths straight away due to the possibility we didn't get the plugin path from QML yet.
        const auto &path { model["path"].toString() };

        unsigned int sensitivity { model["sensitivity"].toUInt() };
        if (sensitivity < 1 || sensitivity > 9) {
            qCWarning(plWuW) << "Invalid sensitivity for model" << model << "- using default," << s_defaultModelSensitivity;
            sensitivity = s_defaultModelSensitivity;
        }
        mModels.append({ name, path, sensitivity });
    }

    if (mModels.empty()) {
        qCWarning(plWuW) << "No models configured!";
        emit errorInvalidConfiguration();
        return;
    }
}

void WakeUpWordModule::start()
{
    if (!mAvailable) {
        qCWarning(plWuW) << "Start called when unavailable!";
        return;
    }
    if (mListener != nullptr) {
        qCWarning(plWuW) << "Already started!";
        return;
    }

    mListener = new Listener(mFormat, mDeviceInfo, mRunTimePath, mModels, mUsePreprocessing, mAudioGain);
    connect(mListener, &Listener::errorInvalidConfiguration, this, &WakeUpWordModule::errorInvalidConfiguration);
    connect(mListener, &Listener::errorCannotOpenDevice, this, &WakeUpWordModule::errorCannotOpenDevice);
    connect(mListener, &Listener::wordDetected, this, &WakeUpWordModule::wordDetected);

    if (mCreateDumpFile) {
        connect(mListener, &Listener::data, this, &WakeUpWordModule::appendToRecording);
        mDumpFile.setFileName(s_userModelDirectory + "/dump.raw");
        if (!mDumpFile.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
            qCWarning(plWuW) << "Unable to open dump file!";
        } else {
            qCInfo(plWuW) << "Dumping recording to:" << mDumpFile.fileName();
        }
    }

    mListener->start();
}

void WakeUpWordModule::stop()
{
    mDumpFile.close();
    mAvailable = false;
    mModels.clear();

    if (mListener != nullptr) {
        mListener->stop();
        delete mListener;
        mListener = nullptr;
    }
}

void WakeUpWordModule::appendToRecording(const QByteArray &buffer)
{
    mDumpFile.write(buffer);
}

void WakeUpWordModule::verifyConfiguration()
{
    mAvailable = false;
    QThread *validationThread = QThread::create([&]() {
        if (mDeviceName.isEmpty()) {
            mDeviceInfo = QAudioDeviceInfo::defaultInputDevice();
        } else {
            const auto list { QAudioDeviceInfo::availableDevices(QAudio::AudioInput) };
            for (const auto &dev : list) {
                if (dev.deviceName() == mDeviceName) {
                    mDeviceInfo = dev;
                    break;
                }
            }
            if (mDeviceInfo.isNull()) {
                qCCritical(plWuW) << "Unable to find specified audio input device:" << mDeviceName;
                qCCritical(plWuW) << "List of INPUT devices available on this system:";
                for (const auto &dev : list) {
                    qCCritical(plWuW) << " - Device" << dev.deviceName() << " - with preferred format" << dev.preferredFormat();
                }
                mAvailable = false;
                emit errorInvalidConfiguration();
                return;
            }
        }

        if (!mDeviceInfo.isFormatSupported(mFormat)) {
            qCWarning(plWuW) << "Unsupported format: trying to find a similar one";
            mFormat = mDeviceInfo.nearestFormat(mFormat);
            if (!mDeviceInfo.isFormatSupported(mFormat)) {
                qCCritical(plWuW) << "Unable to find a supported format! Use another device.";
                mAvailable = false;
                emit errorInvalidConfiguration();
                return;
            }
        }

        qCDebug(plWuW) << "Device" << mDeviceInfo.deviceName() << "- with format" << mFormat;

        QMutableListIterator<Model> it { mModels };
        while (it.hasNext()) {
            Model &model { it.next() };
            QFileInfo info { model.path };
            if (info.exists()) {
                model.path = info.absoluteFilePath();
                continue;
            }

            info = { mRunTimePath + "/models/" + model.path };
            if (info.exists()) {
                model.path = info.absoluteFilePath();
                continue;
            }

            info = { s_userModelDirectory + "/" + model.path };
            if (info.exists()) {
                model.path = info.absoluteFilePath();
                continue;
            }

            qCWarning(plWuW) << "File for model" << model.path << "not found!";
            it.remove();
        }

        if (mModels.empty()) {
            qCCritical(plWuW) << "- No valid models found!";
            mAvailable = false;
            emit errorInvalidConfiguration();
            return;
        }

        for (const auto &model : mModels) {
            qCDebug(plWuW) << "Model" << model.name << "with sensitivity" << model.sensitivity << "@" << model.path;
        }

        mAvailable = true;
        emit available();
    });
    validationThread->start();
}
