/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#include "plugin_interface.hpp"

#include <QtQml/QQmlEngine>
#include <QtQml/QQmlExtensionPlugin>

#include "wuw.h"


#define PLUGIN_MAIN_OBJECT WakeUpWord
#define PLUGIN_VERSION_MAJOR 1
#define PLUGIN_VERSION_MINOR 0

#define _TO_STRING_DONTUSE_(s) #s
#define TO_STRING(s) _TO_STRING_DONTUSE_(s)

namespace {
    const QString s_mainObject { QStringLiteral(TO_STRING(PLUGIN_MAIN_OBJECT)) };
    const QString s_version { QStringLiteral("%1.%2").arg(PLUGIN_VERSION_MAJOR).arg(PLUGIN_VERSION_MINOR) };
} // namespace


class WakeUpWordPlugin : public QQmlExtensionPlugin, public SpecchioPlugin
{
    Q_OBJECT
    Q_INTERFACES(SpecchioPlugin)
    Q_PLUGIN_METADATA(IID SPECCHIO_PLUGIN_INTERFACE_NAME)

public:
    void registerTypes(const char *uri) override {
        qmlRegisterType<WakeUpWordModule>(uri, PLUGIN_VERSION_MAJOR, PLUGIN_VERSION_MINOR, TO_STRING(WakeUpWordModule));
    }

    const QString &mainObject() override {
        return s_mainObject;
    }

    const QString &version() override {
        return s_version;
    }
};

#include "plugin.moc"
