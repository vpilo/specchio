/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#pragma once

#include <QtCore/QFile>
#include <QtCore/QObject>
#include <QtCore/QVariant>
#include <QtMultimedia/QAudioDeviceInfo>
#include <QtMultimedia/QAudioFormat>

#include "model.h"


class Listener;

class WakeUpWordModule : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString path READ path WRITE setPath);
    Q_PROPERTY(QVariantMap configuration READ configuration WRITE setConfiguration);

public:
    ~WakeUpWordModule() override;

    Q_INVOKABLE void start();
    Q_INVOKABLE void stop();
    Q_INVOKABLE void verifyConfiguration();

    void setConfiguration(const QVariantMap &config);
    void setPath(const QString &path) { mRunTimePath = path; }

    // Unused
    QVariantMap configuration() const { return {}; };
    QString path() const { return mRunTimePath; };


signals:
    void errorInvalidConfiguration();
    void errorCannotOpenDevice();
    void available();

    void wordDetected(const QString &word);

private slots:
    void appendToRecording(const QByteArray &buffer);

private:
    bool mAvailable = false;
    Listener *mListener = nullptr;
    QString mRunTimePath;

    bool mUsePreprocessing;
    unsigned int mAudioGain;
    bool mCreateDumpFile;
    QFile mDumpFile;

    QString mDeviceName;
    QAudioFormat mFormat;
    QAudioDeviceInfo mDeviceInfo;
    ModelList mModels;
};
