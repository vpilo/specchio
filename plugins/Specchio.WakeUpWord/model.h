/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#pragma once

#include <QtCore/QList>
#include <QtCore/QString>


struct Model {
    QString name;
    QString path;
    unsigned int sensitivity;
};

using ModelList = QList<Model>;
