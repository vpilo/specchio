/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

import QtQuick 2.8
import QtQuick.Layouts 1.1

import Specchio 1.0
import Specchio.WakeUpWord 1.0

Item {
    id: wakeupword

    state: 'unavailable'
    states: [
    State {
        name: 'listening'
        PropertyChanges { target: label; visible: Specchio.debugMode }
        PropertyChanges { target: label; text: qsTr('Listening') }
    },
    State {
        name: 'unavailable'
        PropertyChanges { target: label; visible: true }
        PropertyChanges { target: label; text: qsTr('WuW not ready') }
    },
    State {
        name: 'config_error'
        PropertyChanges { target: label; visible: true }
        PropertyChanges { target: label; text: qsTr('Invalid WuW settings') }
    }
    ]

    AdaptableText {
        id: label
        anchors.fill: parent
        color: 'white'
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.weight: Font.Thin
        maximumSize: Units.fontSize.medium
    }

    Connections {
        target: Specchio
        onStarting: {
            wuw.verifyConfiguration()
        }
        onStopping: {
            wuw.stop()
        }
    }

    Connections {
        target: Plugin
        onBroadcast: {
            if (wakeupword.state != 'listening') {
                return
            }
            switch (name) {
                case 'WAKEUPWORD_SUSPEND': wuw.stop(); break
                case 'WAKEUPWORD_RESUME': wuw.start(); break
                default: break
            }
        }
    }

    WakeUpWordModule {
        id: wuw
        path: Plugin.pluginPath
        configuration: settings

        onErrorInvalidConfiguration: {
            wakeupword.state = 'config_error'
        }
        onAvailable: {
            wakeupword.state = 'listening'
            wuw.start()
        }
        onWordDetected: {
            Plugin.sendBroadcast(settings.broadcast, { word: word })
        }
    }
}
