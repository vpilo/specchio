# DEPRECATED

Snowboy is already abandoned and its model generation website will be turned off at the end of 2020, making this plugin not usable anymore.
It could still be adapted to use another hotword recognition library.

# Specchio plugin: Wake Up Word

Listens to your voice for the configured commands, and outputs broadcasts in response.

This plugin uses [Snowboy](https://snowboy.kitt.ai/) and your microphone to constantly listen, in the background, for a
specified set of so-called _hotwords_. Upon hearing one, it will emit a broadcast to act on it.

## Settings

The configuration file contains the following settings:

| Name | Type | Optional/default value | Description |
| --- | --- | --- | --- |
| device | `String` | Yes: `"none"` | Hardware device to record from. When kept at the default, Specchio will print out all available recording devices to the console, to help in selecting one. |
| audioGain | `Integer` | Yes: `2` | Extra volume to add to the listened audio. Helps with weak microphones. Allowed range is 0-10. |
| preprocessing | `Boolean` | Yes: `true` | Whether to enable pre-processing and noise canceling. Said to be better disabled when using personalized models. |
| dumpToFile | `Boolean` | Yes: `false` | Helps with finding out why your words aren't heard. If this option is on, a .raw file in your `~/.specchio/Specchio.WakeUpWord/` directory will be created, containing the full listening session. *Be careful of available space when this is on* . The file is raw audio data: import it in e.g. [Audacity](https://www.audacityteam.org/) with the following settings. Sample rate: `16000`; channels: `1`; encoding: `Signed 16-bit PCM`; byte order: `Little-endian`. |
| models | `Table` | Yes: `"snowboy" and "smart_mirror"` | Snowboy voice models to recognize. See below for more information. |

### Models

The download package of Snowboy comes with a few models only. Personalized models can be made by first [registering on the Snowboy website](https://snowboy.kitt.ai/dashboard) and then by either recording your own voice to train a new model, or by adding your
training to an existing model and then downloading that one.

Once you have a model, you can place it in the directory `~/.specchio/Specchio.WakeUpWord`, and then configure it. Create
a configuration group `[models.<your model name>]` and put the following options in:

| Name | Type | Optional/default value | Description |
| --- | --- | --- | --- |
| enabled | `Boolean` | Yes: `true` | Whether to use this model or not. |
| path | `String` | No | Just the filename, or the relative path, where your model is stored. |
| sensitivity | `Integer` | Yes: `5` | A number between 1 and 9 to identify how sensitive voice recognition will be. A lower number means less probability of false positives, but also less successful recognitions. |

For example:

```toml
[models.my_precious_smart_mirror]
enabled = true
path = "my_smart_mirror_model.pmdl"
sensitivity = 3
```

## Broadcast

When a model has been recognized, the plugin will emit a broadcast called `WAKEUPWORD_DETECTED`, with extras
`{ word: <your model name> }`. The string matches the name you gave to the recognized model; in the example above, you
would get `word: "my_precious_smart_mirror"`.

## Control

The plugin can be paused and resumed by broadcast. Send a broadcast `WAKEUPWORD_SUSPEND` without extras to stop recognition,
and free up the microphone to allow another software to record audio. When it's done, send another broadcast called
`WAKEUPWORD_RESUME`, also without extras.
