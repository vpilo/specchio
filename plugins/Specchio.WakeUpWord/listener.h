/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#pragma once

#include <QtCore/QThread>
#include <QtMultimedia/QAudioDeviceInfo>
#include <QtMultimedia/QAudioFormat>

#include "model.h"


namespace snowboy {
    class SnowboyDetect;
}

class QIODevice;
class QAudioInput;

class Listener : public QThread
{
    Q_OBJECT

    enum DetectionType {
        SILENCE_DETECTED = -2,
        NOTHING_DETECTED = 0,
        ERROR = -1,
    };

public:
    Listener(const QAudioFormat &format, const QAudioDeviceInfo &device, const QString &pluginPath, ModelList models, bool enablePreprocessing, unsigned int audioGain);
    ~Listener() override;

    void start();
    void stop();

private:
    void run() override;

private slots:
    void audioStateChanged(QAudio::State state);
    void read();

signals:
    void errorInvalidConfiguration();
    void errorCannotOpenDevice();

    void wordDetected(const QString &word);
    void data(const QByteArray &buffer);

private:
    QAudioFormat mFormat;
    QAudioDeviceInfo mDeviceInfo;
    QString mCommonResourcesPath;

    ModelList mModels;

    int mLastDetection = INT_MAX;
    QIODevice *mDevice = nullptr;
    QAudioInput *mRecorder = nullptr;
    snowboy::SnowboyDetect *mDetector = nullptr;

    int mMinimumBufferLength;
    QByteArray mBuffer;
};
