/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

#include "listener.h"

#include <QtCore/QDebug>
#include <QtCore/QLoggingCategory>
#include <QtMultimedia/QAudioDeviceInfo>
#include <QtMultimedia/QAudioInput>

#include <snowboy-detect.h>


namespace {
    // Maximum time to allow the thread to close by itself, before killing
    const int s_threadJoinMaxWaitMsec = 1000;
} // namespace

Q_LOGGING_CATEGORY(plLs, "Specchio.WuW.Listener", QtInfoMsg);


Listener::Listener(const QAudioFormat &format, const QAudioDeviceInfo &device, const QString &pluginPath, ModelList models, bool enablePreprocessing, unsigned int audioGain)
: mFormat(format)
, mDeviceInfo(device)
, mModels(std::move(models))
{
    mCommonResourcesPath = pluginPath + "/common.res";

    QString paths;
    QString sensitivities;
    for (const auto &model : mModels) {
        if (!paths.isEmpty()) {
            paths.append(',');
            sensitivities.append(',');
        }
        paths.append(model.path);
        sensitivities.append(QStringLiteral("0.%1").arg(model.sensitivity));
    }

    mDetector = new snowboy::SnowboyDetect(mCommonResourcesPath.toStdString(), paths.toStdString());
    mDetector->SetSensitivity(sensitivities.toStdString());
    mDetector->ApplyFrontend(enablePreprocessing);
    mDetector->SetAudioGain(audioGain);

    if (mDetector->SampleRate() != mFormat.sampleRate() ||
        mDetector->NumChannels() != mFormat.channelCount() ||
        mDetector->BitsPerSample() != mFormat.sampleSize()) {
        qCCritical(plLs) << "Snowboy config mismatch!";
        qCCritical(plLs) << "Sample rate is" << mDetector->SampleRate() << "vs required" << mFormat.sampleRate();
        qCCritical(plLs) << "Sample size is" << mDetector->BitsPerSample() << "vs required" << mFormat.sampleSize();
        qCCritical(plLs) << "Channel count is" << mDetector->NumChannels() << "vs required" << mFormat.channelCount();
        delete mDetector;
        mDetector = nullptr;
    } else {
        qCDebug(plLs) << "Snowboy config: sample rate" << mDetector->SampleRate() << "Hz -" << mDetector->NumChannels() << "channels -" << mDetector->BitsPerSample() << "bit - gain" << audioGain << "- preprocessing" << enablePreprocessing;
    }
}

Listener::~Listener()
{
    delete mDetector;
}

void Listener::run()
{
    if (mDetector == nullptr) {
        emit errorInvalidConfiguration();
        return;
    }

    mRecorder = new QAudioInput(mDeviceInfo, mFormat);
    connect(mRecorder, &QAudioInput::stateChanged, this, &Listener::audioStateChanged);

    int bufferSize = mFormat.sampleRate() * mFormat.bytesPerFrame();
    mMinimumBufferLength = bufferSize / 10;
    mRecorder->setBufferSize(bufferSize);
    mBuffer.reserve(bufferSize);

    mDevice = mRecorder->start();

    if (mDevice == nullptr || mRecorder->error() != QAudio::NoError) {
        qCWarning(plLs) << "Unable to start listening:" << mRecorder->error();
        emit errorCannotOpenDevice();
    } else {
        connect(mDevice, &QIODevice::readyRead, this, &Listener::read, Qt::DirectConnection);

        qCInfo(plLs) << "Started listening";

        exec();

        mRecorder->stop();
        if (mRecorder->error() != QAudio::NoError) {
            qCWarning(plLs) << "On exit, recording error:" << mRecorder->error();
        }
        qCDebug(plLs) << "Stopped listening";
    }

    delete mRecorder;
    mDevice = nullptr;
    mRecorder = nullptr;
}

void Listener::read()
{
    mBuffer.append(mDevice->readAll());
    if (mBuffer.size() < mMinimumBufferLength) {
        return;
    }

    int result = mDetector->RunDetection(mBuffer.toStdString(), false);
    if (result > NOTHING_DETECTED) {
        int hotwordIndex = result - 1;
        if (hotwordIndex < 0 || hotwordIndex >= mModels.count()) {
            qCWarning(plLs) << "Detected: hotword with unrecognized index" << hotwordIndex;
        } else {
            const QString &hotword { mModels[hotwordIndex].name };
            qCDebug(plLs) << "Detected: hotword" << hotword;
            emit wordDetected(hotword);
        }
    } else if (mLastDetection != result) {
        mLastDetection = result;
        if (result == SILENCE_DETECTED) {
            qCDebug(plLs) << "Detected: silence";
        } else if (result == NOTHING_DETECTED) {
            qCDebug(plLs) << "Detected: not an hotword";
        } else if (result == ERROR) {
            qCWarning(plLs) << "Snowboy processing error";
        }
    }

    emit data(mBuffer);
    mBuffer.clear();
}

void Listener::audioStateChanged(QAudio::State state)
{
    if (mRecorder->error() != QAudio::NoError) {
        qCDebug(plLs) << "Recording error while in state" << state << ":" << mRecorder->error();
    }
}

void Listener::start()
{
    if (isRunning()) {
        qCWarning(plLs) << "Already started!";
        return;
    }
    QThread::start(QThread::HighPriority);
}

void Listener::stop()
{
    if (!isRunning()) {
        qCWarning(plLs) << "Already stopped!";
        return;
    }
    exit(0);
    wait(s_threadJoinMaxWaitMsec);
}
