# Specchio plugin: Compliments

Shows a random compliment on screen, changing every few seconds.

Example:

![Plugin screenshot](compliments-screenshot-1.png)

Another:

![Plugin screenshot](compliments-screenshot-2.png)

## Settings

The configuration file contains the following settings:

| Name | Type | Optional/default value | Description |
| --- | --- | --- | --- |
| changeIntervalSeconds | `Integer` | Yes: `60` | Time (in seconds) after which to show another compliment. |
| compliments | `Array` | No | The list of compliments to give. |

The compliments table should contain at least an element, of type `Array` and name `generic`, containinng an array with
all the strings to display:

```
[compliments]
    "generic" = [ "Hello", "Hi", "How's it going?" ]
```

If three extra elements called `morning`, `afternoon` and `evening` are present, compliments will also be randomly
selected from the list of the current time of day. For example:

```
[compliments]
    "generic" = [ "Hello", "Hi", "How's it going?" ]
    "morning" = [
        "Good morning",
        "Enjoy your day!",
    ]
    "afternoon" = [
        "Looking great today!",
        "Nice day!",
    ]
    "evening" = [
        "You look nice in the sunset!",
    ]
```
