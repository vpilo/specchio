/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

import QtQuick 2.6

import Specchio 1.0

AdaptableText {
    id: compliments
    anchors.fill: parent
    horizontalAlignment: Text.AlignHCenter
    verticalAlignment: Text.AlignVCenter
    padding: Units.spacing.small

    color: "white"
    style: Text.Raised
    styleColor: "black"

    Behavior on text {
        SequentialAnimation {
            NumberAnimation { target: compliments; property: "opacity"; duration: Units.duration.lenghty ; to: 0 }
            PropertyAction {}
            NumberAnimation { target: compliments; property: "opacity"; duration: Units.duration.lenghty ; to: 1 }
        }
    }

    function changeCompliment() {
        if (settings.compliments.generic.length == 0 &&
            settings.compliments.morning.length == 0 &&
            settings.compliments.afternoon.length == 0 &&
            settings.compliments.evening.length == 0) {
            compliments.text = qsTr('Hello')
            return
        }
        if (settings.compliments.morning.length == 0 ||
            settings.compliments.afternoon.length == 0 ||
            settings.compliments.evening.length == 0) {
            compliments.text = settings.compliments.generic[ Math.floor(Math.random() * settings.compliments.generic.length) ]
            return
        }

        var list = settings.compliments.generic

        if (settings.compliments.generic.indexOf(compliments.text) != -1) {
            var hour = new Date().getHours()
            if (hour >= 3 && hour < 13) {
                list = settings.compliments.morning
            } else if (hour < 19) {
                list = settings.compliments.afternoon
            } else {
                list = settings.compliments.evening
            }

            compliments.text = list[ Math.floor(Math.random() * list.length) ]
        } else {
            compliments.text = list[ Math.floor(Math.random() * list.length) ]
        }
    }

    Timer {
        running: true
        repeat: true
        triggeredOnStart: true
        interval: settings.changeIntervalSeconds * 1000
        onTriggered: compliments.changeCompliment()
    }
}
