/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

import QtQuick 2.6
import QtQuick.Layouts 1.1

import Specchio 1.0

Column {
    id: root

    property string time
    property string seconds

    property var locale: Qt.locale(Specchio.language)

    Timer {
        interval: settings.showSeconds ? 500 : 5000
        running: true
        repeat: true
        triggeredOnStart: true
        onTriggered: updateClock()
    }

    AdaptableText {
        id: clockWeek
        visible: settings.showDate
        anchors { left: parent.left; right: parent.right }
        height: contentHeight

        color: "lightgray"
        font.weight: Font.Thin
        minimumSize: Units.fontSize.tiny
        maximumSize: Units.fontSize.smallMedium
        horizontalAlignment: settings.alignLeft ? Text.AlignLeft : Text.AlignRight
    }

    AdaptableText {
        id: clockDate
        visible: settings.showDate
        anchors { left: parent.left; right: parent.right }
        height: contentHeight

        color: "lightgray"
        font.weight: Font.Thin
        minimumSize: Units.fontSize.small
        maximumSize: Units.fontSize.large
        horizontalAlignment: settings.alignLeft ? Text.AlignLeft : Text.AlignRight
    }

    AdaptableText {
        id: clockTimeLeftAligned
        visible: settings.alignLeft
        anchors { left: parent.left; right: parent.right }
        rightPadding: clockSecondsLeft.contentWidth

        text: root.time
        color: "white"
        font.weight: Font.Black
        maximumSize: Units.fontSize.huge
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignBottom

        AdaptableText {
            id: clockSecondsLeft
            visible: settings.showSeconds
            height: parent.height
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            x: parent.contentWidth

            leftPadding: Units.spacing.small

            text: root.seconds
            color: "white"
            font.weight: Font.Thin
            maximumSize: Units.fontSize.smallMedium
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignBottom
        }
    }

    AdaptableText {
        id: clockTimeRightAligned
        visible: !settings.alignLeft
        anchors { left: parent.left; right: parent.right }
        rightPadding: clockSecondsRight.contentWidth

        text: root.time
        color: "white"
        font.weight: Font.Black
        maximumSize: Units.fontSize.huge
        horizontalAlignment: Text.AlignRight
        verticalAlignment: Text.AlignBottom

        AdaptableText {
            id: clockSecondsRight
            visible: settings.showSeconds
            height: parent.height
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.right: parent.right

            text: root.seconds
            color: "white"
            font.weight: Font.Thin
            maximumSize: Units.fontSize.smallMedium
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignBottom
        }
    }

    function updateClock() {
        const date = new Date()
        var hour, min, sec

        if (settings.useUtcTime) {
            hour = date.getUTCHours()
            min = date.getUTCMinutes()
            sec = date.getUTCSeconds()
        } else {
            hour = date.getHours()
            min = date.getMinutes()
            sec = date.getSeconds()
        }

        if (hour < 10) hour = '0' + hour
        if (min < 10) min = '0' + min
        if (sec < 10) sec = '0' + sec

        root.time = hour + ':' + min
        if (settings.showSeconds) {
            root.seconds = sec
        }
        if (settings.showDate) {
            clockWeek.text = locale.standaloneDayName(date.getDay())
            clockDate.text = date.toLocaleDateString(locale, qsTr("MMMM d", "Localized date format, see https://doc.qt.io/qt-5/qml-qtqml-date.html#format-strings"))
        }
    }
}
