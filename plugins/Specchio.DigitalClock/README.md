# Specchio plugin: Digital Clock

A digital clock.

Example:

![Plugin screenshot](digitalclock-screenshot-1.png)

## Settings

The configuration file contains the following settings:

| Name | Type | Optional/default value | Description |
| --- | --- | --- | --- |
| showSeconds | `Boolean` | Yes: `false` | Whether to show the seconds. |
| showDate | `Boolean` | Yes: `true` | Whether to show the current date. |
| useUtcTime | `Boolean` | Yes: `false` | Whether to display UTC time instead of the local timezone. |
| alignLeft | `Boolean` | Yes: `true` | If false, aligns the text to the right side of the region.|
