/*
 * Copyright 2020 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

import QtQuick 2.8
import QtQuick.XmlListModel 2.0

RssModelBase {
    type: 'Normal'

    query: '/rss/channel/item'

    XmlRole { name: 'title'; query: 'title/string()' }
    XmlRole { name: 'link'; query: 'link/string()' }
    XmlRole { name: 'pubDate'; query: "pubDate/string()"; isKey: true }
}
