/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

import QtQuick 2.8
import QtQuick.Layouts 1.1
import QtQuick.XmlListModel 2.0

Item {
    id: fetcher

    property var feeds: []
    property int maxAgeDays: 0

    signal statusChanged(string status)

    function setup() {
        if (maxAgeDays < 1 || maxAgeDays > 365) {
            console.debug(tag, 'Invalid setting: maxAgeDays must be an integer between 1 and 365')
            statusChanged('fatal')
        }

        var componentNormal = Qt.createComponent('RssModelNormal.qml')
        var componentReddit = Qt.createComponent('RssModelReddit.qml')

        var maxAgeDate = new Date()
        maxAgeDate.setDate(maxAgeDate.getDate() - maxAgeDays)
        console.debug(tag, "Oldest date allowed: " + maxAgeDate.toISOString())
        var maxAgeTimestamp = maxAgeDate.getTime()

        data.models = []
        for (var feedName in feeds) {
            var feed = feeds[feedName]

            if (feed.enabled === false) {
                continue
            }

            var component = undefined
            if (feed.url.includes('.reddit.com/')) {
                component = componentReddit
            } else {
                component = componentNormal
            }
            var model = component.createObject(fetcher, {
                sourceUri: feed.url,
                userTitle: feed.title,
                oldestTimestamp: maxAgeTimestamp
                });

            model.modelChanged.connect(onModelChanged)

            data.models.push(model)
            console.debug(tag, 'Added ' + model.type + ' feed: ' + feed.title)
        }

        if (data.models.length == 0) {
            statusChanged('error')
        } else {
            statusChanged('loading')
        }
    }

    function fetch() {
        for (var index = 0 ; index < data.models.length ; ++index) {
            data.models[index].fetch()
        }
    }

    function onModelChanged(model, status) {
        // At least one model is ready, start using it
        if (status == 'ready') {
            statusChanged(status)
            return
        }

        // Determine the state of the fetcher by analysing the state of all models

        var anyReady = false
        var anyLoading = false
        var anyErrors = false

        for (var index = 0 ; index < data.models.length ; ++index) {
            switch (data.models[index].lastStatus) {
                case 'ready': anyReady = true; break
                case 'loading': anyLoading = true; break
                default: anyErrors = true; break
            }
        }

        if (anyReady) {
            statusChanged('ready')
        } else if (anyLoading) {
            statusChanged('loading')
        } else if (anyErrors) {
            statusChanged('error')
        }
    }

    function getNewsItem() {
        var attempts = data.models.length
        while(attempts-- > 0) {
            var item = data.models[current].getNewsItem()
            current++

            if (current >= data.models.length) {
                current = 0
            }

            // This model is in error or still loading, ignore it
            if (item == undefined) {
                continue
            }

            return item
        }

        statusChanged('error')
        return undefined
    }

    QtObject {
        id: data

        property int current: 0
        property var models: []
    }
}
