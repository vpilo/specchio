/*
 * Copyright 2020 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

import QtQuick 2.8
import QtQuick.XmlListModel 2.0

RssModelBase {
    type: 'Reddit'

    query: '/feed/entry'
    namespaceDeclarations: "declare default element namespace 'http://www.w3.org/2005/Atom';"

    XmlRole { name: 'title'; query: 'title/string()' }
    XmlRole { name: 'link'; query: 'link/string()' }
    XmlRole { name: 'pubDate'; query: 'updated/string()'; isKey: true }
}
