/*
 * Copyright 2020 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

import QtQuick 2.8
import QtQuick.XmlListModel 2.0

XmlListModel {
    id: root

    property string type: "Base"

    property string userTitle
    property var sourceUri
    property double oldestTimestamp

    property string lastStatus: 'loading'
    signal modelChanged(var instance, var status)

    property int currentItem: 0

    function getNewsItem() {
        if (count == 0) {
            return undefined
        }

        let initialItem = currentItem

        do {
            let item = this.get(currentItem++)
            if (currentItem >= this.count) {
                currentItem = 0
            }

            if (item.hasOwnProperty('pubDate') && item.hasOwnProperty('title')) {
                let pubDate = Date.parse(item.pubDate)
                if (pubDate != NaN && pubDate >= oldestTimestamp) {
                    return {
                        'title': item.title.replace(/<[^>]+>/g, '').trim(),
                        'link': item.link,
                        'date': pubDate,
                        'source': userTitle
                    }
                }
            }
        } while (initialItem != currentItem)
        return undefined
    }

    function fetch() {
        if (source != sourceUri) {
            // Modifying the source triggers fetching it
            source = sourceUri
        } else {
            reload()
        }
    }

    onStatusChanged: {
        switch (status) {
            case XmlListModel.Null:
            case XmlListModel.Loading:
                lastStatus = 'loading'
                break
            case XmlListModel.Ready:
                currentItem = 0
                if (count == 0) {
                    console.warn(tag, type + " feed '" + userTitle + "': cannot fetch news")
                    lastStatus = 'error'
                } else {
                    console.log(tag, type + " feed '" + userTitle + "': fetched " + count + ' news')
                    lastStatus = 'ready'
                }
                break
            case XmlListModel.Error:
                console.warn(tag, type + " feed '" + userTitle + "' got error: " + errorString())
                lastStatus = 'error'
                break
        }
        modelChanged(root, lastStatus)
    }
}
