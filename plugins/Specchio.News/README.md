# Specchio plugin: News

A fading carousel of the latest news.

Example:

![Plugin screenshot](news-screenshot-1.png)

## Settings

The configuration file contains the following settings:

| Name | Type | Optional/default value | Description |
| --- | --- | --- | --- |
| newsChangeIntervalSeconds | `Integer` | Yes: `60` | The next news item will be displayed after this many seconds. |
| refreshIntervalMinutes | `Integer` | Yes: `15` | Interval in minutes between attempts to refresh the news. |
| showSourceTitle | `Boolean` | Yes: `true` | If enabled, you will be able to read from which source the current news item comes from. |
| maxAgeDays | `Integer` | Yes: `7` | Maximum time for which a news item will be shown. Using 1 will only show news published in the last 24 hours. |
| showItemDate | `Boolean` | Yes: `true` | When enabled, the news item's publication date will be shown. |
| feeds | `Table` | No | The sources of news. |

Every news feed has the following options:

| Name | Type | Optional/default value | Description |
| --- | --- | --- | --- |
| title | `String` | No | Name of the news source. |
| url | `String` | No | Where to fetch the feed from. Accepted URL schemas are `http://` and `https://`. |
| enabled | `Boolean` | Yes: `true` | If false, the news feed will not be fetched nor displayed. |

## Example Configurations

Here is an example configuration which disables one of the three default news feeds, all of which are enabled by default.

```toml
title="Almost Default"

[feeds.yahoo]
enabled=false
```

This example has only one news feed:

```toml
showSourceTitle=false
[feeds.yahoo]
enabled=false
[feeds.cnn]
enabled=false
[feeds.bbc]
enabled=false
[feeds.mine]
name="Reddit"
url="https://www.reddit.com/r/news.rss"
```
