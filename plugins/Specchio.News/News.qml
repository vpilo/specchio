/*
 * Copyright 2019 Valerio Pilo <vpilo@coldshock.net>
 * MIT licensed
 */

import QtQuick 2.6
import QtQuick.Layouts 1.1

import Specchio 1.0

import Specchio.News 1.0

Item {
    id: news

    property int current: 0
    property var items: []

    state: 'not_ready'
    states: [
        State {
            name: 'ready'
        },
        State {
            name: 'not_ready'
        }
    ]


    transitions: [
        Transition {
            from: 'ready'
            to: 'not_ready'
            ParallelAnimation {
                NumberAnimation { target: main; property: 'opacity'; duration: Units.duration.brief ; to: 0 }
                NumberAnimation { target: not_ready; property: 'opacity'; duration: Units.duration.brief ; to: .75 }
            }
        },
        Transition {
            from: 'not_ready'
            to: 'ready'
            ParallelAnimation {
                NumberAnimation { target: main; property: 'opacity'; duration: Units.duration.brief ; to: 1 }
                NumberAnimation { target: not_ready; property: 'opacity'; duration: Units.duration.brief ; to: 0 }
            }
        }
    ]

    AdaptableText {
        id: not_ready
        anchors.fill: parent

        text: qsTr('Loading news...')

        color: 'lightgray'
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.weight: Font.Thin
        maximumSize: Units.fontSize.medium
    }

    Item {
        id: main
        anchors.fill: parent
        opacity: 0

        AdaptableText {
            id: newsInfo
            anchors { top: parent.top ; left: parent.left ; right: parent.right }
            width: parent.width

            color: 'dimgray'
            maximumSize: Units.fontSize.small
            font.weight: Font.Thin
            font.italic: true

            visible: settings.showSourceTitle
            horizontalAlignment: Text.AlignHCenter

            Behavior on text {
                SequentialAnimation {
                    NumberAnimation { target: newsInfo; property: 'opacity'; duration: Units.duration.lenghty ; to: 0 }
                    PropertyAction {}
                    NumberAnimation { target: newsInfo; property: 'opacity'; duration: Units.duration.lenghty ; to: 1 }
                }
            }
        }

        AdaptableText {
            id: newsTitle
            anchors { top: newsInfo.bottom ; left: parent.left ; right: parent.right ; bottom: parent.bottom }
            anchors.margins: Units.spacing.large
            width: parent.width

            color: 'lightgray'
            maximumSize: Units.fontSize.medium
            textFormat: Text.StyledText
            maximumLineCount: 2
            elide: Text.ElideRight
            horizontalAlignment: Text.AlignHCenter
            font.family: Specchio.font.family + ', Noto Emoji'

            Behavior on text {
                SequentialAnimation {
                    NumberAnimation { target: newsTitle; property: 'opacity'; duration: Units.duration.lenghty ; to: 0 }
                    PropertyAction {}
                    NumberAnimation { target: newsTitle; property: 'opacity'; duration: Units.duration.lenghty ; to: 1 }
                }
            }
        }
    }

    Fetcher {
        id: fetcher

        feeds: settings.feeds
        maxAgeDays: (settings['maxAgeDays'] != undefined) ? settings.maxAgeDays : 7

        onStatusChanged: {
            switch (status) {
                case 'ready':
                    news.state = 'ready'
                    if (!fetchTimer.running) {
                        Plugin.sendBroadcast('NEWS_UPDATED')
                        fetchTimer.start()
                    }
                    itemChangeTimer.start()
                    break
                case 'loading':
                    news.state = 'not_ready'
                    not_ready.text = qsTr('Loading news...')
                    fetchTimer.stop()
                    itemChangeTimer.stop()
                    break
                case 'fatal':
                    news.state = 'not_ready'
                    not_ready.text = qsTr('Invalid settings')
                    fetchTimer.stop()
                    itemChangeTimer.stop()
                    retryTimer.stop()
                    break
                default:
                    news.state = 'not_ready'
                    not_ready.text = qsTr('Cannot get any news at the moment')
                    fetchTimer.stop()
                    itemChangeTimer.stop()
                    retryTimer.start()
                    break
            }
        }
    }

    Connections {
        target: Specchio
        onStarting: {
            fetcher.setup()
        }
        onStopping: {
            itemChangeTimer.stop()
        }
        onConnectedChanged: {
            if (!isOnline) {
                retryTimer.stop()
            }
        }
    }

    property SmartTimer fetchTimer: Plugin.networkTimer("fetch", settings.refreshIntervalMinutes * 60, true)
    Connections {
        target: fetchTimer
        onTriggered: fetcher.fetch()
    }

    Timer {
        id: itemChangeTimer
        interval: settings.newsChangeIntervalSeconds * Units.duration.second
        running: false
        repeat: true
        triggeredOnStart: true
        onTriggered: {
            var item = fetcher.getNewsItem()
            if (item == undefined) {
                return
            }
            newsTitle.text = item.title
            newsInfo.text = qsTr('%1 - %2')
                .arg(item.source)
                .arg(Specchio.timeDistance(Number(item.date)))
        }
    }

    Timer {
        id: retryTimer
        interval: 5 * Units.duration.minute
        running: false
        repeat: false
        triggeredOnStart: false
        onTriggered: fetcher.fetch()
    }
}
