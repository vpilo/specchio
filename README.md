# Specchio

A performant and extensible Smart Mirror.

Specchio shows up on your screen, displaying your agenda, weather, news when you need it.
It is meant to be used as a Smart Mirror: shown on a screen placed behind a one-way mirror where you can see yourself
and, at the same time, check out the information shown by the mirror.

Specchio runs on a terminal, without desktop. It will start its own [Wayland compositor](https://wayland.freedesktop.org/).
It's best installed on a [Raspbian Lite](https://www.raspberrypi.org/downloads/raspbian/) on your Raspberry Pi.
currently tested only on a Raspberry Pi 3 B+, should work on any.

This project is still a work in progress, and there is no 'one-liner' installer: you must [install program and dependency packages](./doc/INSTALL.md) yourself.

# Features

* Builds for x86_64 and ARMv7 (Raspberry Pi) architectures.
* Works from text-mode, from an existing Wayland compositor, and from X11 (mainly for development and troubleshooting).
* Debug mode to help placing plugins and troubleshoot configuration issues.
* Easy configuration files (written in [TOML](https://github.com/toml-lang/toml)).
* Plugins: news, weather, digital clock, compliments, calendar, gpio, display control.
* Emoji support.
* Translations, currently available: English (US) and Italian.

## Running Specchio

Take a look at [the Installation page](./doc/INSTALL.md) to install Specchio on your Raspberry.

# Configuration

Please see [the Configuration page](./doc/CONFIGURATION.md) to learn how to customize your mirror.

# Plugin development

Please see [the Plugins page](./doc/PLUGINS.md) to understand how to make new plugins.

# Specchio development

To compile Specchio on Raspbian (and Ubuntu), you need to install the following packages:

```bash
sudo apt install \
    qtdeclarative5-dev \
    extra-cmake-modules \
    qml-module-qtwayland-compositor \
    libical-dev \
    cmake \
    ninja
```

There are two Docker images to help development. They already contain all the dependencies needed to build
Specchio, so the list of packages installed in the Docker images can also get you set up locally.
They reside in [the `docker/` directory](./docker/) along with easy scripts to launch builds and tests.

For debugging plugins, qmlplugindump can find errors:

```bash
sudo apt install qtdeclarative5-dev-tools
qmlplugindump -v <plugin base path>
```

An useful set of environment variables and commands to debug problems:

```bash
# Runs without graphical output.
export QT_QPA_PLATFORM=offscreen
# Enable full logging from Qt's rendering system and Specchio components.
export QT_LOGGING_RULES=qt.qpa.*=true;*=true
# Enable creating core dumps (`core` files in the current directory). Useful if Specchio crashes.
ulimit -c unlimited
# Runs Specchio in window mode for debugging.
specchio -platform xcb
```

If on startup you get the error `Could not find DRM device!`, ensure you are using the [real Raspberry Pi OpenGL drivers](./doc/INSTALL.md).

To launch the tests, use the Docker script `docker/docker-launch <arch> debug tests`. If you're developing on the
Raspberry itself, for the `<arch>` parameter use `arm`, otherwise `x86`.
