# Docker

Specchio is most easily built via Docker. In this directory there are the two Dockerfiles that set up the build
environment, and some scripts to simplify building via Docker.

## Scripts

To build Specchio for your Raspberry Pi (armv6hf architecture), use:

```bash
# Invoke from the root directory of the repository
docker/docker-launch arm release
```

To build it on a desktop PC (x86-64 architecture), use:

```bash
# Invoke from the root directory of the repository
docker/docker-launch x86 release
```

In case you need to change the Docker images, you can easily rebuild them locally with the command:

```bash
# Invoke from the root directory of the repository
docker/docker-build-images
```
